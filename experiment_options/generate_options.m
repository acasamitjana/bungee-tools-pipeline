
YamlStruct = ReadYaml([pwd() filesep '..' filesep '..' filesep 'configFile.yaml']);
directory = YamlStruct.sectionDir;

d  = dir(directory);
stain_list = {'LFB', 'HE', 'LFB_HE'};
l1_list = {0, 1};

options = {};
it_o = 0;
for i=1:length(d)
    if isfolder([directory filesep d(i).name]) && ~strcmp('.', d(i).name) && ~strcmp('..', d(i).name)
        for j=1:length(stain_list)
            for k=1:length(l1_list)
                it_o = it_o+1;
                opt = {};
                opt{1} = stain_list{j};
                opt{2} = d(i).name;
                opt{3} = l1_list{k};
                
                options{it_o}=opt;
                
            end
        end
    end
end
