% Makes mosaic from RGB blocks
function mriMosaic=makeRgbMosaic(BLOCKS,headers,RES,VERBOSE)

if nargin<4
    VERBOSE=0;
end

% Get maximum size
maxSiz=0;
for b=1:length(BLOCKS)
    siz=size(BLOCKS{b});
    siz=siz(1:3);
    maxSiz=max(maxSiz,max(siz));
end

% Get corners of cuboid in RAS space
minR=inf; maxR=-inf;
minA=inf; maxA=-inf;
minS=inf; maxS=-inf;

% For each voxel, check which coordinates (mm) it corresponds and look for
% total extension in each direction (R-A-S)
for b=1:length(BLOCKS)
    for i=[0 size(BLOCKS{b},2)]
        for j=[0 size(BLOCKS{b},1)]
            for k=[0 size(BLOCKS{b},3)]
                
                aux=headers(:,:,b)*[i; j; k; 1];
                
                minR=min(minR,aux(1));
                maxR=max(maxR,aux(1));
                
                minA=min(minA,aux(2));
                maxA=max(maxA,aux(2));
                
                minS=min(minS,aux(3));
                maxS=max(maxS,aux(3));
            end
        end
    end
end

% Define header and size
v2r0=[RES 0 0 minR;
    0 RES 0 minA;
    0 0 RES minS;
    0 0 0 1];
vr=[RES RES RES];
siz=ceil([maxA-minA maxR-minR maxS-minS]/RES); % Extension/resolution (mm)

mriMosaic=[];
mriMosaic.vol=zeros([siz 3]);
mriMosaic.vox2ras0=v2r0;
mriMosaic.volres=vr;


% Resample
NUM=zeros(size(mriMosaic.vol));
DEN=zeros(size(mriMosaic.vol));
[gr1,gr2,gr3]=ndgrid(1:size(mriMosaic.vol,1),1:size(mriMosaic.vol,2),1:size(mriMosaic.vol,3));
voxMosaic=[gr2(:)'-1; gr1(:)'-1; gr3(:)'-1;  ones(1,numel(gr1))];
rasMosaic=mriMosaic.vox2ras0*voxMosaic;
if VERBOSE, disp(['Making mosaic: resampling ' num2str(length(BLOCKS)) ' blocks']); end
for b=1:length(BLOCKS)
    if VERBOSE, fprintf('%d ',b); end
    voxB=inv(headers(:,:,b))*rasMosaic;
    v1=1+voxB(2,:);
    v2=1+voxB(1,:);
    v3=1+voxB(3,:);
    ok=v1>=1 & v2>=1 & v3>=1 & v1<=size(BLOCKS{b},1) & v2<=size(BLOCKS{b},2) & v3<=size(BLOCKS{b},3);
    vals=zeros(1,size(rasMosaic,2));
    for c=1:3
        vals(ok)=interp3(BLOCKS{b}(:,:,:,c),v2(ok),v1(ok),v3(ok));
        intensities=zeros([size(mriMosaic.vol,1),size(mriMosaic.vol,2),size(mriMosaic.vol,3)]);
        intensities(:)=vals;
        NUM(:,:,:,c)=NUM(:,:,:,c)+intensities;
        DEN(:,:,:,c)=DEN(:,:,:,c)+double(intensities>0);
    end
end
if VERBOSE, disp(' '); end
VOL=NUM./(eps+DEN);
mriMosaic.vol=VOL;


