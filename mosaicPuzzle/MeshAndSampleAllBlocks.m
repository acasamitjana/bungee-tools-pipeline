% This script meshes all the masks in a directory, samples the RBG values of
% the block in the vertices, and saves to disk.
clear

YamlStruct = ReadYaml([pwd() filesep '..' filesep 'configFile.yaml']);

downsampledDir=YamlStruct.initDownsampledBlocks; 
initializedDir=YamlStruct.initBlocks;

addpath(genpath(YamlStruct.utilsFunc));
addpath(YamlStruct.isomeshFunc);

outputDir=[downsampledDir filesep 'meshes' filesep];
if exist(outputDir,'dir')==0
    mkdir(outputDir);
end

d=dir([downsampledDir filesep '*.mask.downsampled.mgz']);
for i=1:length(d)
    
    disp(['Block ' num2str(i) ' of ' num2str(length(d))]);
    
    inputMask=[downsampledDir filesep d(i).name];
    inputRGBlarge=[initializedDir filesep d(i).name(1:end-21) '.rgb.initialized.mgz'];
    outputFile=[outputDir filesep d(i).name(1:end-28) '.mesh_and_colors.mat'];
    
    if exist(outputFile,'file')
        disp('     Output already there; skipping');
    else
        disp('     Reading in mask');
        margin=3;
        mri=myMRIread(inputMask);
        M=double(mri.vol>=max(mri.vol(:))/2);
        aux=M;
        M=zeros(size(M)+2*margin);
        M(margin+1:end-margin,margin+1:end-margin,margin+1:end-margin)=aux;
        M=imerode(M,createSphericalStrel(1));
        
        disp('     Meshing');
        opt=0.25;
        method='simplify';
        dofix=1;
        isovalues=1;
        [no,el,regions,holes]=vol2surf(M,1:size(M,1),1:size(M,2),1:size(M,3),opt,dofix,method,isovalues);
        no=no-margin;
        M=M(margin+1:end-margin,margin+1:end-margin,margin+1:end-margin);
        
        disp('     Reading in RGB');
        X=zeros([size(M) 3]);
        mri2=myMRIread(inputRGBlarge);
        factor=mri.volres./mri2.volres;
        for c=1:3
            aux=mri2;
            aux.vol=aux.vol(:,:,:,c);
            aux2=downsampleMRI(aux,factor);
            X(:,:,:,c)=aux2.vol;
        end
        
        disp('     Interpolating');
        
        valR=interpn(X(:,:,:,1),no(:,1),no(:,2),no(:,3));
        valG=interpn(X(:,:,:,2),no(:,1),no(:,2),no(:,3));
        valB=interpn(X(:,:,:,3),no(:,1),no(:,2),no(:,3));
        vals=[valR valG valB];
        
        % VOX2RAS compliant version
        vertices=no(:,[2 1 3])-1;
        elements=el(:,1:3);
        
        % expand a bit, to compensate for erosion
        mu=mean(vertices);
        vertices=bsxfun(@minus,vertices,mu);
        dists=sqrt(sum(vertices.*vertices,2));
        mdist=mean(dists);
        factor=(mdist+1)/mdist;
        vertices=vertices*factor;
        vertices=bsxfun(@plus,vertices,mu);
        
        % Save to file
        save(outputFile,'vertices','vals','elements');
        
    end
end

% And finally, the MRI volume
disp('Working on MRI volume');

inputMask=[downsampledDir filesep 'DownsampledMRI.mask.mgz'];
outputFile=[outputDir filesep 'MRImask.mat'];

if exist(outputFile,'file')
    disp('     Output already there; skipping');
else
    disp('     Reading in mask');
    margin=3;
    mri=myMRIread(inputMask);
    M=double(mri.vol>=max(mri.vol(:))/2);
    aux=M;
    M=zeros(size(M)+2*margin);
    M(margin+1:end-margin,margin+1:end-margin,margin+1:end-margin)=aux;
    M=imerode(M,createSphericalStrel(1));
    
    disp('     Meshing');
    opt=0.25;
    method='simplify';
    dofix=1;
    isovalues=1;
    [no,el,regions,holes]=vol2surf(M,1:size(M,1),1:size(M,2),1:size(M,3),opt,dofix,method,isovalues);
    no=no-margin;
    
    % VOX2RAS compliant version
    vertices=no(:,[2 1 3])-1;
    elements=el(:,1:3);
    vox2ras=mri.vox2ras0;
    
    save(outputFile,'vertices','elements','vox2ras');
end




