YamlStruct = ReadYaml([pwd() filesep '..' filesep 'configFile.yaml']);
histoDir = YamlStruct.HistoRecDir;


loss = 'L1';
stain = 'LFB';
stainDir = [histoDir filesep stain];
blockFileDirs = dir([stainDir filesep loss '*']);
BlockFileNames = {};
it_f=1;
for i=1:length(blockFileDirs)
    if isfolder([stainDir filesep blockFileDirs(i).name])
        fn = [stainDir filesep blockFileDirs(i).name filesep YamlStruct.Subject '_' blockFileDirs(i).name(end-3:end) '_' stain '.volume.rgb.mgz'];
        if exist([stainDir filesep blockFileDirs(i).name filesep YamlStruct.Subject '_' blockFileDirs(i).name(end-3:end) '_' stain '.volume.rgb.mgz'])
            BlockFileNames{it_f} = [stainDir filesep blockFileDirs(i).name filesep YamlStruct.Subject '_' blockFileDirs(i).name(end-3:end) '_' stain '.volume.rgb.mgz'];
            it_f = it_f + 1;
        end
    end
end
RES = 0.5;
VERBOSE = 1;
FWHM=2*log(2)*RES/pi;

mriMosaic=makeRgbMosaicMemoryEfficient(BlockFileNames,RES,FWHM,VERBOSE);
myMRIwrite(mriMosaic, [histoDir filesep stain filesep loss '_histoMosaic.' num2str(RES) 'mm.rgb.mgz'])
