# MOSAIC-PUZZLE
> Code to jointly register blockface volumes to MRI (Mancini et al., ISBI 2019).

## USERS 

RefineWithConstrainedJointRegistration.m

This is the entry point. All other functions are auxiliary. The different 
options are specified in the first 50 lines of code. Please see the comments
in the script.




## DEVELOPERS

These are functions to make movies of the registration

MeshAndSampleAllBlocks.m

This script meshes all the masks in a directory, samples the RBG values of
the block in the vertices, and saves to disk.

makeRegistrationMovie.m

Uses the meshes from MeshAndSampleAllBlocks to make a movie of the registration
procedure.



## AUXILIARY FUNCTIONS 

CostJointRegistrationXXXX.m

A bunch of cost functions at different levels of the registration hierarchy
(see Mancini et al., ISBI 2019). Not the cleanest...


make[Gray/Rgb/Seg]Mosaic.m

Makes a mosaic from a bunch of blocks at the prescribed resolution. It has
three versions, for grayscale data, RGB, and segmentations.

## PACKAGES

L-BFGS-B-C 

Downloaded from https://github.com/stephenbeckr/L-BFGS-B-C/
You should probably recompile in your architecture before running this code.
You can do this by running compile_mex.m in ./L-BFGS-B-C/Matlab/

## TODO
Automatize pyramidal processing in RefineWithConstrainedJointRegistration





