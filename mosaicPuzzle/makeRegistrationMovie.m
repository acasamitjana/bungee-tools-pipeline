% Uses the meshes from MeshAndSampleAllBlocks to make a movie of the
% registration procedure
clear 
clc
close all

YamlStruct = ReadYaml([pwd() filesep '..' filesep 'configFile.yaml']);

meshDir=[YamlStruct.initDownsampledBlocks filesep 'meshes/'];
headerFile=[YamlStruct.RegisteredBlocks filesep 'headers.mat'];
outputDir=[YamlStruct.RegisteredBlocks filesep 'movieTIFsDilatedExplicitRotation/'];
format='tif';


fcolor = [0.9 0.9 0.9];
mriColor = [0.1 0.8 0.2];
colors = [70  130 180; 245 245 245; 205 62  78 ; 120 18  134; 196 58  250 ; 220 248 164; 230 148 34 ;  122 186 220; 236 13  176; 12  48  255; 204 182 142; 42  204 164; 119 159 176; 220 216 20 ; 103 255 255; 80  196 98 ; 60  58  210; 60  60  60 ; 60  255 127; 165 42  42 ; 135 206 235; 160 32  240; 0   200 200; 100 50  100; 135 50  74 ; 122 135 50 ; 51  50  135; 74  155 60 ; 120 62  43 ; 74  155 60 ; 122 135 50];
colors = [colors; colors; colors] / 255; % this should be enough

addpath(genpath(YamlStruct.lbfgsFunc));
addpath(genpath(YamlStruct.utilsFunc));

if exist(outputDir,'dir')==0
    mkdir(outputDir);
end
delete([outputDir filesep '*.' format]);

% Read in meshes and values
d=dir([meshDir filesep '*.mesh_and_colors.mat']);
% d=d(1:9); 
nv=0;
NO=cell([1,length(d)]);
VAL=cell([1,length(d)]);
EL=cell([1,length(d)]);
for i=1:length(d)
    load([meshDir filesep d(i).name],'vertices','vals','elements');
    nv=nv+size(vertices,1);
    NO{i}=vertices;
    VAL{i}=vals;
    EL{i}=elements;
end
load(headerFile,'headersAllIts');

% Warping and concatenating
P=zeros([nv 3 length(headersAllIts)]);
E=zeros([nv 3]);
V=zeros([nv 3]);
Vflat=zeros([nv 3]);
for i=1:length(headersAllIts)
    c=0;
    cc=0;
    for j=1:length(NO)
        aux=headersAllIts{i}(:,:,j)*[NO{j}'; ones(1,size(NO{j},1))];
        P(c+1:c+size(aux,2),:,i)=aux(1:3,:)';
        if i==1 % values are always the same
            V(c+1:c+size(aux,2),:)=VAL{j};
            Vflat(c+1:c+size(aux,2),:)=repmat(colors(j,:),[size(aux,2) 1]);
            E(cc+1:cc+size(EL{j},1),:)=EL{j}+c;
            cc=cc+size(EL{j},1);
        end
        c=c+size(aux,2);
    end
end

% Get FOV
cog=mean(mean(P,1),3);
P=bsxfun(@minus,P,cog);

% Load MRI mesh
load([meshDir filesep 'MRImask.mat'],'vertices','elements','vox2ras');
Em=elements;
aux=vox2ras * [vertices'; ones(1,size(vertices,1))];
Pm=aux(1:3,:)';
Pm=bsxfun(@minus,Pm,cog);

% limits
maxi=max(abs([P(:);Pm(:)]))+5;
mini=-maxi;
limits=[mini maxi mini maxi mini maxi];

% stretch contrast a bit
v1=quantile(V(V>0),0.02);
v2=quantile(V(V<255),0.98);
V=(V-v1)/(v2-v1);
V(V<0)=0; V(V>1)=1;

% Plot and rotate around
close all;
figure;
NF=0;

steps=230;
for angle=([-90:360/steps:270]/180*pi)
    
    RM=[cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];
    Prot=P(:,:,1)*RM;
    
    subplot(1,2,1)
    h=trisurf(E,Prot(:,1,1),Prot(:,2,1),Prot(:,3,1));
    set(h,'FaceVertexCData',V);
    set(h,'edgealpha',0.1);
    set(h,'facecolor','interp');
    set(h,'edgecolor',[0 0 0]);
    axis off
    axis equal
    set(gcf,'color',fcolor);
    axis(limits);
    view(0,0);
    
    subplot(1,2,2)
    h=trisurf(E,Prot(:,1,1),Prot(:,2,1),Prot(:,3,1));
    set(h,'FaceVertexCData',Vflat);
    set(h,'edgealpha',0.333);
    set(h,'facecolor','interp');
    set(h,'edgecolor',[0 0 0]);
    axis off
    axis equal
    set(gcf,'color',fcolor);
    axis(limits);
    view(0,0);
    
    if NF==0
%         set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.05, 0.4, 0.95, 0.5]); %
        set(gcf, 'Units', 'centimeters', 'OuterPosition', [3.5, 15.5, 65, 50]); %squared for 90HH screen
%         set(gcf, 'Position', [0.0262 0 0.9500 0.9500])
    end
    
    NF=NF+1; saveas(gcf,[outputDir filesep 'frame_' num2str(NF,'%.5d')],format); drawnow
end

% Looks good, but not quite, when we show the MRI mask
% Let's fade it in
angle=-pi/2;
RM=[cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];
Prot=P(:,:,1)*RM;
Pmrot=Pm*RM;

subplot(1,2,1);
hold on,
h2a=trisurf(Em,Pmrot(:,1),Pmrot(:,2),Pmrot(:,3)); hold off
set(h2a,'edgealpha',0.1);
set(h2a,'facealpha',0.2);
set(h2a,'facecolor',mriColor);
set(h2a,'edgecolor',[0 0 0]);
hold off

subplot(1,2,2);
hold on,
h2b=trisurf(Em,Pmrot(:,1),Pmrot(:,2),Pmrot(:,3)); hold off
set(h2b,'edgealpha',0.1);
set(h2b,'facealpha',0.2);
set(h2b,'facecolor',mriColor);
set(h2b,'edgecolor',[0 0 0]);
hold off

for ea=0:0.0025:0.1 
    set(h2a,'edgealpha',ea);
    set(h2a,'facealpha',2*ea);
    set(h2b,'edgealpha',ea);
    set(h2b,'facealpha',2*ea);
    drawnow;
    NF=NF+1; saveas(gcf,[outputDir filesep 'frame_' num2str(NF,'%.5d')],format); drawnow
end

% and rotate again
for angle=([-90:360/steps:270]/180*pi)
    
    RM=[cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];
    Prot=P(:,:,1)*RM;
    Pmrot=Pm*RM;
    
    subplot(1,2,1)
    h=trisurf(E,Prot(:,1,1),Prot(:,2,1),Prot(:,3,1));
    set(h,'FaceVertexCData',V);
    set(h,'edgealpha',0.1);
    set(h,'facecolor','interp');
    set(h,'edgecolor',[0 0 0]);
    hold on,
    h2a=trisurf(Em,Pmrot(:,1),Pmrot(:,2),Pmrot(:,3)); hold off
    set(h2a,'edgealpha',0.1);
    set(h2a,'facealpha',0.2);
    set(h2a,'facecolor',mriColor);
    set(h2a,'edgecolor',[0 0 0]);
    set(h2a,'edgealpha',0.1);
    set(h2a,'facealpha',0.2);
    hold off
    axis off
    axis equal
    set(gcf,'color',fcolor);
    axis(limits);
    view(0,0);
    
    subplot(1,2,2)
    h=trisurf(E,Prot(:,1,1),Prot(:,2,1),Prot(:,3,1));
    set(h,'FaceVertexCData',Vflat);
    set(h,'edgealpha',0.333);
    set(h,'facecolor','interp');
    set(h,'edgecolor',[0 0 0]);
    hold on,
    h2b=trisurf(Em,Pmrot(:,1),Pmrot(:,2),Pmrot(:,3)); hold off
    set(h2b,'edgealpha',0.1);
    set(h2b,'facealpha',0.2);
    set(h2b,'facecolor',mriColor);
    set(h2b,'edgecolor',[0 0 0]);
    set(h2b,'edgealpha',0.1);
    set(h2b,'facealpha',0.2);
    hold off
    axis off
    axis equal
    set(gcf,'color',fcolor);
    axis(limits);
    view(0,0);
    
    NF=NF+1; saveas(gcf,[outputDir filesep 'frame_' num2str(NF,'%.5d')],format); drawnow
end

% Registrations
intSteps=30; 
angle=-pi/2;
RM=[cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];
for i=1:size(P,3)-1
    
    Pi=P(:,:,i)*RM;
    Pii=P(:,:,i+1)*RM;
    Pmrot=Pm*RM;
    
    for f=0:1/intSteps:1-1/intSteps
        % RGB
        subplot(1,2,1)
        Pint=(1-f)*Pi+f*Pii;
        h=trisurf(E,Pint(:,1),Pint(:,2),Pint(:,3));
        set(h,'FaceVertexCData',V);
        set(h,'edgealpha',0.1);
        set(h,'facecolor','interp');
        set(h,'edgecolor',[0 0 0]);
        
        hold on, h2a=trisurf(Em,Pmrot(:,1),Pmrot(:,2),Pmrot(:,3)); hold off
        set(h2a,'edgealpha',0.1);
        set(h2a,'facealpha',0.2);
        set(h2a,'facecolor',mriColor);
        set(h2a,'edgecolor',[0 0 0]);
        
        axis off
        axis equal
        set(gcf,'color',fcolor);
        axis(limits);
        view(0,0)
        
        % flat color
        subplot(1,2,2)
        h=trisurf(E,Pint(:,1),Pint(:,2),Pint(:,3));
        set(h,'FaceVertexCData',Vflat);
        set(h,'edgealpha',0.333);
        set(h,'facecolor','interp');
        set(h,'edgecolor',[0 0 0]);
        
        hold on, h2b=trisurf(Em,Pmrot(:,1),Pmrot(:,2),Pmrot(:,3)); hold off
        set(h2b,'edgealpha',0.1);
        set(h2b,'facealpha',0.2);
        set(h2b,'facecolor',mriColor);
        set(h2b,'edgecolor',[0 0 0]);
        
        axis off
        axis equal
        set(gcf,'color',fcolor);
        axis(limits);
        view(0,0)
        
        NF=NF+1; saveas(gcf,[outputDir filesep 'frame_' num2str(NF,'%.5d')],format); drawnow
    end
end

% Go around registered result
for angle=([-90:360/steps:270]/180*pi)
    
    RM=[cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];
    Prot=P(:,:,end)*RM;
    Pmrot=Pm*RM;
    
    subplot(1,2,1)
    h=trisurf(E,Prot(:,1,1),Prot(:,2,1),Prot(:,3,1));
    set(h,'FaceVertexCData',V);
    set(h,'edgealpha',0.1);
    set(h,'facecolor','interp');
    set(h,'edgecolor',[0 0 0]);
    hold on,
    h2a=trisurf(Em,Pmrot(:,1),Pmrot(:,2),Pmrot(:,3)); hold off
    set(h2a,'edgealpha',0.1);
    set(h2a,'facealpha',0.2);
    set(h2a,'facecolor',mriColor);
    set(h2a,'edgecolor',[0 0 0]);
    set(h2a,'edgealpha',0.1);
    set(h2a,'facealpha',0.2);
    hold off
    axis off
    axis equal
    set(gcf,'color',fcolor);
    axis(limits);
    view(0,0);
    
    subplot(1,2,2)
    h=trisurf(E,Prot(:,1,1),Prot(:,2,1),Prot(:,3,1));
    set(h,'FaceVertexCData',Vflat);
    set(h,'edgealpha',0.333);
    set(h,'facecolor','interp');
    set(h,'edgecolor',[0 0 0]);
    hold on,
    h2b=trisurf(Em,Pmrot(:,1),Pmrot(:,2),Pmrot(:,3)); hold off
    set(h2b,'edgealpha',0.1);
    set(h2b,'facealpha',0.2);
    set(h2b,'facecolor',mriColor);
    set(h2b,'edgecolor',[0 0 0]);
    set(h2b,'edgealpha',0.1);
    set(h2b,'facealpha',0.2);
    hold off
    axis off
    axis equal
    set(gcf,'color',fcolor);
    axis(limits);
    view(0,0);
    
    NF=NF+1; saveas(gcf,[outputDir filesep 'frame_' num2str(NF,'%.5d')],format); drawnow
end

% Fade out mask
for ea=0.1:-0.0025:0 
    set(h2a,'edgealpha',ea);
    set(h2a,'facealpha',2*ea);
    set(h2b,'edgealpha',ea);
    set(h2b,'facealpha',2*ea);
    drawnow;
    NF=NF+1; saveas(gcf,[outputDir filesep 'frame_' num2str(NF,'%.5d')],format); drawnow
end

% Go around again, no mask result
for angle=([-90:360/(steps*5/3):510]/180*pi)
    
    RM=[cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];
    Prot=P(:,:,end)*RM;
    Pmrot=Pm*RM;
    
    subplot(1,2,1)
    h=trisurf(E,Prot(:,1,1),Prot(:,2,1),Prot(:,3,1));
    set(h,'FaceVertexCData',V);
    set(h,'edgealpha',0.1);
    set(h,'facecolor','interp');
    set(h,'edgecolor',[0 0 0]);
    axis off
    axis equal
    set(gcf,'color',fcolor);
    axis(limits);
    view(0,0);
    
    subplot(1,2,2)
    h=trisurf(E,Prot(:,1,1),Prot(:,2,1),Prot(:,3,1));
    set(h,'FaceVertexCData',Vflat);
    set(h,'edgealpha',0.333);
    set(h,'facecolor','interp');
    set(h,'edgecolor',[0 0 0]);
    axis off
    axis equal
    set(gcf,'color',fcolor);
    axis(limits);
    view(0,0);
    
    NF=NF+1; saveas(gcf,[outputDir filesep 'frame_' num2str(NF,'%.5d')],format); drawnow
end

% FInally, make video file
disp('Writing video');

disp('Quick preliminary pass to figure out excessive spacing');
for f=1:10:NF
    I = imread([outputDir filesep 'frame_' num2str(f,'%.5d') '.' format]);
    if f==1
        PREV=I;
        ACC=zeros([size(I,1),size(I,2)]);
    else
        ACC = ACC | sum(I~=PREV,3)>0;
        PREV=I;
    end
end
% Kill edges
[r,c]=find(ACC>0);
r1=min(r);
c1=min(c);
r2=max(r);
c2=max(c);

% Big gap in the center?
x=sum(ACC(:,c1:c2),1);
[bw,n]=bwlabeln(x==0);
len=zeros(1,n); for i=1:n, len(i)=sum(bw==i); end
[~,idx]=max(len);
aux = find(bw==idx);
ca=min(aux)+c1; cb=max(aux)+c1;

% Add margins
margin=5;
r1=max(1,r1-margin);
c1=max(1,c1-margin);
r2=min(size(ACC,1),r2+margin);
c2=min(size(ACC,2),c2+margin);
ca=ca+margin;
cb=max(cb-margin,ca+1);

% Second pass: write video
disp('Second pass: writing video');
video = VideoWriter([outputDir filesep 'video_prova.mp4'],'Uncompressed AVI');%MPEG-4%
video.FrameRate=20;
open(video);
for f=1:NF
    I = imread([outputDir filesep 'frame_' num2str(f,'%.5d') '.' format]);
    writeVideo(video,I(r1:r2,[c1:ca cb:c2],:)); %write the image to file
end
close(video); %close the file

disp('All done!');






