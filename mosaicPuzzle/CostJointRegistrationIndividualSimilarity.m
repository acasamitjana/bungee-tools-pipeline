function [cost,grad,new_headers] = CostJointRegistrationIndividualSimilarity(params,MRI,...
    MRI_MASK_DILATED,MRI_MASK,Bmask,Bedge,headers,W_OVERLAP,W_EDGES,W_BOUNDARY,W_SCALE,grouping,referenceResolution,PROP)

Nblocks=length(Bmask);
EPS=0.2; %0.01;

% For random sampling of voxels
if PROP<1
    RANDOM=0;
    SEED=[];
    PROP=[];
else
    RANDOM=1;
    SEED=1981;
    PROP=0.25;
end

% random sampling, if necessary
if RANDOM
    if ~isempty(SEED)
        rng(SEED);
    end
    R=rand(size(MRI_MASK_DILATED))<PROP;
    MRI_MASK_DILATED = MRI_MASK_DILATED & R;
end

% Grouping: essentially treat groups of blocks as single blocks
% Makes the first passes more robust
if exist('grouping','var')==0
    groupingOrig=1:Nblocks;
    grouping=groupingOrig;
else
    groupingOrig=grouping;
    if isempty(grouping)
        grouping=1:Nblocks;
    end
end
Ngroups=max(grouping);

% reshape parameters
paramsOrig=params;
params=reshape(params,[8 Ngroups]);

[II,JJ,KK]=ndgrid(1:size(MRI.vol,1),1:size(MRI.vol,2),1:size(MRI.vol,3));
II=II(MRI_MASK_DILATED);
JJ=JJ(MRI_MASK_DILATED);
KK=KK(MRI_MASK_DILATED);
vox=[JJ(:)'-1; II(:)'-1; KK(:)'-1;  ones(1,numel(II))];
ras=MRI.vox2ras0*vox;

mask=MRI_MASK(MRI_MASK_DILATED);
maskBoundary=~mask;

MASKSUM=zeros([Nblocks,size(vox,2)]);
ACCUMEDGE=zeros([Nblocks,size(vox,2)]);
new_headers=zeros(size(headers));

% First pass: center values
for g=1:Ngroups
    
    % Compute transform
    trans=params(1:3,g);
    angles=params(4:6,g)/180*pi;
    scalingXY=exp(params(7,g)/100);
    scalingZ=exp(params(8,g)/100);
    
    % Center of rotation
    crs=[];
    for b=find(grouping==g)
        cv=([size(Bedge{b},2) size(Bedge{b},1) size(Bedge{b},3)]-1)/2;
        crs(:,end+1)=headers(:,:,b)*[cv' ; 1];
    end
    cr=mean(crs,2);
    
    T1=[1 0 0 -cr(1); 0 1 0 -cr(2); 0 0 1 -cr(3); 0 0 0 1];
    
    T2=[scalingXY 0 0 0;
        0 scalingXY 0 0;
        0 0 scalingZ 0;
        0 0 0 1];
    
    T3=[1 0 0 0; 0 cos(angles(1)) -sin(angles(1)) 0; 0 sin(angles(1)) cos(angles(1)) 0; 0 0 0 1];
    T4=[cos(angles(2)) 0 sin(angles(2)) 0; 0 1 0 0; -sin(angles(2)) 0 cos(angles(2)) 0; 0 0 0 1];
    T5=[cos(angles(3)) -sin(angles(3)) 0 0; sin(angles(3)) cos(angles(3)) 0 0; 0 0 1 0; 0 0 0 1];
    T6=[1 0 0 cr(1); 0 1 0 cr(2); 0 0 1 cr(3); 0 0 0 1];
    T7=[1 0 0 trans(1); 0 1 0 trans(2); 0 0 1 trans(3); 0 0 0 1];
    T=T7*T6*T5*T4*T3*T2*T1;
    
    for b=find(grouping==g)
        
        BE=Bedge{b};
        BM=Bmask{b};
        
        new_headers(:,:,b)=T*headers(:,:,b);
        
        % Interpolate
        % vox=1+inv(new_headers(:,:,b))*ras;
        vox=1+new_headers(:,:,b)\ras;
        v1=1+vox(2,:);
        v2=1+vox(1,:);
        v3=1+vox(3,:);
        
        ok=v1>=1 & v2>=1 & v3>=1 & v1<=size(BE,1) & v2<=size(BE,2) & v3<=size(BE,3);
        
        vals=zeros(1,size(ras,2));
        vals(ok)=interp3(BE,v2(ok),v1(ok),v3(ok),'linear');
        ACCUMEDGE(b,:)=vals;
        vals(ok)=interp3(BM,v2(ok),v1(ok),v3(ok),'linear');
        MASKSUM(b,:)=vals;
    end
end

% Get the cost
sumMASKSUM=sum(MASKSUM,1);
EDGE=sum(ACCUMEDGE.*MASKSUM,1)./(eps+sumMASKSUM);

aux=abs(sumMASKSUM(:)-double(mask(:)>0));
costOverlap=mean(aux);

x=EDGE;
y=MRI.vol(MRI_MASK_DILATED);

% Old version using NCC
% costEdges = 1 - corr(x(mask)',y(mask));
% costEdgesBoundary = 1 - corr(x(maskBoundary)',y(maskBoundary));

% I disabled this after switching to sigmoids to squash gradient maps
% maskX=x>0;
% x=x/median(x(maskX));
% y=y/median(y(maskX));
aux=abs(x'-y);
% Should probably merge these two ...
costEdges=mean(aux(mask));
costEdgesBoundary=mean(aux(maskBoundary));

aux=zeros(1,size(new_headers,3));
for i=1:length(aux)
    naturalScale=(abs(det(new_headers(:,:,i))).^(1/3))/referenceResolution;
    logScale=log(naturalScale);
    aux(i)=logScale^2;
end
costScale=mean(aux);

cost=W_OVERLAP*costOverlap+W_EDGES*costEdges+...
    W_BOUNDARY*costEdgesBoundary+W_SCALE*costScale;

if nargout==2
    
    % OK now we do the gradient.
    % The key here is that we can reuse a lot of the
    % computations
    gradMatrix=zeros(8,Ngroups);
    
    for g=1:Ngroups
        
        paramsGroup=params(:,g);
        MASKSUMgroup=MASKSUM; ACCUMEDGEgroup=ACCUMEDGE;
        
        for p=1:8
            
            for side=1:2
                if side==1, inc=EPS; factor=1; else, inc=-EPS; factor=-1; end
                
                paramsG=paramsGroup;
                paramsG(p)=paramsG(p)+inc;
                
                % Compute transform
                trans=paramsG(1:3);
                angles=paramsG(4:6)/180*pi;
                scalingXY=exp(paramsG(7)/100);
                scalingZ=exp(paramsG(8)/100);
                
                % Center of rotation
                crs=[];
                for b=find(grouping==g)
                    cv=([size(Bedge{b},2) size(Bedge{b},1) size(Bedge{b},3)]-1)/2;
                    crs(:,end+1)=headers(:,:,b)*[cv' ; 1];
                end
                cr=mean(crs,2);
                
                T1=[1 0 0 -cr(1); 0 1 0 -cr(2); 0 0 1 -cr(3); 0 0 0 1];
                
                T2=[scalingXY 0 0 0;
                    0 scalingXY 0 0;
                    0 0 scalingZ 0;
                    0 0 0 1];
                
                T3=[1 0 0 0; 0 cos(angles(1)) -sin(angles(1)) 0; 0 sin(angles(1)) cos(angles(1)) 0; 0 0 0 1];
                T4=[cos(angles(2)) 0 sin(angles(2)) 0; 0 1 0 0; -sin(angles(2)) 0 cos(angles(2)) 0; 0 0 0 1];
                T5=[cos(angles(3)) -sin(angles(3)) 0 0; sin(angles(3)) cos(angles(3)) 0 0; 0 0 1 0; 0 0 0 1];
                T6=[1 0 0 cr(1); 0 1 0 cr(2); 0 0 1 cr(3); 0 0 0 1];
                T7=[1 0 0 trans(1); 0 1 0 trans(2); 0 0 1 trans(3); 0 0 0 1];
                T=T7*T6*T5*T4*T3*T2*T1;
                
                v2rProbe = new_headers;
                for b=find(grouping==g)
                    BE=Bedge{b}; BM=Bmask{b};
                    v2rProbe(:,:,b)=T*headers(:,:,b);
                    
                    vox=1+v2rProbe(:,:,b)\ras; v1=1+vox(2,:); v2=1+vox(1,:); v3=1+vox(3,:);
                    ok=v1>=1 & v2>=1 & v3>=1 & v1<=size(BE,1) & v2<=size(BE,2) & v3<=size(BE,3);
                    
                    vals=zeros(1,size(ras,2));
                    vals(ok)=interp3(BE,v2(ok),v1(ok),v3(ok),'linear');
                    ACCUMEDGEgrad=vals;
                    vals(ok)=interp3(BM,v2(ok),v1(ok),v3(ok),'linear');
                    MASKSUMgrad=vals;
                    
                    MASKSUMgroup(b,:)=MASKSUMgrad;
                    ACCUMEDGEgroup(b,:)=ACCUMEDGEgrad;
                end
                
                % Get the cost
                sumMASKSUMgroup=sum(MASKSUMgroup,1);
                EDGEgroup=sum(ACCUMEDGEgroup.*MASKSUMgroup,1)./(eps+sumMASKSUMgroup);
                
                % aux=abs(sumMASKSUMgroup-1);
                % costOverlapG=mean(aux(mask));
                aux=abs(sumMASKSUMgroup(:)-double(mask(:)>0));
                costOverlapG=mean(aux);
                
                x=EDGEgroup;
                % y=MRI.vol(MRI_MASK_DILATED);
                
                % Old version using NCC
                % costEdgesG = 1 - corr(x(mask)',y(mask));
                % costEdgesBoundaryG = 1 - corr(x(maskBoundary)',y(maskBoundary));
                
                % I disabled this after switching to sigmoids to squash gradient maps
                % maskX=x>0;
                % x=x/median(x(maskX));
                % y=y/median(y(maskX));
                aux=abs(x'-y);
                % Should probably merge these two ...
                costEdgesG=mean(aux(mask));
                costEdgesBoundaryG=mean(aux(maskBoundary));
                
                aux=zeros(1,size(v2rProbe,3));
                for i=1:length(aux)
                    naturalScale=(abs(det(v2rProbe(:,:,i))).^(1/3))/referenceResolution;
                    logScale=log(naturalScale);
                    aux(i)=logScale^2;
                end
                costScaleG=mean(aux);
                
                costG=W_OVERLAP*costOverlapG+W_EDGES*costEdgesG+...
                    W_BOUNDARY*costEdgesBoundaryG+W_SCALE*costScaleG;
                
                gradMatrix(p,g)=gradMatrix(p,g)+factor*costG; % we divide by 2*EPS later on
            end
        end
    end
    
    grad=gradMatrix(:)/(2*EPS);
else
    grad=nan*ones(size(params));
end








