
% Makes mosaic from RGB blocks
function mriMosaic=makeRgbMosaicMemoryEfficient(BlockFileNames,RES,FWHM,VERBOSE)

YamlStruct = ReadYaml([pwd() filesep '..' filesep 'configFile.yaml']);

setenv('FREESURFER_HOME', YamlStruct.freesurferDev);
addpath(YamlStruct.utilsFunc)
if nargin<4
    VERBOSE=0;
end

% Get maximum size  
headers=zeros(4,4,length(BlockFileNames));
sizeBlocks=zeros(3,length(BlockFileNames));
for b=1:length(BlockFileNames)
    mri=myMRIread(BlockFileNames{b},1); % 1 is for only header (much faster)
    headers(:,:,b)=mri.vox2ras0;
    sizeBlocks(:,b)=mri.volsize;
end

% Get corners of cuboid in RAS space
minR=inf; maxR=-inf;
minA=inf; maxA=-inf;
minS=inf; maxS=-inf;

for b=1:length(BlockFileNames)
    for i=[0 sizeBlocks(2,b) ]
        for j=[0 sizeBlocks(1,b)]
            for k=[0 sizeBlocks(3,b)]
                
                aux=headers(:,:,b)*[i; j; k; 1];
                
                minR=min(minR,aux(1));
                maxR=max(maxR,aux(1));
                
                minA=min(minA,aux(2));
                maxA=max(maxA,aux(2));
                
                minS=min(minS,aux(3));
                maxS=max(maxS,aux(3));
            end
        end
    end
end

% Define header and size
v2r0=[RES 0 0 minR;
    0 RES 0 minA;
    0 0 RES minS;
    0 0 0 1];
vr=[RES RES RES];
siz=ceil([maxA-minA maxR-minR maxS-minS]/RES);

mriMosaic=[];
mriMosaic.vol=zeros([siz 3]);
mriMosaic.vox2ras0=v2r0;
mriMosaic.volres=vr;

% Resample
NUM=zeros(size(mriMosaic.vol));
DEN=zeros(size(mriMosaic.vol));
[gr1,gr2,gr3]=ndgrid(1:size(mriMosaic.vol,1),1:size(mriMosaic.vol,2),1:size(mriMosaic.vol,3));
voxMosaic=[gr2(:)'-1; gr1(:)'-1; gr3(:)'-1;  ones(1,numel(gr1))];
rasMosaic=mriMosaic.vox2ras0*voxMosaic;
if VERBOSE, disp(['Making mosaic: resampling ' num2str(length(BlockFileNames)) ' blocks']); end
for b=1:length(BlockFileNames)
    if VERBOSE, fprintf('%d ',b); end

    cmd = ['mri_convert ' '--fwhm ' num2str(FWHM) ' ' BlockFileNames{b} ' ' [BlockFileNames{b}(1:end-3) 'tmp.mgz']];
    system(cmd)

    mri=myMRIread([BlockFileNames{b}(1:end-3) 'tmp.mgz']);
    mask=myMRIread([BlockFileNames{b}(1:end-7) 'mask.mgz']);
    mri.vol = mri.vol.*mask.vol;
    
    voxB=inv(mri.vox2ras0)*rasMosaic;
    
    v1=1+voxB(2,:);
    v2=1+voxB(1,:);
    v3=1+voxB(3,:);
    ok=v1>=1 & v2>=1 & v3>=1 & v1<=size(mri.vol,1) & v2<=size(mri.vol,2) & v3<=size(mri.vol,3);
    vals=zeros(1,size(rasMosaic,2));
    for c=1:3
        vals(ok)=interp3(mri.vol(:,:,:,c),v2(ok),v1(ok),v3(ok));
        intensities=zeros([size(mriMosaic.vol,1),size(mriMosaic.vol,2),size(mriMosaic.vol,3)]);
        intensities(:)=vals;
        
        NUM(:,:,:,c)=NUM(:,:,:,c)+intensities;
        DEN(:,:,:,c)=DEN(:,:,:,c)+double(intensities>0);
    end
    delete([BlockFileNames{b}(1:end-3) 'tmp.mgz'])
    
end
if VERBOSE, disp(' '); end
VOL=NUM./(eps+DEN);
mriMosaic.vol=VOL;



