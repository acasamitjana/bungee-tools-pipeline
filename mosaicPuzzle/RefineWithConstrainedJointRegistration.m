%    We jointly optimize for all blocks by
%    optimizing a cost function that encourages the blocks to solve the jigsaw puzzle
%    while rigidly registering the MR. The blocks are allowed to move with increasing
%    degrees of freedom for robustness
clear
clc
tic

YamlStruct = ReadYaml([pwd() filesep '..' filesep 'configFile.yaml']);

% Voxel size at which we work
downsampledVoxSize=0.5; 

% Directory with autopsy photos of whole slices
wholeSliceDir=YamlStruct;

% Directory with autopsy photos of blocks
cutFileDir=YamlStruct;

% Directory with blockface volumes
blockFaceStackDir=YamlStruct;

% Directory with registered blocks, typically initializedBlocks
initialDir=YamlStruct.initBlocks; %'/Users/iglesias/Downloads/initBlocksEpinav/'; 

% Output directory of your choice. Should be registeredBlocks
outputDir=YamlStruct.RegisteredBlocks;%'/Users/iglesias/Downloads/registeredBlocks/'; 

% This is a directory with the downsampled blocks. Useful for developing
downsampledDir=YamlStruct.initDownsampledBlocks;%'/Users/iglesias/Downloads/initBlocksEpinavDownsampled0.5mm/'; 

% Input MRI
inputMRI=YamlStruct.mriFile;%'/Users/iglesias/Downloads/averageWithReg.reoriented.nii.gz';


% You can either provide a segmentation mask or compute it by thresholding at this leve
MRI_SEG_OR_THRESHOLD=YamlStruct.segFile;
% MRI_SEG_OR_THRESHOLD=300;

% Registration schedule and number of iterations:
% 1: each structure has a similarity transform, with shared scaling factors (in plane + thickness)
% 2: each slice has 3D translation + rotation in plane, plus globally  shared XY & Z scalings
% 3: each block has 3D translation + rotation in plane, plus globally shared XY & Z scalings
% 4: each block has 3D translation + rotation in plane + XY & Z scalings
% 5: each block has 3D translation + 3D rotation + XY & Z scalings, all for each block.
SCHEDULE   =  [1  2  1  3  1  4  1  5  1  5]; 
N_INNER_ITS = [10 10 5  10 5  10 5  10 5  5];

% proportion of pixels used to compute cost function
PROP_COST=1.0; 

% Compute image gradient of blockface in 3D or 2D?
GRAD_IN_3D = 1;

% PARAMETER OF SIGMOID SQUASHING OF EDGES 
% I have a heuristic depending on resolution
USE_SIGMOID=1;
MU_SIGMOID_BLOCKS=20*downsampledVoxSize;
SIGMA_SIGMOID_BLOCKS=5*downsampledVoxSize;
MU_SIGMOID_MRI=100*downsampledVoxSize;
SIGMA_SIGMOID_MRI=25*downsampledVoxSize;

% A bunch of weights for the different terms in the registration
W_EDGES=10; 
W_BOUNDARY=W_EDGES;  % I should probably merge these 2 at some point...
W_OVERLAP=100;
W_SCALE=5;


%%%%%%%%%%%%  END OF OPTIONS %%%%%%%%%%%%%%%%%

addpath(genpath(YamlStruct.lbfgsFunc));
addpath(genpath(YamlStruct.utilsFunc));
addpath(genpath(YamlStruct.freesurfer6));

tempdir=[outputDir filesep 'temp' filesep];

% Some constants...
CEREBRUM=1;
CEREBELLUM=2;
BRAINSTEM=3;

% output and temporary dirs
if exist(outputDir,'dir')==0
    mkdir(outputDir);
end
if exist(tempdir,'dir')==0
    mkdir(tempdir);
end
if exist(downsampledDir,'dir')==0
    mkdir(downsampledDir);
end

% Downsample blocks: MRI and blocks (gray/rgb/mask)

% MRI scan
vs=num2str(downsampledVoxSize);
MRIdownsampled=[downsampledDir filesep 'DownsampledMRI.mgz'];
MRIdownsampledMask=[downsampledDir filesep 'DownsampledMRI.mask.mgz'];
MRIdownsampledMaskDilated=[downsampledDir filesep 'DownsampledMRI.mask.dilated.mgz'];

if exist(MRIdownsampled,'file')
    disp('  Downsampled MRI scan already found; reading from disk');
    mri=myMRIread(MRIdownsampled,0,tempdir);
else
    disp('  Downsampling MRI scan');
    mri=myMRIread(inputMRI);
    factor=downsampledVoxSize./mri.volres;
    mri2 = downsampleMRI(mri,factor);
    myMRIwrite(mri2,MRIdownsampled);
    mri=mri2;
end

if exist(MRIdownsampledMask,'file')
    disp('  Downsampled MRI mask already found; reading from disk');
    aux=myMRIread(MRIdownsampledMask,0,tempdir);
    MRI_MASK=aux.vol>0;
else
    disp('  Preparing MRI mask');
   if ischar(MRI_SEG_OR_THRESHOLD)
       aux=myMRIread(MRI_SEG_OR_THRESHOLD);
       aux.vol=double(aux.vol>0);
       factor=downsampledVoxSize./aux.volres;
       aux2 = downsampleMRI(aux,factor);
       MRI_MASK=aux2.vol>0.5;
   else
       MRI_MASK=mri.vol>MRI_SEG_OR_THRESHOLD;
   end
   strel=createSphericalStrel(3,downsampledVoxSize);
   MRI_MASK=imdilate(MRI_MASK,strel);
   MRI_MASK=imfill(MRI_MASK,'holes');
   MRI_MASK=imerode(MRI_MASK,strel);
   mri.vol=255*double(MRI_MASK);
   myMRIwrite(mri,MRIdownsampledMask,'float',tempdir);
   MRI_MASK=mri.vol>0;
end
if exist(MRIdownsampledMaskDilated,'file')
    disp('Dilated mask found in disk; reading');
    aux=myMRIread(MRIdownsampledMaskDilated);
    MRI_MASK_DILATED=aux.vol>0;
else
    disp('Dilating mask');
    MRI_MASK_DILATED=imdilate(getLargestCC(MRI_MASK),createSphericalStrel(8,downsampledVoxSize));
    aux=mri; 
    aux.vol=MRI_MASK_DILATED;
    myMRIwrite(aux,MRIdownsampledMaskDilated);  
end
% Read in manual transforms, if available
try
    LTAcerebellum=my_lta_read([initialDir filesep 'initialResampledCerebellum.regToMRI.ras2ras.lta']);
    LTAbrainstem=my_lta_read([initialDir filesep 'initialResampledBrainstem.regToMRI.ras2ras.lta']);
    LTAcerebrum=my_lta_read([initialDir filesep 'initialResampledCerebrum.regToMRI.ras2ras.lta']);
    suffix='initialized';
catch
    disp('Warning: initialization files missing:');
    disp(['   ' initialDir filesep 'initialResampled[Cerebrum/Cerebellum/Brainstem].regToMRI.lta']);
    disp('If you are OK with this, type "dbcont"');
    keyboard;
    LTAcerebellum=eye(4);
    LTAbrainstem=eye(4);
    LTAcerebrum=eye(4);
    suffix='reg';
end
LS=length(suffix);

% Block downsampling
disp('Downsampling blocks');
d=dir([initialDir filesep '*mask.' suffix '.mgz']);
Nblocks=length(d);
Bgray=cell([1,Nblocks]);
Bgraymasked=cell([1,Nblocks]);
Bmask=cell([1,Nblocks]);
headers=zeros([4,4,Nblocks]);
headersOrig=zeros([4,4,Nblocks]);
volreses=zeros([Nblocks,3]);
structureIdx=zeros([1,Nblocks]);
sliceIdx=zeros([1,Nblocks]);
NA=0; NP=0; NB=0; NC=0;

for i=1:Nblocks
    
    disp(['  Block ' num2str(i) ' of ' num2str(Nblocks)]);
    
    % find corresponding initial registration and slice indices
    nsl=str2double(d(i).name(end-19-LS));
    sliceIdx(i)=nsl;
    if any(d(i).name=='C') % cerebellum
        structureIdx(i)=CEREBELLUM;
        NC=max(NC,nsl);
        LTA=LTAcerebellum;
    elseif any(d(i).name=='B') % brainstem
        structureIdx(i)=BRAINSTEM;
        NB=max(NB,nsl);
        LTA=LTAbrainstem;
    else
        if num2str(d(i).name(end-20-LS))=='A'
            NA=max(NA,nsl);
            structureIdx(i)=CEREBRUM;
        else
            NP=max(NP,nsl);
            structureIdx(i)=CEREBRUM-1;
        end
        LTA=LTAcerebrum;
    end
    
    input=[initialDir filesep d(i).name];
    output=[downsampledDir filesep d(i).name(1:end-5-LS) '.downsampled.mgz'];
    if exist(output,'file')
        disp('     Mask already there; reading');
        mri2 = myMRIread(output);
    else
        mri = myMRIread(input);
        factor=downsampledVoxSize./mri.volres;
        mri2 = downsampleMRI(mri,factor);
        mri2.vox2ras0 = LTA * mri2.vox2ras0;
        myMRIwrite(mri2,output);
    end
    % a bit of cleanup, and normalizing to [0,1]
    mri2.vol=getLargestCC(mri2.vol>0).*double(mri2.vol/max(mri2.vol(:))); 
    % assign to cell
    Bmask{i} = mri2.vol;
    headers(:,:,i) = mri2.vox2ras0;
    headersOrig(:,:,i)= mri2.vox2ras0;
    volreses(i,:) = mri2.volres;

    input=[initialDir filesep d(i).name(1:end-9-LS) 'gray.' suffix '.mgz'];
    output=[downsampledDir filesep d(i).name(1:end-9-LS) 'gray.downsampled.mgz'];
    if exist(output,'file')
        disp('     Gray already there; reading');
        mri2 = myMRIread(output);
    else
        mri = myMRIread(input);
        factor=downsampledVoxSize./mri.volres;
        mri2 = downsampleMRI(mri,factor);
        mri2.vox2ras0 = LTA * mri2.vox2ras0;
        myMRIwrite(mri2,output);
    end
    Bgray{i} = mri2.vol;
    
    % The masked one is easy 
    Bgraymasked{i}=Bgray{i}.*Bmask{i};
end
sliceIdx(structureIdx==BRAINSTEM)=sliceIdx(structureIdx==BRAINSTEM)+NC;
sliceIdx(structureIdx==CEREBRUM)=sliceIdx(structureIdx==CEREBRUM)+NC+NB;
sliceIdx(structureIdx==CEREBRUM-1)=sliceIdx(structureIdx==CEREBRUM-1)+NC+NB+NA;
structureIdx(structureIdx==CEREBRUM-1)=CEREBRUM;

disp('Building edge maps')

%  Build (masked) edge maps
Bedge=cell([1,Nblocks]);
for i=1:Nblocks
    
    I=Bgray{i};
    M=Bmask{i};
    
    if GRAD_IN_3D % gradient in 3d or only in-plane?
        G = grad3d(I);
    else
        [~,GX,GY]=grad3d(I); % we avoid contribution from block
        G=sqrt(GX.*GX+GY.*GY);
    end
    
    G = G.*M; 
    
    if USE_SIGMOID % squash through a sigmoid
        x=(G-MU_SIGMOID_BLOCKS)/SIGMA_SIGMOID_BLOCKS; % for 3d gradient
        % x=(G-15)/3.5; % for 2d gradient
        x2=exp(x);
        G = x2./(1+x2);
    end
    
    Bedge{i}=G;
end

% MRI edges
MRIdownsampledEdges=[downsampledDir filesep 'DownsampledMRI.edges.mgz'];

mri=myMRIread(MRIdownsampled);
G=grad3d(mri.vol);
G(1:3,:,:)=0; G(:,1:3,:)=0; G(:,:,1:3)=0;
G(end-2:end,:,:)=0; G(:,end-2:end,:)=0; G(:,:,end-2:end)=0;
if USE_SIGMOID
    x=(G-MU_SIGMOID_MRI)/SIGMA_SIGMOID_MRI; % for 3d gradient
    % x=(G-15)/3.5; % for 2d gradient
    x2=exp(x);
    G = x2./(1+x2);
end
mri.vol=G;
myMRIwrite(mri,MRIdownsampledEdges,'float',tempdir);
MRI = mri; % we'll use this one over and over later on

% We are ready to iterate! We follow a schedule, with different levels of 
% flexibilityEach outter iteration does:
% a) Align the MRI to the mosaic of the blocks, using edges
% b) Align the blocks to the MRI with our highly customized cost function

mriMosaic=makeGrayMosaic(Bedge,headers,downsampledVoxSize);
myMRIwrite(mriMosaic,[outputDir filesep 'edgeMosaic_it_0.nii.gz']);
mriMosaic=makeGrayMosaic(Bgraymasked,headers,downsampledVoxSize);
myMRIwrite(mriMosaic,[outputDir filesep 'grayMosaic_it_0.nii.gz'],'float',tempdir);

N_OUTTER_ITS = length(SCHEDULE);

for outterIts=1:N_OUTTER_ITS
    disp('**************************');
    disp(['* Outter iteration ' num2str(outterIts) ' / ' num2str(N_OUTTER_ITS) ' *']);
    disp('**************************');
    disp(['  Iteration limit: ' num2str(N_INNER_ITS(outterIts))]);
        
    % We follow the schedule, that tells us which registration modes we use
    % Different modes are at different levels in the registration hierarchy, 
    % allowing different degrees of flexibility
    % Also, note that all parameters are 0 at the beginning of each
    % iteration, as we update the headers of the blocks after registration
    type=SCHEDULE(outterIts);
    
    if type == 1  
        disp('   Type 1: similarity transforms (1 per structure) with shared scaling');
        grouping=structureIdx;
        params=zeros([6*max(grouping)+2,1]);
        fun=@CostJointRegistration3DGlobalScaling;
    elseif type == 2   
        disp('   Type 2: 3D translation + rotation in plane, per slice, plus shared XY & Z scalings');
        grouping=sliceIdx;
        params=zeros([4*max(grouping)+2,1]);
        fun=@CostJointRegistrationGlobalScaling;
    elseif type==3  
        disp('   Type 3: 3D translation + rotation in plane, per block, plus globally shared XY & Z scalings');
        grouping=[];
        params=zeros([4*Nblocks+2,1]);
        fun=@CostJointRegistrationGlobalScaling;
    elseif type==4 
        disp('   Type 4: 3D translation + rotation in plane + XY & Z scalings, all for each block');
        grouping=[];
        params=zeros([6*Nblocks,1]);
        fun=@CostJointRegistrationIndividualScales;
    else 
        disp('   Type 5: 3D translation + 3D rotation + XY & Z scalings, all for each block');
        grouping=[];
        params=zeros([8*Nblocks,1]);
        fun=@CostJointRegistrationIndividualSimilarity;
    end
    
    % Optimization with L-BFGS
    x0=params;
    n=length(x0);
    u = Inf*ones(n,1); l = -u;
    opts = struct( 'x0', x0 );
    opts.printEvery     = 1;
    
    opts.m  = 5; % should be between 3 and 20; default is 5
    opts.maxIts = N_INNER_ITS(outterIts); % scheduled
    opts.maxTotalIts = 30 * opts.maxIts;
    % opts.verbose = -1; % default is -1, i.e., no output from mex
    
    [x,finalCost,info] = lbfgsb(  @(p)fun(p,MRI,MRI_MASK_DILATED,MRI_MASK,Bmask,...
        Bedge,headers,W_OVERLAP,W_EDGES,W_BOUNDARY,W_SCALE,grouping,downsampledVoxSize,PROP_COST),...
        l, u, opts );
    params=x;
    
    
    % Update headers and resolutions
    [~,~,headers]=fun(params,MRI,MRI_MASK_DILATED,MRI_MASK,Bmask,Bedge,headers,W_OVERLAP,W_EDGES,W_BOUNDARY,W_SCALE,grouping,downsampledVoxSize,PROP_COST);
    for i=1:Nblocks
        volreses(i,:)=sqrt(sum(headers(1:3,1:3,i).^2));
    end
    headersAllIts{1}=headersOrig;
    headersAllIts{outterIts+1}=headers;
    
    % Write some mosaics
    mriMosaic=makeGrayMosaic(Bedge,headers,downsampledVoxSize);
    myMRIwrite(mriMosaic,[outputDir filesep 'edgeMosaic_it_' num2str(outterIts) '.nii.gz'],'float',tempdir);
    mriMosaic=makeGrayMosaic(Bgraymasked,headers,downsampledVoxSize);
    myMRIwrite(mriMosaic,[outputDir filesep 'grayMosaic_it_' num2str(outterIts) '.nii.gz'],'float',tempdir);

    aux = Bmask;
    for i=1:length(d)
        aux{i}=i*double(aux{i}>.5);
    end
    mriMosaic=makeSegMosaic(aux,headers,downsampledVoxSize);
    myMRIwrite(mriMosaic,[outputDir filesep 'blockMosaic_it_' num2str(outterIts) '.nii.gz'],'float',tempdir);
    
end
% Write headers to disk (useful for visualization)
save([outputDir filesep 'headers.mat'],'headersAllIts');

% Almost there: I just need to undo the affine transform of the MRI,
% and then go back to the original resolution of the blockface photos
disp('*****************************************************');
disp('* Going back to original space (undoing transforms) *');
disp('*****************************************************');

% Now we've got all the downsampled blocks in the space of the original MRI
% We can compare with the original downsampled blocks in order to compute
% the RAS transforms
% Along the way, we also build a volume with the block indices, and a mat 
% file with the names of the blocks and their vox2ras0 transforms
vox2ras0s=cell([1,length(Bgray)]);
blocknames=cell([1,length(Bgray)]);
mriBlockIdx=myMRIread(inputMRI);
mriBlockIdx.vol(:)=0;
maxprob=zeros(size(mriBlockIdx.vol));
[II,JJ,KK]=ndgrid(1:size(mriBlockIdx.vol,1),1:size(mriBlockIdx.vol,2),1:size(mriBlockIdx.vol,3));
ras=mriBlockIdx.vox2ras0*[JJ(:)'-1; II(:)'-1; KK(:)'-1; ones([1,numel(II)])];
for i=1:length(Bgray)
    
    disp(['Block ' num2str(i) ' of ' num2str(length(Bgray))]);

    if any(d(i).name=='C') % cerebellum
        LTA=LTAcerebellum;
    elseif any(d(i).name=='B') % brainstem
        LTA=LTAbrainstem;
    else
        LTA=LTAcerebrum;
    end
    
    T=headers(:,:,i)*inv(headersOrig(:,:,i))*LTA;
    
    % GRAY
    mri=myMRIread([initialDir filesep d(i).name(1:end-10-LS) '.gray.' suffix '.mgz']);
    mri2=[];
    mri2.vol=mri.vol; 
    mri2.vox2ras0=T*mri.vox2ras; 
    mri2.volres=sqrt(sum(mri2.vox2ras0(1:3,1:3).^2));
    myMRIwrite(mri2,[outputDir filesep d(i).name(1:end-10-LS) '.gray.reg.mgz']);
        
    % RGB
    aux=myMRIread([initialDir filesep d(i).name(1:end-10-LS) '.rgb.' suffix '.mgz']);
    mri2.vol=aux.vol;
    myMRIwrite(mri2,[outputDir filesep d(i).name(1:end-10-LS) '.rgb.reg.mgz'],'float',tempdir);
    
    % MASK
    aux=myMRIread([initialDir filesep d(i).name]);
    mri2.vol=aux.vol;
    myMRIwrite(mri2,[outputDir filesep d(i).name(1:end-10-LS) '.mask.reg.mgz'],'float',tempdir);
    
    % Use mask to build index volume
    vox2ras0s{i}=mri2.vox2ras0;
    blocknames{i}=d(i).name(1:end-17-LS);
    vox=inv(mri2.vox2ras0)*ras;
    ii2=vox(2,:)-1; jj2=vox(1,:)-1; kk2=vox(3,:)-1;
    ok = ii2>=1 & jj2>=1 & kk2>=1 & ii2<=size(mri2.vol,1) & jj2<=size(mri2.vol,2) & kk2<=size(mri2.vol,3);
    aux=zeros(size(ii2));
    aux(ok)=interpn(mri2.vol,ii2(ok),jj2(ok),kk2(ok));
    aux=reshape(aux,size(mriBlockIdx.vol));
    mask=aux>maxprob;
    mriBlockIdx.vol(mask)=i;
    maxprob(mask)=aux(mask);
end
% Save index volume and companion mat file with headers and block names
% We take the largest connected component with p>0.5
mask=maxprob>0.5*max(maxprob(:));
mriBlockIdx.vol(mask==0)=0;
myMRIwrite(mriBlockIdx,[outputDir filesep 'IndexVolume.mgz']);
save([outputDir filesep 'IndexNamesAndHeaders.mat'],'blocknames','vox2ras0s'); 


% Finally, we resample the MRI to the space of the blocks
disp('*****************************************************');
disp('*   Resampling MRI to create  virtual MRI blocks    *');
disp('*****************************************************');

cmds=cell([1,Nblocks]);
for i=1:Nblocks
    outputFile=[outputDir filesep d(i).name(1:end-10-LS) '.mri.reg.mgz'];
    refFile=[outputDir filesep d(i).name(1:end-10-LS) '.gray.reg.mgz'];
    cmds{i}=['mri_convert '  inputMRI ' ' outputFile ' -odt float -rl ' refFile];
end


[s,r] = system(cmds{1});
if s>0
    disp('mri convert not found; you need to run these commands externally');
    for i=1:Nblocks
        disp(cmds{i});
    end
else
    for i=2:Nblocks
        disp(['Block ' num2str(i) ' of ' num2str(Nblocks)]);
        system(cmds{i});
    end
end

% And also the masks, if provided!
if ischar(MRI_SEG_OR_THRESHOLD)
    cmds=cell([1,Nblocks]);
    for i=1:Nblocks
        outputFile=[outputDir filesep d(i).name(1:end-10-LS) '.aparc+aseg.reg.mgz'];
        refFile=[outputDir filesep d(i).name(1:end-10-LS) '.gray.reg.mgz'];
        cmds{i}=['mri_convert '  MRI_SEG_OR_THRESHOLD ' ' outputFile ' -rt nearest -odt float -rl ' refFile];
    end
    
    if s>0
        for i=1:Nblocks
            disp(cmds{i});
        end
    else
        for i=1:Nblocks
            disp(['Segmentation of block ' num2str(i) ' of ' num2str(Nblocks)]);
            system(cmds{i});
        end
    end
end


system(['rm -rf ' tempdir]);

disp('All done!');
toc


