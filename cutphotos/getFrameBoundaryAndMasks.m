% This function get a (typically smoothed) gradient magnitude image derived
% from a cut photo, and detects the plexiglass frame. It also requires the 
% corners detected by the SVM. It's a bit ugly but fairly works well. 
% It essentially finds sets of parallel lines that are ~1 frame width apart 
%       Note: Good values for number_of_points and delta_norm_probe are 
%       100 and 0.1, respectively
% [insideCornersCoords, outsideCornersCoords, MaskInside, MaskOutside, MaskFrame = 
%   getFrameBoundaryAndMasks(GRADIENT,corners_from_SVM,number_of_points,...
%       delta_norm_probe,typical_frame_width,rescalingFactor,inside_corners_optional)
function [cornersIn,cornersOut,Min,Mout,Mframe]=getFrameBoundaryAndMasks(G,corners,NP,INCNORM,TYP_FRAME_W,rescalingFactor,providedCornersIn)

NNORM=1.5*TYP_FRAME_W/rescalingFactor/INCNORM;
mu=TYP_FRAME_W/rescalingFactor/2/INCNORM;
sigma=TYP_FRAME_W/rescalingFactor/3/INCNORM;
prior=exp(-([0:NNORM-1]-mu).^2/(2*sigma*sigma));

SlopesIn=zeros(1,4);
SlopesOut=zeros(1,4);
InterceptsIn=zeros(1,4);
InterceptsOut=zeros(1,4);

for c=1:4
    p1=corners(:,c);
    if c<4, p2=corners(:,c+1); else, p2=corners(:,1); end
    tmp1=p1(1):(p2(1)-p1(1))/(NP-1):p2(1); if length(tmp1)<2, tmp1=p1(1)*ones(1,NP); end
    tmp2=p1(2):(p2(2)-p1(2))/(NP-1):p2(2); if length(tmp2)<2, tmp2=p1(2)*ones(1,NP); end
    ps=[ tmp1 ; tmp2 ];
    cp=cross([p2-p1;0]',[0 0 1]);
    nIn=cp(1:2)/norm(cp(1:2));
    nOut=-nIn;
    psShiftedIn=zeros(size(ps));
    if nargin<=6 % normal fit
        for j=1:NP
            aux=repmat(ps(:,j),[1 NNORM]) +  repmat([0:NNORM-1]*INCNORM,[2 1]).*repmat(nIn',[1 NNORM]);
            Gint=interp2(G,aux(2,:),aux(1,:));
            metric=prior.*Gint;
            [~,idx]=max(metric);
            psShiftedIn(:,j)=aux(:,idx);
        end
    else  % we are given the solution by the inner corners
        pp1=providedCornersIn(:,c);
        if c<4, pp2=providedCornersIn(:,c+1); else, pp2=providedCornersIn(:,1); end
        
        nC=(pp2-pp1)/norm(pp2-pp1);
        t=(nC(2)*(pp1(1)-ps(1,:))-nC(1)*(pp1(2)-ps(2,:)))/(nC(2)*nIn(1)-nC(1)*nIn(2));
        psShiftedIn(1,:)=ps(1,:)+t*nIn(1);
        psShiftedIn(2,:)=ps(2,:)+t*nIn(2);
    end

    psShiftedOut=zeros(size(ps));
    for j=1:NP
        aux=repmat(ps(:,j),[1 NNORM]) +  repmat([0:NNORM-1]*INCNORM,[2 1]).*repmat(nOut',[1 NNORM]);
        Gint=interp2(G,aux(2,:),aux(1,:));
        metric=prior.*Gint;
        [~,idx]=max(metric);
        psShiftedOut(:,j)=aux(:,idx);
    end
    
    % L1 fit: make sure slope is close to 0 (rather than Inf)
    if mod(c,2)==0  % predict column from row
        params=robustfit([psShiftedIn(1,:)' zeros(NP,1); psShiftedOut(1,:)' ones(NP,1)],[psShiftedIn(2,:)'; psShiftedOut(2,:)' ]);
    else  % predict row from column
        params=robustfit([psShiftedIn(2,:)' zeros(NP,1); psShiftedOut(2,:)' ones(NP,1)],[psShiftedIn(1,:)'; psShiftedOut(1,:)' ]);
    end

    SlopesIn(c)=params(2);
    InterceptsIn(c)=params(1);
    SlopesOut(c)=params(2);
    InterceptsOut(c)=params(1)+params(3);
end

% Now, compute inner and outter corners analitically
cornersIn=zeros(size(corners));
cornersOut=zeros(size(corners));
for k=1:4
    if k>1, k1=k-1; else, k1=4; end
    k2=k;
    if mod(k,2)==0
        cIn=(SlopesIn(k2)*InterceptsIn(k1)+InterceptsIn(k2))/(1-SlopesIn(k1)*SlopesIn(k2));
        rIn=SlopesIn(k1)*cIn+InterceptsIn(k1);
        cOut=(SlopesOut(k2)*InterceptsOut(k1)+InterceptsOut(k2))/(1-SlopesOut(k1)*SlopesOut(k2));
        rOut=SlopesOut(k1)*cIn+InterceptsOut(k1);
    else
        cIn=(SlopesIn(k1)*InterceptsIn(k2)+InterceptsIn(k1))/(1-SlopesIn(k1)*SlopesIn(k2));
        rIn=SlopesIn(k2)*cIn+InterceptsIn(k2);
        cOut=(SlopesOut(k1)*InterceptsOut(k2)+InterceptsOut(k1))/(1-SlopesOut(k1)*SlopesOut(k2));
        rOut=SlopesOut(k2)*cIn+InterceptsOut(k2);
    end
    cornersIn(1,k)=rIn;
    cornersIn(2,k)=cIn;
    cornersOut(1,k)=rOut;
    cornersOut(2,k)=cOut;
end

% Create mask for frame
[RR,CC]=ndgrid(1:size(G,1),1:size(G,2));

Min=ones(size(G,1),size(G,2))>0;
Min=Min & RR>(SlopesIn(1)*CC+InterceptsIn(1));
Min=Min & CC<(SlopesIn(2)*RR+InterceptsIn(2));
Min=Min & RR<(SlopesIn(3)*CC+InterceptsIn(3));
Min=Min & CC>(SlopesIn(4)*RR+InterceptsIn(4));

Mout=ones(size(G,1),size(G,2))>0;
Mout=Mout & RR>(SlopesOut(1)*CC+InterceptsOut(1));
Mout=Mout & CC<(SlopesOut(2)*RR+InterceptsOut(2));
Mout=Mout & RR<(SlopesOut(3)*CC+InterceptsOut(3));
Mout=Mout & CC>(SlopesOut(4)*RR+InterceptsOut(4));

Mframe=Mout & ~Min;




