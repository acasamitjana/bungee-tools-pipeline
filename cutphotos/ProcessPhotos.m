% This is the main script to go over the photos of whole or blocked slices.
% You provide a directory with the photos as input, and it will create a 
% subdirectory named "cornersAndMasks" with mat files that store the 
% coordinates of the corners, the masks for the different blocks, etc.
% Photos of whole slices are processed the same way as photos of whole blocks
% simply by setting the number of blocks to 1.
clear
close all
clc 

addpath(genpath(['..' filesep 'functions' filesep]));

matFile='SVM_resc6_deriv_3_scales_0369_matlab_2018a.mat';
TYP_FRAME_W=150; % (very) rough estimate of the width of the frame in pixels
MIN_AREA_COMP=3000; % minimum area of chunk of tissue

% load classifier
global rescalingFactor;
global POST;
global Ir;
load(matFile,'rescalingFactor','featMaxDerivOrder','featScales','SVMModel');


inputDirectory = uigetdir('','Please select input directory:');
outputDirectory = [inputDirectory filesep 'cornersAndMasks' filesep];

if exist(outputDirectory,'dir')==0
    mkdir(outputDirectory);
end

ddd=dir([inputDirectory filesep '*.JPG']);

for iii=1:length(ddd)
    
    outputMatFile=[outputDirectory filesep ddd(iii).name(1:end-4) '.mat'];
    
    if exist(outputMatFile,'file')>0
        disp(['Skipping   ' ddd(iii).name ' : image ' num2str(iii)  ' of ' num2str(length(ddd)) ' (output already there)']);
    else
        disp(['Working on ' ddd(iii).name ' : image ' num2str(iii)  ' of ' num2str(length(ddd))]);
        
        % Run classifier
        disp('  Reading in image and detecting corners');
        I=imread([inputDirectory filesep ddd(iii).name]);
        Ir=imresize(I,1/rescalingFactor);
        feats=computeGaussianScaleSpaceFeatures(Ir,featMaxDerivOrder,featScales);
        feats=reshape(feats,[size(feats,1)*size(feats,2) size(feats,3)]);
        labs=SVMModel.predict(feats);
        Mr=reshape(uint8(255*double(labs>0)),[size(Ir,1) size(Ir,2)]);
        Mr(1:2,:)=0; Mr(end-1:end,:)=0; Mr(:,1:2)=0; Mr(:,end-1:end)=0;
        
        % Find four corners
        corners=zeros(2,4);
        [BW,n]=bwlabel(imdilate(Mr>0,strel('disk',ceil(20/rescalingFactor))));
        
        areas=zeros(1,n);
        for nn=1:n
            areas(nn)=sum(BW(:)==nn);
        end
        [~,idx]=sort(areas,'descend');
        
        for j=1:4
            aux=BW==idx(j);
            [r,c]=find(aux);
            
            corners(:,j)=[mean(r); mean(c)];
        end
        
        % Are we happy with the detected corners?
        close all
        figure('units','normalized','outerposition',[0 0 1 1])
        imagesc(Ir), axis equal, axis off
        hold on, plot(corners(2,:),corners(1,:),'r.','markersize',40); hold off
        aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
        button = questdlg('Are you happy with the detected corners?',' ','Yes','No','Yes');
        if strcmp(button,'No')
            ready=0;
            while ready==0
                imagesc(Ir); axis equal, axis off
                aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
                button = questdlg('Please click on the middle of the black square on all four corners',' ','OK','OK');
                for c=1:4
                    aux=ginput(1);
                    corners(:,c)=flipud(aux');
                    hold on, plot(aux(1),aux(2),'r.','markersize',40); hold off
                end
                button = questdlg('Are you happy with your selection?',' ','Yes','No','Yes');
                if strcmp(button,'Yes')
                    ready=1;
                end
            end
        end
        
        
        % Reorder to: NW,NE,SE,SW (Hungarian algorithm; a bit of an overkill but
        % whatever...)
        NWness=corners(1,:)+corners(2,:);
        NEness=corners(1,:)-corners(2,:);
        SEness=-NWness;
        SWness=-NEness;
        [idx,cost] = munkres([NWness; NEness; SEness; SWness]);
        corners=corners(:,idx);
        
        
        % Next step: fit straight lines
        disp('  Detecting frame boundary');
        G=zeros(size(Mr));
        for c=1:size(Ir,3)
            aux=imfilter(double(Ir(:,:,c)),[-1 0 1]);
            G=G+aux.*aux;
            aux=imfilter(double(Ir(:,:,c)),[-1 0 1]');
            G=G+aux.*aux;
        end
        G=sqrt(G);
        G(1,:)=0; G(end,:)=0; G(:,1)=0; G(:,end)=0;
        sigmaG=10/rescalingFactor;
        G=imfilter(G,fspecial('gaussian',(1+2*ceil(3*sigmaG))*ones(1,2),sigmaG));
        
        NP=100;
        INCNORM=0.1;
        [cornersIn,cornersOut,Min,Mout,Mframe]=getFrameBoundaryAndMasks(G,corners,NP,INCNORM,TYP_FRAME_W,rescalingFactor);
        
        imagesc(Ir), axis equal, axis off
        idc=[1 2 3 4 1];
        hold on, plot(cornersIn(2,idc),cornersIn(1,idc),'r-','linewidth',3); hold off
        hold on, plot(cornersOut(2,idc),cornersOut(1,idc),'r-','linewidth',3); hold off
        aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
        button = questdlg('Are you happy with the detected frame?',' ','Yes','No','Yes');
        
        % If unhappy, specify manually
        cornersInManual=[];
        if strcmp(button,'No')
            ready=0;
            while ready==0
                imagesc(Ir); axis equal, axis off
                aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
                button = questdlg('Please click on the four inner corners',' ','OK','OK');
                for c=1:4
                    aux=ginput(1);
                    cornersIn(:,c)=flipud(aux');
                    hold on, plot(aux(1),aux(2),'r.','markersize',40); hold off
                end
                button = questdlg('Are you happy with your selection?',' ','Yes','No','Yes');
                if strcmp(button,'Yes')
                    NWness=cornersIn(1,:)+cornersIn(2,:);
                    NEness=cornersIn(1,:)-cornersIn(2,:);
                    SEness=-NWness;
                    SWness=-NEness;
                    [idx,cost] = munkres([NWness; NEness; SEness; SWness]);
                    cornersInManual=cornersIn(:,idx);
                    [cornersIn,cornersOut,Min,Mout,Mframe]=getFrameBoundaryAndMasks(G,corners,NP,INCNORM,TYP_FRAME_W,rescalingFactor,cornersInManual);
                    
                    imagesc(Ir); axis equal, axis off
                    idc=[1 2 3 4 1];
                    hold on, plot(cornersIn(2,idc),cornersIn(1,idc),'r-','linewidth',3); hold off
                    hold on, plot(cornersOut(2,idc),cornersOut(1,idc),'r-','linewidth',3); hold off
                    aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
                    button = questdlg('Are you happy with the detected frame (at least the innder side)?',' ','Yes','No','Yes');
                    if strcmp(button,'Yes')
                        ready=1;
                    end
                end
            end
        end
        
        
        % Get distribution of background
        disp('  Calculating intensity distributions of foreground');
        M=~(imdilate(Mout>0,strel('disk',2)));
        data=zeros(3,sum(M(:)));
        for c=1:3
            aux=Ir(:,:,c);
            data(c,:)=double(aux(M));
        end
        mu=mean(data,2);
        SigmaData=cov(data');
        nData=size(data,2);
        nPrior=nData;
        SigmaPrior=10*diag(diag(SigmaData));
        Sigma=(nData*SigmaData+nPrior*SigmaPrior)/(nPrior+nData);
        Prec=inv(Sigma);
        
        % Compute likelihood of background
        POST=zeros(size(Min));
        
        mask=Min;
        data=zeros(3,sum(mask(:)));
        for c=1:3
            aux=Ir(:,:,c);
            data(c,:)=double(aux(mask));
        end
        aux=bsxfun(@minus,data,mu);
        mahalanobisSq=sum(aux.*(Prec*aux));
        
        if 1  % I find it easier / better to thrshold mahalanobis directly, rather than EM...
            
            mahalanobis=sqrt(mahalanobisSq);
            POST(Min)=mahalanobis;
        else
            lhood=zeros(2,size(data,2));
            lhood(2,:)=1/sqrt(det(2*pi*Sigma))*exp(-0.5* mahalanobisSq  );
            
            % Foreground: EM
            npdensity=ones(3,256)/256;
            readyEM=0;
            costPrev=Inf;
            its=0;
            while readyEM==0
                its=its+1;
                
                % E step
                lhood(1,:)=1;
                for c=1:3
                    aux=Ir(:,:,c);
                    data=round(aux(Min));
                    lhood(1,:)=lhood(1,:).*npdensity(c,1+data);
                end
                normalizer=sum(lhood);
                cost=-sum(log(normalizer))/length(data);
                post=lhood./repmat(normalizer,[2 1]);
                
                % Mstep
                w=post(1,:)/(eps+sum(post(1,:)));
                for c=1:3
                    aux=Ir(:,:,c);
                    npdensity(c,:)=ksdensity(double(aux(Min)),0:255,'weights',w);
                    npdensity(c,:)=npdensity(c,:)/sum(npdensity(c,:));
                end
                
                if costPrev-cost<1e-4
                    readyEM=1;
                elseif its>50
                    readyEM=1;
                else
                    costPrev=cost;
                end
            end
            
            POST(Min)=post(1,:);
        end
        
        
        % Set threshold interactively (after filling masks)
        global mahalanobisThreshold;
        close all
        gui=GUIthreshold();
        set(gui,'Toolbar','figure');
        set(gui,'Menubar','none');
        button = questdlg('Adjust threshold if necessary to get right segmentation',' ','OK','OK');
        uiwait(gui);
        
        [BW,n]=bwlabel(imopen(imfill(POST>mahalanobisThreshold,'holes'),strel('disk',round(35/rescalingFactor))));
        figure('units','normalized','outerposition',[0 0 1 1])
        subplot(1,2,1), imagesc(Ir),axis equal, axis off;
        aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
        subplot(1,2,2), imagesc(BW), axis equal, axis off
        prompt = {'How many blocks are there in the image?'};
        defaultans = {'1'};
        answer = newid(prompt,' ',1,defaultans);
        nblocks=str2double(answer{1});
        BW2=zeros(size(BW));
        for b=1:nblocks
            title(['Please draw rectangle on block ' num2str(b)]);
            rect=imrect();
            maskRect=rect.createMask;
            labels=unique(BW(maskRect));
            rectPos=round(rect.getPosition);
            labels(end+1)=BW(rectPos(2),rectPos(1));
            labels=unique(labels(labels>0));
            for l=1:length(labels)
                BW2(BW==labels(l))=b;
            end
        end
        subplot(1,2,1), imagesc(Ir); axis equal, axis off
        aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
        subplot(1,2,2), imagesc(BW2),
        for b=1:nblocks
            [rr,cc]=find(BW2==b);
            text(mean(cc),mean(rr),num2str(b));
        end
        title('Labeled blocks'), axis equal, axis off
        
        button = questdlg('Are you happy with the labeled blocks?',' ','Yes','No','Yes');
        if strcmp(button,'No')
            ready=0;
            while ready==0
                subplot(1,2,1), imagesc(Ir), axis equal, axis off;
                aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
                subplot(1,2,2), imagesc(BW), axis equal, axis off
                BW2=zeros(size(BW));
                for b=1:nblocks
                    title(['Please draw rectangle on block ' num2str(b)]);
                    rect=imrect();
                    maskRect=rect.createMask;
                    labels=unique(BW(maskRect));
                    rectPos=round(rect.getPosition);
                    labels(end+1)=BW(rectPos(2),rectPos(1));
                    labels=unique(labels(labels>0));
                    for l=1:length(labels)
                        BW2(BW==labels(l))=b;
                    end
                end
                subplot(1,2,1), imagesc(Ir), axis equal, axis off
                aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
                subplot(1,2,2), imagesc(BW2), axis equal, axis off
                for b=1:nblocks
                    [rr,cc]=find(BW2==b);
                    text(mean(cc),mean(rr),num2str(b));
                end
                title('Labeled blocks'), axis equal, axis off
                
                button = questdlg('Are you happy with the labeled blocks?',' ','Yes','No','Yes');
                if strcmp(button,'Yes')
                    ready=1;
                end
            end
        end
        BW=BW2;
        
        
        % Done! Display stuff, and see if result is OK
        subplot(1,2,1), imagesc(Ir), axis equal, axis off;
        aux=ddd(iii).name; aux(aux=='_')='-'; title(aux);
        subplot(1,2,2),
        Iaux=Ir;
        for b=1:nblocks
            aux=(BW==b);
            [rr,cc]=find(aux);
            text(mean(cc),mean(rr),num2str(b));
            Mbound=aux & ~imerode(aux,strel('disk',3));
            aux=Iaux(:,:,1); aux(Mbound)=0; Iaux(:,:,1)=aux;
            aux=Iaux(:,:,2); aux(Mbound)=255; Iaux(:,:,2)=aux;
            aux=Iaux(:,:,3); aux(Mbound)=0; Iaux(:,:,3)=aux;
        end
        imagesc(Iaux), axis equal, axis off
        idc=[1 2 3 4 1];
        hold on, plot(cornersIn(2,idc),cornersIn(1,idc),'r-','linewidth',3); hold off
        hold on, plot(cornersOut(2,idc),cornersOut(1,idc),'r-','linewidth',3); hold off
        for b=1:nblocks
            [rr,cc]=find(BW==b);
            text(mean(cc),mean(rr),num2str(b),'color',[0 1 0]);
        end
        title(['Processed image (' num2str(nblocks) ' blocks)']);
        
        button = questdlg('Are you happy with the final results?',' ','Yes','No','Yes');
        
        if strcmp(button,'Yes')
            save(outputMatFile,'cornersIn','cornersOut','BW','Mframe','rescalingFactor');
        end
        
        close all
    end
end
disp('All files in directory done!');







