function varargout = GUIthreshold(varargin)
% GUITHRESHOLD MATLAB code for GUIthreshold.fig
%      GUITHRESHOLD, by itself, creates a new GUITHRESHOLD or raises the existing
%      singleton*.
%
%      H = GUITHRESHOLD returns the handle to a new GUITHRESHOLD or the handle to
%      the existing singleton*.
%
%      GUITHRESHOLD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUITHRESHOLD.M with the given input arguments.
%
%      GUITHRESHOLD('Property','Value',...) creates a new GUITHRESHOLD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUIthreshold_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUIthreshold_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUIthreshold

% Last Modified by GUIDE v2.5 07-Aug-2017 22:18:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUIthreshold_OpeningFcn, ...
                   'gui_OutputFcn',  @GUIthreshold_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUIthreshold is made visible.
function GUIthreshold_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUIthreshold (see VARARGIN)

% Choose default command line output for GUIthreshold
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUIthreshold wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global Ir;
global POST;
global rescalingFactor;
axes(handles.axes1);
imshow(Ir);
axes(handles.axes2);
imshow(imopen(imfill(POST>chi2inv(0.99,3),'holes'),strel('disk',round(35/rescalingFactor))));
set(handles.slider1,'Value',chi2inv(0.99,3)/30);


% --- Outputs from this function are returned to the command line.
function varargout = GUIthreshold_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global POST;
global rescalingFactor;
global mahalanobisThreshold;
axes(handles.axes2);
sliderValue=get(handles.slider1,'Value'); % Mahalanobis = 30 is quite a lot...
mahalanobisThreshold=30*sliderValue;
imshow(imopen(imfill(POST>mahalanobisThreshold,'holes'),strel('disk',round(35/rescalingFactor))));


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
global mahalanobisThreshold;
sliderValue=chi2inv(0.99,3)/30;
mahalanobisThreshold=sliderValue*30;
set(hObject,'Value',sliderValue); 


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

close all
