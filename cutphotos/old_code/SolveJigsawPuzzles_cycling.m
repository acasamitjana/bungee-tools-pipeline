clear

addpath rigidTransform

rootDir = '~/exvivoAcq/P57-16';
blockRoot = [rootDir, filesep, 'Blocks/'];
wholeRoot = [rootDir, filesep, 'Whole Slice/'];
lsBlock = dir(blockRoot);
lsWhole = dir(wholeRoot);

RESOLUTION=0.2; % mm
DEBUG=0;
MAXERR=10;

for j=3:length(lsBlock)
    % Go over images
    blockDir=[blockRoot,lsBlock(j).name];
    [pref,rest]=strtok(lsBlock(j).name,'_');
    [~,suff]=strtok(rest,'_');
    wholeDir = [wholeRoot,pref,suff];
    disp(blockDir);
    disp(wholeDir);
    d=dir(blockDir);
    for i=1:length(d)
        disp(['Working on file ' num2str(i) ' of ' num2str(length(d)) ' in input directory']);
        isImage=0;
        try
            % Image with whole slice
            BI=imread([blockDir, filesep, d(i).name]);
            isImage=1;
        catch
            disp('  Not an image');
        end
        
        if isImage
            
            % Image with blocks
            idx=strfind(d(i).name,'Blocks');
            name=d(i).name([1:idx-1 idx+7:end]);
            WI=imread([wholeDir filesep name]);
            
            % Load annotations for blocks
            load([blockDir filesep 'cornersAndMasks' filesep d(i).name(1:end-4) '.mat']);
            BWb=BW; MframeB=Mframe; CIb=cornersIn; COb=cornersOut; rfB=rescalingFactor;
            
            % Load annotations for whole slice
            load([wholeDir filesep 'cornersAndMasks' filesep  name(1:end-4) '.mat']);
            BWw=BW; MframeW=Mframe; CIw=cornersIn; COw=cornersOut; rfW=rescalingFactor;
            
            % Resize and resample
            if norm(CIb(:,1)-CIb(:,2)) > norm(CIb(:,1)-CIb(:,4))  % landscape
                SIZ=[90 120]/RESOLUTION; % it's actually 120x90mm
            else  % portrait
                SIZ=[120 90]/RESOLUTION;
            end
            cornersTarget=[1 1; 1 SIZ(2); SIZ(1) SIZ(2); SIZ(1) 1]';
            A=[CIb(2,:)' CIb(1,:)' zeros(4,2) ones(4,1) zeros(4,1);
                zeros(4,2) CIb(2,:)' CIb(1,:)'  zeros(4,1) ones(4,1)];
            b=[cornersTarget(2,:)'; cornersTarget(1,:)'];
            aux=A\b;
            tform=affine2d([aux(1) aux(3) 0; aux(2) aux(4) 0; aux(5) aux(6) 1]);
            BIw = imwarp(imresize(BI,1/rfB),tform,'OutputView',imref2d(SIZ));
            BMw = imwarp(BWb,tform,'OutputView',imref2d(SIZ));
            
            if norm(CIw(:,1)-CIw(:,2)) > norm(CIw(:,1)-CIw(:,4))  % landscape
                SIZ=[90 120]/RESOLUTION; % it's actually 120x90mm
            else  % portrait
                SIZ=[120 90]/RESOLUTION; % it's actually 120x90mm
            end
            cornersTarget=[1 1; 1 SIZ(2); SIZ(1) SIZ(2); SIZ(1) 1]';
            A=[CIw(2,:)' CIw(1,:)' zeros(4,2) ones(4,1) zeros(4,1);
                zeros(4,2) CIw(2,:)' CIw(1,:)'  zeros(4,1) ones(4,1)];
            b=[cornersTarget(2,:)'; cornersTarget(1,:)'];
            aux=A\b;
            tform=affine2d([aux(1) aux(3) 0; aux(2) aux(4) 0; aux(5) aux(6) 1]);
            WIw = imwarp(imresize(WI,1/rfW),tform,'OutputView',imref2d(SIZ));
            WMw = imwarp(BWw,tform,'OutputView',imref2d(SIZ));
            
            % Get SIFT features
            pointsB = detectSURFFeatures(rgb2gray(BIw),'NumOctaves',5,'MetricThreshold',10);
            [fB,vptsB] = extractFeatures(rgb2gray(BIw),pointsB);
            pointsW = detectSURFFeatures(rgb2gray(WIw),'NumOctaves',5,'MetricThreshold',10);
            [fW,vptsW] = extractFeatures(rgb2gray(WIw),pointsW);
            
            % Mask with whole slice WMw
            idx=find(WMw(sub2ind(size(WMw),round(pointsW.Location(:,2)),round(pointsW.Location(:,1)))));
            pointsW=pointsW(idx);
            fW=fW(idx,:);
            vptsW=vptsW(idx);
            
            % For each block, mask with BMw and match
            nlab=max(round(BMw(:)));
            WARPED=zeros(size(WIw),'uint8');
            WARPED(:)=255;
            tforms=cell(1,nlab);
            for l=1:nlab
                % mask
                idx=find(BMw(sub2ind(size(BMw),round(pointsB.Location(:,2)),round(pointsB.Location(:,1))))==l);
                pointsBx=pointsB(idx);
                fBx=fB(idx,:);
                vptsBx=vptsB(idx);
                
                % match
                indexPairs = matchFeatures(fBx,fW) ;
                matchedPoints1 = vptsBx(indexPairs(:,1));
                matchedPoints2 = vptsW(indexPairs(:,2));
                
                % tform = estimateGeometricTransform(matchedPoints1,matchedPoints2,'similarity','MaxNumTrials',10000,'Confidence',99.9999);
                aux=estimateRigidTransformation( matchedPoints1.Location, matchedPoints2.Location, 1000, 0.1 );
                tform=affine2d(aux');
                
                % now refine
                aux=[matchedPoints1.Location ones(length(indexPairs),1)]*tform.T;
                aux=aux(:,1:2);
                err=aux-matchedPoints2.Location;
                errMag=sqrt(sum(err.*err,2));
                ok=find(errMag<MAXERR);
                
                tform = estimateGeometricTransform(matchedPoints1(ok),matchedPoints2(ok),'similarity','MaxNumTrials',10000,'Confidence',99.9999);
                %             aux=estimateRigidTransformation( matchedPoints1(ok).Location, matchedPoints2(ok).Location, 1000, 0.1 );
                %             tform=affine2d(aux');
                
                aux = imwarp(BIw,tform,'OutputView',imref2d(size(WIw)));
                auxM = repmat(imwarp(BMw==l,tform,'OutputView',imref2d(size(WIw))),[1 1 3]);
                WARPED(auxM)=aux(auxM);
                
                tforms{l}=tform;
                
            end
            
            save([outputdir filesep name(1:end-4) '.transforms.mat'],'tforms','BIw','BMw','RESOLUTION');
            imwrite(WIw,[outputdir filesep name(1:end-4) '.warped.target.png']);
            imwrite(WARPED,[outputdir filesep name(1:end-4) '.warped.mosaic.png']);
            
            if DEBUG
                close all, figure,
                for jkjk=1:10
                    imshow(WIw), pause(0.5), imshow(WARPED), pause(0.5);
                end
            end
            
        end
        
        disp(' ');
    end
end