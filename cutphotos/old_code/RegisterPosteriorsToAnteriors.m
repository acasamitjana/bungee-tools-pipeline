% registers anterior faces of whole slices to the posterior faces
%  of the neighboring slices. I suspect it's not very useful - I'd 
% rather register to mosaic of initialized blockfaces *directly*
clear
clc

addpath rigidTransform

 disp('Please select input directory for images of whole slices:');
inputdir = uigetdir('','Please select input directory for images of whole slices:');
inputdir(end+1)=filesep;
% inputdir='~/Downloads/Whole Slice/';

maskdir=[inputdir filesep 'cornersAndMasks' filesep];


RESOLUTION=0.2; % mm

outputdir=[inputdir filesep 'registrations' filesep];

if exist(outputdir,'dir')==0
    mkdir(outputdir);
end

% We will register from anterior slice (posterior face) to posterior slice (anterior face)
d=dir([inputdir filesep '*[P].JPG']);
f=find(d(1).name=='_');
blockName=d(1).name(1:f-1);

for i=1:length(d)
    
    disp(['Working on image ' num2str(i) ' of ' num2str(length(d))]);
    
    f1=find(d(i).name=='_');
    f2=find(d(i).name=='[');
    ap=d(i).name(f1+1);
    n=str2double(d(i).name(f1+2:f2-1));
    
    % input and target for the registration
    input=[inputdir filesep d(i).name];
    if ap=='A' && n>1
        apt='A';
        nt=n-1;
    elseif ap=='A' && n==1
        apt='P';
        nt=1;
    else
        apt='P';
        nt=n+1;
    end
    targetName=[blockName '_' apt num2str(nt) '[A].JPG'];
    target=[inputdir filesep targetName];
    
    
    if exist(target,'file')  % Last posterior block has to target to register to
        
        % Load annotations for both images
        II=imread(input);
        load([maskdir filesep  d(i).name(1:end-4) '.mat']);
        BWi=BW; MframeI=Mframe; CIi=cornersIn; COi=cornersOut; rfI=rescalingFactor;
        
        TI=imread(target);
        load([maskdir filesep  targetName(1:end-4) '.mat']);
        BWt=BW; MframeT=Mframe; CIt=cornersIn; COt=cornersOut; rfT=rescalingFactor;
        
        clear BW Mframe cornersIn cornersOut rescalingFactor
        
        % Resize and resample (it's a bit stupid, I'm just repeating what I
        % did in SolveJigsawPuzzles.m, but whatever... careful if I change
        % target resolution, though!
        if norm(CIi(:,1)-CIi(:,2)) > norm(CIi(:,1)-CIi(:,4))  % landscape
            SIZ=[90 120]/RESOLUTION; % it's actually 120x90mm
        else  % portrait
            SIZ=[120 90]/RESOLUTION; % it's actually 120x90mm
        end
        cornersTarget=[1 1; 1 SIZ(2); SIZ(1) SIZ(2); SIZ(1) 1]';
        A=[CIi(2,:)' CIi(1,:)' zeros(4,2) ones(4,1) zeros(4,1);
            zeros(4,2) CIi(2,:)' CIi(1,:)'  zeros(4,1) ones(4,1)];
        b=[cornersTarget(2,:)'; cornersTarget(1,:)'];
        aux=A\b;
        tform=affine2d([aux(1) aux(3) 0; aux(2) aux(4) 0; aux(5) aux(6) 1]);
        IIw = imwarp(imresize(II,1/rfI),tform,'OutputView',imref2d(SIZ));
        IMw = imwarp(BWi,tform,'OutputView',imref2d(SIZ));
        
        % Resize and resample
        if norm(CIt(:,1)-CIt(:,2)) > norm(CIt(:,1)-CIt(:,4))  % landscape
            SIZ=[90 120]/RESOLUTION; % it's actually 120x90mm
        else  % portrait
            SIZ=[120 90]/RESOLUTION; % it's actually 120x90mm
        end
        cornersTarget=[1 1; 1 SIZ(2); SIZ(1) SIZ(2); SIZ(1) 1]';
        A=[CIt(2,:)' CIt(1,:)' zeros(4,2) ones(4,1) zeros(4,1);
            zeros(4,2) CIt(2,:)' CIt(1,:)'  zeros(4,1) ones(4,1)];
        b=[cornersTarget(2,:)'; cornersTarget(1,:)'];
        aux=A\b;
        tform=affine2d([aux(1) aux(3) 0; aux(2) aux(4) 0; aux(5) aux(6) 1]);
        TIw = imwarp(imresize(TI,1/rfT),tform,'OutputView',imref2d(SIZ));
        TMw = imwarp(BWt,tform,'OutputView',imref2d(SIZ));
        TIw=fliplr(TIw);
        TMw=fliplr(TMw);
        
        
        
        % MATLAB registration
        [optimizer,metric] = imregconfig('multimodal');
        optimizer.InitialRadius = 0.009;
        optimizer.Epsilon = 1.5e-4;
        optimizer.GrowthFactor = 1.01;
        optimizer.MaximumIterations = 300;
        
        moving=rgb2gray(IIw); moving(IMw==0)=nan;
        fixed=rgb2gray(TIw); fixed(TMw==0)=nan;
        tform = imregtform(moving,fixed, 'rigid', optimizer, metric);
        WARPED = imwarp(IIw,tform,'OutputView',imref2d(size(TIw)));
        
        again='Yes';
        close all, figure(1)
        while strcmp(again,'Yes');
            for nview=1:5
                imshow(TIw), pause(0.5);
                imshow(WARPED), pause(0.5);
            end
            again = questdlg('Want to see registration again?',' ','Yes','No','Yes');
        end
        close all
        
        happy = questdlg('Are you happy with the registration?',' ','Yes','No','Yes');
        while strcmp(happy,'No');
            uiwait(msgbox('Please select a minimum of 3 corresponding landmarks and close the window when ready','modal'));
            close all
            [movP,fixP] = cpselect(IIw,TIw,'Wait',true);
            close all
            
            np=size(movP,1);
            if np<3
                uiwait(msgbox(['I said at least 3 points; you only provided ' num2str(np)],'modal'));
            else
                 aux=computeRigidTransformation( movP, fixP );
                 tform=affine2d(aux');
                 WARPED = imwarp(IIw,tform,'OutputView',imref2d(size(TIw)));
                 
                 again='Yes';
                 close all, figure(1)
                 while strcmp(again,'Yes');
                     for nview=1:5
                         imshow(TIw), pause(0.5);
                         imshow(WARPED), pause(0.5);
                     end
                     again = questdlg('Want to see registration again?',' ','Yes','No','Yes');
                 end
                 close all
        
                 happy = questdlg('Are you happy with the registration?',' ','Yes','No','Yes');
            end
        end
     
        save([outputdir filesep d(i).name(1:end-4) '.mat'],'tform');
        imwrite(TIw,[outputdir filesep d(i).name(1:end-4) '.target.png']);
        imwrite(WARPED,[outputdir filesep d(i).name(1:end-4) '.warped.png']);
         
    end
    
end



