# CUTPHOTOS
> Functions to process photographs from the brain disection ("cut photos").

There are two sets of functions here: for users and for developers

## USERS 

ProcessPhoto.m

This is the main script to go over the photos of whole or blocked slices.
You provide a directory with the photos as input, and it will create a 
subdirectory named "cornersAndMasks" with mat files that store the 
coordinates of the corners, the masks for the different blocks, etc.
Photos of whole slices are processed the same way as photos of whole blocks
simply by setting the number of blocks to 1.



RegisterCutfaceToBlockfaceAllRegions.m

This script registers the face of the block to the top/bottom of the 
corresponding blockface photo stack. It typically requires a fair amount of  
manual interaction. But you only need it to do it once, it's not too bad.
This script unifies what used to be 3 separate scripts for cerebrum,
brainstem and cerebellum, each with its peculiarities, as the most posterior/
lateral block in the cerebrum / cerebellum is sectioned in the opposite
direction - this is because the only flat side of the block has to face down 
in the cassette


SolveJigsawPuzzles.m

This script solves the jigsaw puzzle for the autopsy photographs (blocks
vs whole slices). It doesn't take long, so I leave DEBUG=1 so you can check 
whether the outputs are reasonable. It relies on SIFT and is pretty robust




## DEVELOPERS 

TrainFrameCornerClassifier: 

This scripts trains the SVM classifier to find the corners of the 
plexiglass frame in the photographs, painted in black.  You simply put a 
bunch of images (*.jpg) and corresponding segmentations  (*.mask.png) in a 
directory and it will train the classifier, which is saved in a .mat file.  



## AUXILIARY FUNCTIONS / PACKAGES

F = computeGaussianScaleSpaceFeatures(IMAGE,max_order,vector_of_scales)

This function computes Gaussian scale space features of an image, up to a 
maximum order, and at a given vector of scales. Works with grayscale & RGB
F = computeGaussianScaleSpaceFeatures(IMAGE,max_order,vector_of_scales)


[cornersIn,cornersOut,Min,Mout,Mframe]=getFrameBoundaryAndMasks(G,corners,
    NP,INCNORM,TYP_FRAME_W,rescalingFactor,providedCornersIn)

This function get a (typically smoothed) gradient magnitude image derived
from a cut photo, and detects the plexiglass frame. It also requires the 
corners detected by the SVM. It's a bit ugly but fairly works well. 
It essentially finds sets of parallel lines that are ~1 frame width apart 

      Note: Good values for number_of_points and delta_norm_probe are 
      100 and 0.1, respectively

[insideCornersCoords, outsideCornersCoords, MaskInside, MaskOutside, MaskFrame = 
  getFrameBoundaryAndMasks(GRADIENT,corners_from_SVM,number_of_points,...
      delta_norm_probe,typical_frame_width,rescalingFactor,inside_corners_optional)


GUIthreshold.fig and GUIthreshold.m: GUI to select a segmentation threshold
This GUI is used by ProcessPhotos.m.

munkres.m: implementation of Hungarian algorithm. We use it to map sets of 
salient points to NE, NW, SW, SE corners.

newid: a dialog box in which ENTER = clicking on OK.

rigidTransform: a package to estimate rigid transforms with RANSAC


## WEIGHTS / MAT FILES

SVM_resc6_deriv_3_scales_0369_matlab_2018a.mat: svm to segment pixels inside
tha black corners of the frame, using images downsampled by a factor of 6 in
each dimension, scales sigma = 0, 3, 6, 9, and order up to 3. 


## TODO: 
Too many hardcoded resolutions, etc. It'd be good to do it in a cleaner way ...


