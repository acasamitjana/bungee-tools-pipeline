clear

d=dir('*.mask.png');

for i=1:length(d)
    
    [i length(d)]
    
    I=imread(d(i).name);
    
    [BW,n]=bwlabel(I>128);
    
    areas=zeros(1,n);
    for nn=1:n
        areas(nn)=sum(BW(:)==nn);
    end
    [~,idx]=sort(areas,'descend');
    
    M=zeros(size(I),'uint8');
    for j=1:4
        aux=BW==idx(j);
        aux=imfill(aux,'holes');
        M(aux)=255;
    end
    
    imwrite(M,d(i).name);
      
end