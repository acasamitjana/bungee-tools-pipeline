% This script uses training data (images & manual segmentations) in an 
% input directory to train a classifier that detects the corners of the 
% plexiglass frame that we put around the slices/blocks we photograph
clear

% input directory with training data
inputDir=[pwd() filesep 'trainingDataFrameCorners' filesep];
% We don't operate at full resolution, but an image downsampled by this factor
rescalingFactor=6; 
% maximum derivative order for scale space features
featMaxDerivOrder=3; 
% Scales (Gaussian sigmas) at which derivatives are computed
featScales=[0 3 6 9]; 
% pixels per image and class used in training
npixPerImagePerClass=2000; 
% verbosity for svm training: 0, 1 or 2
verbosity=1; 
% File where the SVM is stored
outputFile='SVM_resc6_deriv_3_scales_0369_matlab_2018a.mat';


addpath(genpath(['..' filesep 'functions' filesep]));


% count number of features (to allocate feature matrix)
count=0;
for order=0:featMaxDerivOrder, for ox=0:order, for oy=0:order, if ox+oy==order, count=count+1; end; end; end; end
nfeats=3*count*length(featScales); % 3 is for RGB

% Gather features
d=dir([inputDir '/*.JPG']);
F=zeros(npixPerImagePerClass*2*length(d),nfeats);
labels=zeros(size(F,1),1);
indP=0;
for i=1:length(d)
    
    disp(['Gathering features of image ' num2str(i) ' of ' num2str(length(d))]);
    
    I=imread([inputDir '/' d(i).name]);
    M=imread([inputDir '/' d(i).name(1:end-4) '.mask.png']);
    
    Ir=imresize(I,1/rescalingFactor);
    Mr=imresize(M,1/rescalingFactor)>128;
    
    aux=find(Mr);
    rp=randperm(length(aux));
    idxPos=aux(rp(1:npixPerImagePerClass));
    
    aux=find(~Mr);
    rp=randperm(length(aux));
    idxNeg=aux(rp(1:npixPerImagePerClass));
    
    feats=computeGaussianScaleSpaceFeatures(Ir,featMaxDerivOrder,featScales);
    feats=reshape(feats,[size(feats,1)*size(feats,2) size(feats,3)]);
    
    F(indP+1:indP+npixPerImagePerClass,:)=feats(idxPos,:);
    F(indP+npixPerImagePerClass+1:indP+2*npixPerImagePerClass,:)=feats(idxNeg,:);
    labels(indP+1:indP+npixPerImagePerClass)=1;
    labels(indP+npixPerImagePerClass+1:indP+2*npixPerImagePerClass)=0;
    indP=indP+2*npixPerImagePerClass;
end

disp('Training SVM');

SVMModel = fitcsvm(F,labels>0,'Standardize',true,'Verbose',verbosity,'Solver','smo');

disp('Done! Saving to disk');
save(outputFile,'rescalingFactor','featMaxDerivOrder','featScales','SVMModel');

