% Get input directory
inputDir = '/home/acasamitjana/Data/P41-16/Sectioning';%uigetdir('','Please select input directory with all blocks for a single subject (typically: ''XXXX/Sectioning''):');
blockVolsDir = '/home/acasamitjana/Data/P41-16/BlockVols';%uigetdir('','Please select the directory to save the block volumes (typically: ''XXXX/BlockVols''):');


% Get all input directories
struct_blocks = dir(inputDir);

% Copy files
for i=1:length(struct_blocks)
    
    if strcmp(struct_blocks(i).name,'.') || strcmp(struct_blocks(i).name,'..')
        continue
    end
    
    blockDir=[inputDir filesep struct_blocks(i).name filesep 'renamed'];
    
    baseNameVolumes = struct_blocks(i).name;
    
    inputVolumeGrayscale = [blockDir filesep baseNameVolumes '_volume.gray.nii.gz'];
    inputVolumeRGB = [blockDir filesep baseNameVolumes '_volume.rgb.nii.gz'];
    inputVolumeMask = [blockDir filesep baseNameVolumes '_volume.mask.nii.gz'];
    
    if contains(baseNameVolumes, 'BS')
        for it_b=1:5
            outputVolumeGrayscale = [blockVolsDir filesep baseNameVolumes(1:7) 'B' num2str(it_b) '.1_volume.gray.nii.gz'];
            outputVolumeRGB = [blockVolsDir filesep baseNameVolumes(1:7) 'B' num2str(it_b) '.1_volume.rgb.nii.gz'];
            outputVolumeMask = [blockVolsDir filesep baseNameVolumes(1:7) 'B' num2str(it_b) '.1_volume.mask.nii.gz'];

            if exist(inputVolumeGrayscale, 'file') && ~exist(outputVolumeGrayscale, 'file')
                copyfile(inputVolumeGrayscale, outputVolumeGrayscale)
            end

            if exist(inputVolumeRGB, 'file') && ~exist(outputVolumeRGB, 'file')
                copyfile(inputVolumeRGB, outputVolumeRGB)
            end

            if exist(inputVolumeMask, 'file') && ~exist(outputVolumeMask, 'file')
                copyfile(inputVolumeMask, outputVolumeMask)
            end
        end
    else
        outputVolumeGrayscale = [blockVolsDir filesep baseNameVolumes '_volume.gray.nii.gz'];
        outputVolumeRGB = [blockVolsDir filesep baseNameVolumes '_volume.rgb.nii.gz'];
        outputVolumeMask = [blockVolsDir filesep baseNameVolumes '_volume.mask.nii.gz'];

        if exist(inputVolumeGrayscale, 'file') && ~exist(outputVolumeGrayscale, 'file')
            copyfile(inputVolumeGrayscale, blockVolsDir)
        end

        if exist(inputVolumeRGB, 'file') && ~exist(outputVolumeRGB, 'file')
            copyfile(inputVolumeRGB, blockVolsDir)
        end

        if exist(inputVolumeMask, 'file') && ~exist(outputVolumeMask, 'file')
            copyfile(inputVolumeMask, blockVolsDir)
        end
    end
    
   
end
