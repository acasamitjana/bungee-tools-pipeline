# CUTPHOTOS
> Functions to process photographs from the sectioning ("blockface photos")

There are two sets of functions here: for users and for developers

## USERS 

RenameBlockfacePhotos.m

This script goes over the pictures of an input directory, and lets you
double check that there are no duplicates / missing photos while renaming
the files. At the end, it also prompts the user to select a reference photo 
for the block, as well as to mark the corners of the green cassette on it: 
a small effort providing increased robustness later down the (processing) road.


processBlockFaceDirectory.m

This is a long script that, without any user interaction, takes a bunch
of photographs renamed by RenameBlockfacePhotos.m, and builds three
volumes: one grayscale, one RBG, and one probabilistic segmentation.
It has 3 distinct parts: the first one registers the photos; the second
resamples and segments with a fully convolutional network; and the third
simply stacks photos to build volumes.


## DEVELOPERS

GenerateTrainingData.m

Goes around a bunch of volumes, randomly extracts slices, and asks the 
user to annotate masks
Just put a bunch of rgb blockface volumes in an input directory, specify the
input and output directories below, and label the images when prompted.
I have a bunch of image / segmentation pair labeled this way inside the 
directory "trainingDataSgmentBlockface"


TrainSegmentBFclassifierFCN.m

This script tains a segmentation network (fully convolutional network or FCN)
from the examples gathered by GenerateTrainingData.m, to segment tissue vs wax
The output is a mat file with the network weights; the latest one we have is
FCN_resc2_Nov18.mat. This code requires:

1. A GPU.

2. Modifying Matlab's imageDataAugmenter class with the code in
   BrightContrAugmenter.txt (you may want to make a backup first)



## TEST DATA

testblock

In this directory, you'll find ~20 blockface photographs that you can use
to test the code

