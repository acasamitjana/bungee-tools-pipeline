% This version uses a "global" reference photograph to propagate 2 masks
% manually delineated on it to a reference image within a block, typically
% one around the center that has no occlusions or weird lighthning
% conditions. The first mask, corresponds to the sliding part of the
% microtome, is propagated with SURF+RANSAC. The second, which follows the
% contour of the green cassette, is propagated with a transform that is
% computed from manually labeled corners - I give the option of labeling
% them here, but the idea is that the (block) reference image and the
% corner coordinates are provided by Shauna in the script that reanmes the
% files containing the photographs.
%clear
close all

addpath(genpath('../functions/'));

DEBUG=0;
MAX_PRED_ERR=100; % points that deviate more than this many pixels from global transform are outliers (Inf = no cleanup)
transformMode='affine'; % projective of affine

% globalRefImageFile='reference.jpg';
% globalRefMaskFile1='reference_mask_sliding_part.png';
% globalRefMaskFile2='reference_mask_only_plate_edges.png';
% cornersGlobalREF=[1440 1999; 1501 3142; 2329 3099; 2269 1951]';

globalRefImageFile='reference2.jpg';
globalRefMaskFile1='reference2_mask_sliding.png';
globalRefMaskFile2='reference2_mask_plate.png';
cornersGlobalREF=[1797 2440 ; 1767 3586 ; 2595 3604 ; 2619 2458 ]';

% inputDir='~/Downloads/P47-17_PracticeBlock/';
% outputDir=['~/Downloads/P47-17_PracticeBlock/reg_' transformMode
% '_0501_resampling3/'];
%inputDir=[pwd() '/testblock/renamed/'];
%outputDir=[pwd() '/testblock/renamed/registered/'];

%inputDir='/home/matman/matlab/renamed';
%inputDir='/home/matman/exvivoAcq/P57-16/Sectioning/P57-16_C1.1/renamed';
%outputDir='/home/matman/matlab/registered';
%outputDir='/home/matman/matlab/C1.1_registered';

if exist(outputDir,'dir')==0
    mkdir(outputDir);
end

if exist([inputDir '/REFdata.mat' ],'file')
    copyfile([inputDir '/REFdata.mat'], outputDir);
end

% read in global reference
disp('Reading in global reference image / masks');
globalREFrgb=imread(globalRefImageFile);
globalREF=rgb2gray(globalREFrgb);
globalREFmaskA=imread(globalRefMaskFile1)>0;
globalREFmaskB=imdilate(imread(globalRefMaskFile2)>0,strel('disk',20));
globarREFmaskBdilated=imdilate(globalREFmaskB,strel('disk',20));

% Detect SURF points for global reference (I guess I could save them to a
% file, but whatever... it's jut 2 seconds)
disp('Detecting SURF points for global reference');
points1gr = detectSURFFeatures(globalREF,'NumOctaves',5);
[f1gr,vpts1gr] = extractFeatures(globalREF,points1gr);
idx1a=find(globalREFmaskA(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1aGr=points1gr(idx1a); f1aGr=f1gr(idx1a,:); vpts1aGr=vpts1gr(idx1a);
idx1b=find(globalREFmaskB(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1bGr=points1gr(idx1b); f1bGr=f1gr(idx1b,:); vpts1bGr=vpts1gr(idx1b);

% Detect points for block-specific reference, with two purposes:
% a) Matching to global reference
% b) Matching to all other photos in this block
if exist([outputDir '/REFdata.mat' ],'file')
    load([outputDir '/REFdata.mat' ],'imageFilename');
    REFrgb=imread([inputDir '/' imageFilename]);
else
    
    disp('Picking a suitable reference image for this block');
    d=dir([inputDir '/*.JPG']);
    i=round(length(d)/2)-1;
    happy='No';
    while strcmp(happy,'No')
        i=i+1;
        imageFilename=[inputDir '/' d(i).name];
        REFrgb=imread(imageFilename);
        figure(1), imshow(REFrgb)
        happy = questdlg('Are you happy with this one?',' ','Yes','No','Yes');
    end
    close all
end
REF=rgb2gray(REFrgb);

disp('Detecting SURF points for global reference');
points1 = detectSURFFeatures(REF,'NumOctaves',5);
[f1,vpts1] = extractFeatures(REF,points1);

disp('Matching global and block reference')
% matching based on whole sliding part
indexPairs = matchFeatures(f1aGr,f1) ;
matchedPoints1 = vpts1aGr(indexPairs(:,1));
matchedPoints2 = vpts1(indexPairs(:,2));
if DEBUG
    close all, figure; showMatchedFeatures(globalREF,REF,matchedPoints1,matchedPoints2);
    legend('matched points 1','matched points 2');
    title('Alignment Global Reference vs. Block reference using sliding part of microtome');
    pause(3)
end
tform = estimateGeometricTransform(matchedPoints1,matchedPoints2,'affine','MaxNumTrials',10000,'Confidence',99.9999);
globalREFw = imwarp(globalREF,tform,'OutputView',imref2d(size(REF)));
REFmaskA = imwarp(globalREFmaskA,tform,'OutputView',imref2d(size(REF)))>0;
if DEBUG
    close all, figure, for k=1:3, imshow(REF), pause(0.5), imshow(REFmaskA), pause(0.5),  end
end

% Get four points from user
if exist([outputDir '/REFdata.mat' ], 'file')
    load([outputDir '/REFdata.mat' ],'cornersREF');
else
    happy = 'No';
    while strcmp(happy,'No')
        close all, figure, imshow(REFrgb)
        button = questdlg('Please click on the four corners of the green cassette',' ','OK','OK');
        cornersREF=zeros(2,4);
        for c=1:4
            aux=ginput(1);
            cornersREF(:,c)=flipud(aux');
            hold on, plot(aux(1),aux(2),'r.','markersize',40); hold off
        end
        happy = questdlg('Are you happy with your selection?',' ','Yes','No','Yes');
    end
    close all
    
    % Reorder: NW, NE, SE, SW (Hungarian algorithm)
    NWness=cornersREF(1,:)+cornersREF(2,:);
    NEness=cornersREF(1,:)-cornersREF(2,:);
    SEness=-NWness;
    SWness=-NEness;
    [idx,cost] = munkres([NWness; NEness; SEness; SWness]);
    cornersREF=round(cornersREF(:,idx));
    save([outputDir '/REFdata.mat' ], 'cornersREF','imageFilename');
end

% We can now easily warp the mask following the edges of the plate / cassette
tform = estimateGeometricTransform(flipud(cornersGlobalREF)',flipud(cornersREF)','affine');
REFmaskB = imwarp(globalREFmaskB,tform,'OutputView',imref2d(size(REF)))>0;
REFmaskBdilated=imdilate(REFmaskB,strel('disk',20));


if DEBUG
    close all, figure, for k=1:3, imshow(REF), pause(0.5), imshow(REFmaskB), pause(0.5),  end
end

% We can now mask the points, and start iterating!
idx1a=find(REFmaskA(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1a=points1(idx1a); f1a=f1(idx1a,:); vpts1a=vpts1(idx1a);
idx1b=find(REFmaskB(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1b=points1(idx1b); f1b=f1(idx1b,:); vpts1b=vpts1(idx1b);

% Go over target images
d=dir([inputDir '/*.JPG']);
for i=1:length(d)
    
    outputFile=[outputDir '/' d(i).name(1:end-4) '.warped.jpg'];
    
    if exist(outputFile,'file')
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': output already there']);
    else
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': working on it']);
        
        % Read in
        I2rgb=imread([inputDir '/'    d(i).name]);
        I2=rgb2gray(I2rgb);
        
        % Detect points
        points2 = detectSURFFeatures(I2,'NumOctaves',5);
        [f2,vpts2] = extractFeatures(I2,points2);
        
        % matching based on whole sliding part
        indexPairs = matchFeatures(f1a,f2) ;
        matchedPoints1 = vpts1a(indexPairs(:,1));
        matchedPoints2 = vpts2(indexPairs(:,2));
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Alignment sliding part image');
            pause(3)
        end
        tform = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);
        I2w = imwarp(I2,tform,'OutputView',imref2d(size(REF)));
        if DEBUG
            close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w), pause(0.5), end
        end
        
        % Second pass: recompute feature, but use only points inside mask 
        points2b = detectSURFFeatures(I2w,'NumOctaves',5);
        [f2b,vpts2b] = extractFeatures(I2w,points2b);
        idx2=find(REFmaskBdilated(sub2ind(size(REF),round(points2b.Location(:,2)),round(points2b.Location(:,1)))));
        points2b=points2b(idx2); f2b=f2b(idx2,:); vpts2b=vpts2b(idx2);
        indexPairs = matchFeatures(f1b,f2b) ;
        matchedPoints1 = vpts1b(indexPairs(:,1));
        matchedPoints2 = vpts2b(indexPairs(:,2));
        
        % Clean up (first transform should have provided good alignment)
        predErr=sqrt(sum((matchedPoints1.Location-matchedPoints2.Location).^2,2));
        mask=predErr<MAX_PRED_ERR;
        matchedPoints1=matchedPoints1(mask);
        matchedPoints2=matchedPoints2(mask);
         
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2w,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Aligment of plate');
            pause(3)
        end
        tform2 = estimateGeometricTransform(matchedPoints2,matchedPoints1,transformMode,'MaxNumTrials',10000,'Confidence',99.9999);
        
        % Composition doesn't work in projective...
        if strcmp(transformMode,'affine');        
            TFORM=tform;
            TFORM.T=tform.T*tform2.T;
            if DEBUG
                I2w2 = imwarp(I2,TFORM,'OutputView',imref2d(size(REF)));
                close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w2), pause(0.5), end
            end
            
            WARPED=zeros(size(REFrgb),'uint8');
            for c=1:3
                WARPED(:,:,c)= imwarp(I2rgb(:,:,c),TFORM,'OutputView',imref2d(size(REF)));
            end
            imwrite(WARPED,outputFile);
        else
            if DEBUG
                I2w2 = imwarp(I2w,tform2,'OutputView',imref2d(size(REF)));
                close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w2), pause(0.5), end
            end
            
            WARPED=zeros(size(REFrgb),'uint8');
            for c=1:3   
                aux=imwarp(I2rgb(:,:,c),tform,'OutputView',imref2d(size(REF)));
                WARPED(:,:,c)=imwarp(aux,tform2,'OutputView',imref2d(size(REF)));
            end
            imwrite(WARPED,outputFile);
        end
    end
end
disp('All done!');






