% apply the classifier to stacks of blockface photographs
clear

addpath('~/matlab/myFunctions/');

inputdir='~/exvivoAcq/P57-16/BlockVols';
outputdir='~/exvivoAcq/P57-16/NewMasks';
DLmatFile='FCN_resc2_Nov18.mat';

if exist(outputdir,'dir')==0
    mkdir(outputdir);
end

load(DLmatFile,'rescalingFactor','trainedNet');
d=dir([inputdir filesep '*rgb.nii.gz']);
for i=1:length(d)
    
    outputfile=[outputdir filesep d(i).name(1:end-7) '.mask.nii.gz'];
    
    if exist(outputfile,'dir')
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': output already there']);
        disp(' ');
    else
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': reading in volume']);
        
        I=myMRIread([inputdir filesep d(i).name]);
        S=I;
        S.vol=zeros([size(I.vol,1) size(I.vol,2) size(I.vol,3)]);
        
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': classifying slices']);
        for j=1:size(I.vol,3)
            disp(['   Slice ' num2str(j) ' of ' num2str(size(I.vol,3))]);
            Ij=uint8(squeeze(I.vol(:,:,j,:)));
            Ij=imresize(Ij,1/rescalingFactor);
            [labelsJ,~,scoresJ]=semanticseg(Ij,trainedNet);
            Lj=uint8(255*double(scoresJ(:,:,2)));
            Lj=imresize(Lj,[size(S.vol,1) size(S.vol,2)]);
            S.vol(:,:,j)=Lj;
        end
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': writing output']);
    
        myMRIwrite(S,outputfile);
        
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': done!']);
        disp(' ');
        
    end
end
disp('All done!');



