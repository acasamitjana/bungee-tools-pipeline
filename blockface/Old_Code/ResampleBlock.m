% This function resamples the block face in registered images to a rectangle
% of specified size
% It expects REFdata.mat  to exist in the input directory
%clear

%inputDir=[pwd() '/testblock/renamed/registered/'];
SIZ=[740 520]; % actual dims are 74 x 52 mm
%inputDir='/home/matman/matlab/registered';
%inputDir='/home/matman/matlab/C1.1_registered';

outputDir=[inputDir '/cropped/'];
if exist(outputDir,'dir')==0
    mkdir(outputDir);
end

load([inputDir '/REFdata.mat'],'cornersREF');
cornersTarget=[1 1; 1 SIZ(1); SIZ(2) SIZ(1); SIZ(2) 1]';


% Estimate affine transform
A=[cornersREF(2,:)' cornersREF(1,:)' zeros(4,2) ones(4,1) zeros(4,1);
    zeros(4,2) cornersREF(2,:)' cornersREF(1,:)'  zeros(4,1) ones(4,1)];
b=[cornersTarget(2,:)'; cornersTarget(1,:)'];
aux=A\b;
tform=affine2d([aux(1) aux(3) 0; aux(2) aux(4) 0; aux(5) aux(6) 1]);
        
d=dir([inputDir '/*.jpg']);
for i=1:length(d)
    
    outputFile=[outputDir '/' d(i).name(1:end-4) '.cropped.jpg'];
    
    if exist(outputFile,'file')
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': output already there']);
    else
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': working on it']);
        
        I=imread([inputDir '/' d(i).name]);
        WARPED = imwarp(I,tform,'OutputView',imref2d(fliplr(SIZ)));
        
        imwrite(uint8(WARPED),outputFile);
    end
end

disp('All done!');
