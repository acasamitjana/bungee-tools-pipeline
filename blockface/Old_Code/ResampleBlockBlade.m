% This function resamples the block face in registered images to a rectangle
% of specified size
% This version uses the registration of the blade to estimate a zoom factor
% that compensates for the fact that the distance between the cassette and
% the block face is decreasing
% It expects REFdata.mat  to exist in the input directory
clear

inputDir='~/Downloads/testblock/renamed/registered/';

SIZ=[740 520]; % actual dims are 74 x 52 mm

outputDir1=[inputDir '/cropped/'];
if exist(outputDir1,'dir')==0
    mkdir(outputDir1);
end
outputDir2=[inputDir '/croppedNoCorrection/'];
if exist(outputDir2,'dir')==0
    mkdir(outputDir2);
end

load([inputDir '/REFdata.mat'],'cornersREF');

% Part 1: compute zoom factors
disp('We first read in the zoom factors, and smooth them');
d=dir([inputDir '/*.jpg']);
factors=zeros([1,length(d)]);
for i=1:length(d)
    factorFile=[inputDir '/' d(i).name(1:end-11) '.ScalingFactors.mat'];
    load(factorFile,'ScalingFactorCassette','ScalingFactorBlade');
    factors(i)=ScalingFactorBlade/ScalingFactorCassette;
end
logFactors=log(factors);
x=(1:length(factors))';
params=robustfit(x,logFactors');
logFactorsFitted=params(1)+params(2)*x;
factorsFitted=exp(logFactorsFitted);

% We need not only to map cornersREF to cornersTarget, but also enlarge
% image i by factorsFitted(i)

% Estimate affine transform
for i=1:length(d)
    
    outputFile1=[outputDir1 '/' d(i).name(1:end-4) '.cropped.jpg'];
    outputFile2=[outputDir2 '/' d(i).name(1:end-4) '.cropped.jpg'];
    
    if exist(outputFile1,'file') && exist(outputFile2,'file')
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': output already there']);
    else
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': working on it']);
        
        I=imread([inputDir '/' d(i).name]);
        
        for f=1:2
            if f==1
                margin=SIZ*(factorsFitted(i)-1)/2;
                ofile=outputFile1;
            else
                margin=[0 0];
                ofile=outputFile2;
            end
            
            cornersTarget=[1-margin(2) 1-margin(1); 1-margin(2) SIZ(1)+margin(1);...
                SIZ(2)+margin(2) SIZ(1)+margin(1); SIZ(2)+margin(2) 1-margin(1)]';
            
            A=[cornersREF(2,:)' cornersREF(1,:)' zeros(4,2) ones(4,1) zeros(4,1);
                zeros(4,2) cornersREF(2,:)' cornersREF(1,:)'  zeros(4,1) ones(4,1)];
            b=[cornersTarget(2,:)'; cornersTarget(1,:)'];
            aux=A\b;
            tform=affine2d([aux(1) aux(3) 0; aux(2) aux(4) 0; aux(5) aux(6) 1]);
            
            WARPED = imwarp(I,tform,'OutputView',imref2d(fliplr(SIZ)));
            
            imwrite(uint8(WARPED),ofile);
        end
    end
end

disp('All done!');
