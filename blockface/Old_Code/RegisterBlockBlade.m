% This version uses a "global" reference photograph to propagate 2 masks
% manually delineated on it to a reference image within a block, typically
% one around the center that has no occlusions or weird lighthning
% conditions. The first mask, corresponds to the sliding part of the
% microtome, is propagated with SURF+RANSAC. The second, which follows the
% contour of the green cassette, is propagated with a transform that is
% computed from manually labeled corners - I give the option of labeling
% them here, but the idea is that the (block) reference image and the
% corner coordinates are provided by Shauna in the script that reanmes the
% files containing the photographs.
clear
close all

addpath(genpath('../functions/'));

DEBUG=0;
MAX_PRED_ERR=100; % points that deviate more than this many pixels from global transform are outliers (Inf = no cleanup)
globalRefImageFile='reference2.jpg';
globalRefMaskFileSliding='reference2_mask_sliding.png';
globalRefMaskFilePlateEdges='reference2_mask_plate.png';
globalRefMaskFileBladeAndSupport='reference2_mask_bladeAndSupport.png';
globalRefMaskFileOnlyBlade='reference2_mask_blade.png';
% cornersGlobalREF=[1797 2440 ; 1767 3586 ; 2595 3604 ; 2619 2458 ]'; this was before I rotated it 2 degrees...
cornersGlobalREF=[1760 2439 ; 1764 3596 ; 2607 3588 ; 2598 2435 ]';

inputDir='~/Downloads/testblock/renamed/';
outputDir='~/Downloads/testblock/renamed/registered/';


if exist(outputDir,'dir')==0
    mkdir(outputDir);
end

if exist([inputDir '/REFdata.mat' ],'file')
    copyfile([inputDir '/REFdata.mat'], outputDir);
end

% read in global reference
disp('Reading in global reference image / masks');
globalREFrgb=imread(globalRefImageFile);
globalREF=rgb2gray(globalREFrgb);
globalREFmaskSliding=imread(globalRefMaskFileSliding)>0;
globalREFmaskPlateEdges=imdilate(imread(globalRefMaskFilePlateEdges)>0,strel('disk',20));
globarREFmaskPlateEdgesDilated=imdilate(globalREFmaskPlateEdges,strel('disk',20));
globalREFmaskBladeAndSupport=imread(globalRefMaskFileBladeAndSupport)>0;
globalREFmaskBlade=imread(globalRefMaskFileOnlyBlade)>0;

% Detect SURF points for global reference (I guess I could save them to a
% file, but whatever... it's jut 2 seconds)
disp('Detecting SURF points for global reference');
points1gr = detectSURFFeatures(globalREF,'NumOctaves',5);
[f1gr,vpts1gr] = extractFeatures(globalREF,points1gr);
idx1a=find(globalREFmaskSliding(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1aGr=points1gr(idx1a); f1aGr=f1gr(idx1a,:); vpts1aGr=vpts1gr(idx1a);
idx1b=find(globalREFmaskPlateEdges(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1bGr=points1gr(idx1b); f1bGr=f1gr(idx1b,:); vpts1bGr=vpts1gr(idx1b);
idx1aa=find(globalREFmaskBladeAndSupport(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1aaGr=points1gr(idx1aa); f1aaGr=f1gr(idx1aa,:); vpts1aaGr=vpts1gr(idx1aa);
idx1bb=find(globalREFmaskBlade(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1bbGr=points1gr(idx1bb); f1bbGr=f1gr(idx1bb,:); vpts1bbGr=vpts1gr(idx1bb);

% Detect points for block-specific reference, with two purposes:
% a) Matching to global reference
% b) Matching to all other photos in this block
if exist([outputDir '/REFdata.mat' ],'file')
    load([outputDir '/REFdata.mat' ],'imageFilename');
    [pathstr,name,ext] = fileparts(imageFilename);
    REFrgb=imread([inputDir '/' name  ext]);
else
    
    disp('Picking a suitable reference image for this block');
    d=dir([inputDir '/*.JPG']);
    i=round(length(d)/2)-1;
    happy='No';
    while strcmp(happy,'No')
        i=i+1;
        imageFilename=[inputDir '/' d(i).name];
        REFrgb=imread(imageFilename);
        figure(1), imshow(REFrgb)
        happy = questdlg('Are you happy with this one?',' ','Yes','No','Yes');
    end
    close all
end
REF=rgb2gray(REFrgb);

disp('Detecting SURF points for block reference');
points1 = detectSURFFeatures(REF,'NumOctaves',5);
[f1,vpts1] = extractFeatures(REF,points1);

disp('Matching global and block reference: sliding part')
% matching based on whole sliding part
indexPairs = matchFeatures(f1aGr,f1) ;
matchedPoints1 = vpts1aGr(indexPairs(:,1));
matchedPoints2 = vpts1(indexPairs(:,2));
if DEBUG
    close all, figure; showMatchedFeatures(globalREF,REF,matchedPoints1,matchedPoints2);
    legend('matched points 1','matched points 2');
    title('Alignment Global Reference vs. Block reference using sliding part of microtome');
    pause(3)
end
tform = estimateGeometricTransform(matchedPoints1,matchedPoints2,'affine','MaxNumTrials',10000,'Confidence',99.9999);
globalREFw = imwarp(globalREF,tform,'OutputView',imref2d(size(REF)));
REFmaskA = imwarp(globalREFmaskSliding,tform,'OutputView',imref2d(size(REF)))>0;

if DEBUG
    close all, figure, for k=1:3, imshow(REF), pause(0.5), imshow(REFmaskA), pause(0.5), end
end

disp('Matching global and block reference: blade and support')
indexPairs = matchFeatures(f1aaGr,f1) ;
matchedPoints1 = vpts1aaGr(indexPairs(:,1));
matchedPoints2 = vpts1(indexPairs(:,2));
if DEBUG
    close all, figure; showMatchedFeatures(globalREF,REF,matchedPoints1,matchedPoints2);
    legend('matched points 1','matched points 2');
    title('Alignment Global Reference vs. Block reference using blade and its support frame');
    pause(3)
end
tform = estimateGeometricTransform(matchedPoints1,matchedPoints2,'affine','MaxNumTrials',10000,'Confidence',99.9999);
globalREFww = imwarp(globalREF,tform,'OutputView',imref2d(size(REF)));
REFmaskAA = imwarp(globalREFmaskBladeAndSupport,tform,'OutputView',imref2d(size(REF)))>0;
if DEBUG
    close all, figure, for k=1:3, imshow(REF), pause(0.5), imshow(REFmaskAA), pause(0.5), end
end

disp('   (refinement using only the blade)')
idx1a=find(globalREFmaskSliding(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1aGr=points1gr(idx1a); f1aGr=f1gr(idx1a,:); vpts1aGr=vpts1gr(idx1a);

idxblade=find(REFmaskAA(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
f1blade=f1(idxblade,:);
vpts1blade=vpts1(idxblade);

indexPairs = matchFeatures(f1bbGr,f1blade) ;
matchedPoints1 = vpts1bbGr(indexPairs(:,1));
matchedPoints2 = vpts1blade(indexPairs(:,2));
if DEBUG
    close all, figure; showMatchedFeatures(globalREF,REF,matchedPoints1,matchedPoints2);
    legend('matched points 1','matched points 2');
    title('Alignment Global Reference vs. Block reference using only the blade');
    pause(3)
end
tform = estimateGeometricTransform(matchedPoints1,matchedPoints2,'affine','MaxNumTrials',10000,'Confidence',99.9999);
REFmaskBB = imwarp(globalREFmaskBlade,tform,'OutputView',imref2d(size(REF)))>0;
REFmaskBBdilated=imdilate(REFmaskBB,strel('disk',20));
if DEBUG
    close all, figure, for k=1:3, imshow(REF), pause(0.5), imshow(REFmaskBB), pause(0.5), end
end


% Get four points from user
if exist([outputDir '/REFdata.mat' ], 'file')
    load([outputDir '/REFdata.mat' ],'cornersREF');
else
    happy = 'No';
    while strcmp(happy,'No')
        close all, figure, imshow(REFrgb)
        button = questdlg('Please click on the four corners of the green cassette',' ','OK','OK');
        cornersREF=zeros(2,4);
        for c=1:4
            aux=ginput(1);
            cornersREF(:,c)=flipud(aux');
            hold on, plot(aux(1),aux(2),'r.','markersize',40); hold off
        end
        happy = questdlg('Are you happy with your selection?',' ','Yes','No','Yes');
    end
    close all
    
    % Reorder: NW, NE, SE, SW (Hungarian algorithm)
    NWness=cornersREF(1,:)+cornersREF(2,:);
    NEness=cornersREF(1,:)-cornersREF(2,:);
    SEness=-NWness;
    SWness=-NEness;
    [idx,cost] = munkres([NWness; NEness; SEness; SWness]);
    cornersREF=round(cornersREF(:,idx));
    save([outputDir '/REFdata.mat' ], 'cornersREF','imageFilename');
end

% We can now easily warp the mask following the edges of the plate / cassette
tform = estimateGeometricTransform(flipud(cornersGlobalREF)',flipud(cornersREF)','affine');
REFmaskB = imwarp(globalREFmaskPlateEdges,tform,'OutputView',imref2d(size(REF)))>0;
REFmaskBdilated=imdilate(REFmaskB,strel('disk',20));

% We can now mask the points!
idx1a=find(REFmaskA(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1a=points1(idx1a); f1a=f1(idx1a,:); vpts1a=vpts1(idx1a);
idx1b=find(REFmaskB(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1b=points1(idx1b); f1b=f1(idx1b,:); vpts1b=vpts1(idx1b);

idx1aa=find(REFmaskAA(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1aa=points1(idx1aa); f1aa=f1(idx1aa,:); vpts1aa=vpts1(idx1aa);
idx1bb=find(REFmaskBB(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1bb=points1(idx1bb); f1bb=f1(idx1bb,:); vpts1bb=vpts1(idx1bb);



% Go over target images
d=dir([inputDir '/*.JPG']);
for i=1:length(d)
    
    outputImageFile=[outputDir '/' d(i).name(1:end-4) '.warped.jpg'];
    outputFactorFile=[outputDir '/' d(i).name(1:end-4) '.ScalingFactors.mat'];
    
    if exist(outputImageFile,'file') && exist(outputFactorFile,'file')
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': output already there']);
    else
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': working on it']);
        
        % Read in
        I2rgb=imread([inputDir '/'    d(i).name]);
        I2=rgb2gray(I2rgb);
        
        % Detect points
        points2 = detectSURFFeatures(I2,'NumOctaves',5);
        [f2,vpts2] = extractFeatures(I2,points2);
        
        % matching based on whole sliding part
        indexPairs = matchFeatures(f1a,f2) ;
        matchedPoints1 = vpts1a(indexPairs(:,1));
        matchedPoints2 = vpts2(indexPairs(:,2));
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Alignment sliding part image');
            pause(3)
        end
        tform = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);
        I2w = imwarp(I2,tform,'OutputView',imref2d(size(REF)));
        if DEBUG
            close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w), pause(0.5), end
        end
        
        % Second pass: recompute feature, but use only points on edge of green cassette
        points2b = detectSURFFeatures(I2w,'NumOctaves',5);
        [f2b,vpts2b] = extractFeatures(I2w,points2b);
        idx2=find(REFmaskBdilated(sub2ind(size(REF),round(points2b.Location(:,2)),round(points2b.Location(:,1)))));
        points2b=points2b(idx2); f2b=f2b(idx2,:); vpts2b=vpts2b(idx2);
        indexPairs = matchFeatures(f1b,f2b) ;
        matchedPoints1 = vpts1b(indexPairs(:,1));
        matchedPoints2 = vpts2b(indexPairs(:,2));
        
        % Clean up (first transform should have provided good alignment)
        predErr=sqrt(sum((matchedPoints1.Location-matchedPoints2.Location).^2,2));
        mask=predErr<MAX_PRED_ERR;
        matchedPoints1=matchedPoints1(mask);
        matchedPoints2=matchedPoints2(mask);
        
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2w,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Aligment of plate');
            pause(3)
        end
        tform2 = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);
        
        % Composition
        TFORM=tform;
        TFORM.T=tform.T*tform2.T;
        if DEBUG
            I2w2 = imwarp(I2,TFORM,'OutputView',imref2d(size(REF)));
            close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w2), pause(0.5), end
        end
        
        WARPED=zeros(size(REFrgb),'uint8');
        for c=1:3
            WARPED(:,:,c)= imwarp(I2rgb(:,:,c),TFORM,'OutputView',imref2d(size(REF)));
        end
        imwrite(WARPED,outputImageFile);
        
        ScalingFactorCassette=det(TFORM.T(1:3,1:3));
        
        
        % Here we do something similar, but focusing on the alignment of
        % the blade
        indexPairs = matchFeatures(f1aa,f2) ;
        matchedPoints1 = vpts1aa(indexPairs(:,1));
        matchedPoints2 = vpts2(indexPairs(:,2));
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Alignment: blade and its support part');
            pause(3)
        end
        tform = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);
        I2w = imwarp(I2,tform,'OutputView',imref2d(size(REF)));
        if DEBUG
            close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w), pause(0.5), end
        end
        
        % Second pass for blade: recompute features, but use only points on the actual blade
        points2bb = detectSURFFeatures(I2w,'NumOctaves',5);
        [f2bb,vpts2bb] = extractFeatures(I2w,points2bb);
        idx2=find(REFmaskBBdilated(sub2ind(size(REF),round(points2bb.Location(:,2)),round(points2bb.Location(:,1)))));
        points2bb=points2bb(idx2); f2bb=f2bb(idx2,:); vpts2bb=vpts2bb(idx2);
        indexPairs = matchFeatures(f1bb,f2bb) ;
        matchedPoints1 = vpts1bb(indexPairs(:,1));
        matchedPoints2 = vpts2bb(indexPairs(:,2));
        
        % Clean up (first transform should have provided good alignment)
        predErr=sqrt(sum((matchedPoints1.Location-matchedPoints2.Location).^2,2));
        mask=predErr<MAX_PRED_ERR;
        matchedPoints1=matchedPoints1(mask);
        matchedPoints2=matchedPoints2(mask);
        
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2w,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Aligment of blade');
            pause(3)
        end
        tform2 = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);
        
        % Composition
        TFORM=tform;
        TFORM.T=tform.T*tform2.T;
        if DEBUG
            I2w2 = imwarp(I2,TFORM,'OutputView',imref2d(size(REF)));
            close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w2), pause(0.5), end
        end
        
        ScalingFactorBlade=det(TFORM.T(1:3,1:3));
        
        
        save(outputFactorFile,'ScalingFactorCassette','ScalingFactorBlade');
        
    end
end
disp('All done!');






