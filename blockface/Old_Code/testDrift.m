clear
clc

% inputDir='./testblock/renamed/registered//cropped//';
inputDir='~/Downloads/P47-17_PracticeBlock/registered_21_08_17/cropped/';
outputDir=[inputDir '/driftCorrected/'];
mask='./driftMask.png';
MAX_PRED_ERR = 100; % in 0.1mm pixels
MIN_INC=10;
SKIP=5;

if exist(outputDir,'dir')==0
    mkdir(outputDir);
end


% Detect interest points and obtain features
disp('Computing features')
d=dir([inputDir '/*.jpg']);
M=imread(mask)>0;
for i=1:length(d)
    disp(['    Image ' num2str(i) ' of ' num2str(length(d))]);
    A=rgb2gray(imread([inputDir '/' d(i).name]));
    point=detectSURFFeatures(A,'NumOctaves',5,'MetricThreshold',10);
    idx=find(M(sub2ind(size(M),round(point.Location(:,2)),round(point.Location(:,1))))>0);
    point=point(idx);
    [f,vpt] = extractFeatures(A,point);
    
    points{i}=point;
    fs{i}=f;
    vpts{i}=vpt;
    Is{i}=A;
    
end


disp('Computing features')
D=[];
for i=1:SKIP:length(d)
    
    disp(['    Image ' num2str(i) ' of ' num2str(length(d))]);
    
    for j=i+MIN_INC:SKIP:length(d)
        
        indexPairs = matchFeatures(fs{i},fs{j}) ;
        matchedPoints1 = vpts{i}(indexPairs(:,1));
        matchedPoints2 = vpts{j}(indexPairs(:,2));
        
        
        % Clean up (first transform should have provided good alignment)
        predErr=sqrt(sum((matchedPoints1.Location-matchedPoints2.Location).^2,2));
        mask=predErr<MAX_PRED_ERR;
        matchedPoints1=matchedPoints1(mask);
        matchedPoints2=matchedPoints2(mask);
        
        try
            [tform,inliers1,inliers2] = estimateGeometricTransform(matchedPoints1,matchedPoints2,'affine','MaxNumTrials',10000,'Confidence',99.9999);
            
            D(end+1,:)=(median(inliers2.Location-inliers1.Location))/(j-i);
        end
        
    end
end

drift=median(D);

disp('Resampling images and writing outputs');
tref=round(length(d)/2);
for i=1:length(d)
    disp(['    Image ' num2str(i) ' of ' num2str(length(d))]);
    A=imread([inputDir '/' d(i).name]);
    shift=fliplr(-drift*(i-tref));
    imwrite(imtranslate(A,shift),[outputDir '/' d(i).name]);
end






