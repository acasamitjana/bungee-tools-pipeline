% apply the classifier to stacks of blockface photographs
clear

addpath('~/Desktop/OneDrive - University College London/MATLAB/myFunctions/');

inputdir='~/Downloads';
outputdir='~/Downloads/classified';
SVMmatFile='SVM_resc2_deriv_3_scales_036.mat';

if exist(outputdir,'dir')==0
    mkdir(outputdir);
end

load(SVMmatFile,'rescalingFactor','featMaxDerivOrder','featScales','svmstruct');
d=dir([inputdir filesep '*.nii.gz']);
for i=1:length(d)
    
    outputfile=[outputdir filesep d(i).name(1:end-7) '.mask.nii.gz'];
    
    if exist(outputfile,'file')
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': output already there']);
        disp(' ');
    else
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': reading in volume']);
        
        I=myMRIread([inputdir filesep d(i).name]);
        S=I;
        S.vol=zeros([size(I.vol,1) size(I.vol,2) size(I.vol,3)]);
        
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': classifying slices']);
        for j=1:size(I.vol,3)
            disp(['   Slice ' num2str(j) ' of ' num2str(size(I.vol,3))]);
            Ij=uint8(squeeze(I.vol(:,:,j,:)));
            Ij=imresize(Ij,1/rescalingFactor);
            feats=computeGaussianScaleSpaceFeatures(Ij,featMaxDerivOrder,featScales);
            feats=reshape(feats,[size(feats,1)*size(feats,2) size(feats,3)]);
            labs = predict(svmstruct,feats);
            Lj=uint8(255*double(reshape(labs,[size(Ij,1) size(Ij,2)])>0));
            Lj=imresize(Lj,[size(S.vol,1) size(S.vol,2)]);
            Lj=uint8(255*double(Lj>0));
            S.vol(:,:,j)=Lj;
        end
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': writing output']);
        
        S.vol=255*double(imfill(S.vol>0,'holes'));
        myMRIwrite(S,outputfile);
        
        disp(['Volume ' num2str(i) ' of ' num2str(length(d)) ': done!']);
        disp(' ');
        
    end
end
disp('All done!');



