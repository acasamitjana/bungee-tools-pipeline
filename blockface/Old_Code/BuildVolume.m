%clear

%addpath('~/Desktop/OneDrive - University College London/MATLAB/myFunctions/');
%addpath('/Applications/freesurfer60beta/freesurfer/matlab/');
addpath('/usr/local/freesurfer/matlab');
addpath('~/matlab/myFunctions/');

%inputDir='/home/matman/matlab/C1.1_registered/cropped';
%inputDir='/home/matman/matlab/registered/cropped';
%inputDir='~/Downloads/P57-16_A3.3/renamed/registered/cropped/';
outputVolumeGrayscale=[inputDir '/volume.gray.nii.gz'];
outputVolumeRGB=[inputDir '/volume.rgb.nii.gz'];

% inputDir='~/Downloads/P57-16_A3.3/renamed/registered/croppedNoCorrection/';
% outputVolumeGrayscale=[inputDir '/volume.noCorrection.gray.nii.gz'];
% outputVolumeRGB=[inputDir '/volume.noCorrection.rgb.nii.gz'];


% Go over target images
d=dir([inputDir '/*.jpg']);
for i=1:length(d)
    disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': working on it']);
    I=imread([inputDir '/' d(i).name]);
    if i==1
        V=zeros([size(I,1) size(I,2) length(d) 3],'uint8');
    end
    V(:,:,i,:)=reshape(I,[size(I,1) size(I,2) 1 3]);
end

mri=[];
mri.vol=V;
mri.vox2ras0=[0.1 0 0 0; 0 0.1 0 0; 0 0 0.025 0; 0 0 0 1];
mri.volres=[0.1 0.1 0.025];
% This is a bit stupid, but directory names with spaces were giving me trouble ...
curDir = pwd;
[pathstr, name, ext] = fileparts(outputVolumeRGB);
cd(pathstr);
myMRIwrite(mri,[name ext]);
cd(curDir); 

mri.vol=uint8(mean(single(mri.vol),4));
curDir = pwd;
[pathstr, name, ext] = fileparts(outputVolumeGrayscale);
cd(pathstr);
myMRIwrite(mri,[name ext]);
cd(curDir); 




    