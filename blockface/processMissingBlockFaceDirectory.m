% This is a long script that, without any user interaction, takes a bunch
% of photographs renamed by RenameBlockfacePhotos.m, and builds three
% volumes: one grayscale, one RBG, and one probabilistic segmentation.
% It has 3 distinct parts: the first one registers the photos; the second
% resamples and segments with a fully convolutional network; and the third
% simply stacks photos to build volumes.
clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
YamlStruct = ReadYaml([fileparts(mfilename('fullpath')) filesep '..' filesep 'configFile.yaml']);
 
% NiftyReg executables 
ALADIN=[YamlStruct.niftyReg 'reg-apps/reg_aladin']; 
RESAMPLE=[YamlStruct.niftyReg '/reg-apps/reg_resample'];

addpath(YamlStruct.utilsFunc);

BFres=0.1;
ORIG_VOLRES = [0.1 0.1 0.025];
TARGET_RES = [0.5 0.5 0.025];
Hres = 25.4/6400;
SIZ=[740 520]; % actual dimensions of cassette 74 x 52 mm
blockName = 'P41-16_P3.3';
concurrentBlockName = 'P41-16_P3.1';
stain = 'LFB';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get input directory
histoDir = YamlStruct.HistoDir;
histoBlockDir = [histoDir filesep blockName filesep stain];

blockVol=[YamlStruct.initBlocks filesep concurrentBlockName '_volume.gray.initialized.mgz'];
initBlockVol=[YamlStruct.initBlocks filesep 'init_' blockName '_volume.gray.initialized.mgz'];
outputDir=[YamlStruct.initBlocks filesep];
outputVol=[outputDir filesep blockName '_volume.gray.initialized.old.mgz'];
outputVolRGB=[outputDir filesep blockName '_volume.rgb.initialized.old.mgz'];
outputVolMask=[outputDir filesep blockName '_volume.mask.initialized.old.mgz'];


if exist(outputDir,'dir')==0
    mkdir(outputDir);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Read histology
% load mapping file and build structure for output MRI
load([histoDir filesep blockName filesep 'LFB' filesep blockName '_LFB_mapping.mat'],'mapping');  

% Is this the most posterior block of the cerebrum, or the most lateral
% of a cerebellum? If so, we need to flip mapping
need_to_flip = 0;
if strcmp(blockName,'P57-16_P8.1') || strcmp(blockName,'P41-16_P9.1') || strcmp(blockName,'P57-16_C5.1') || strcmp(blockName,'P41-16_C4.1')
    need_to_flip = 1;
    mapping = size(mriI.vol,3) + 1 - mapping;
end
% 
% mri2 = [];
% mri2.vox2ras0 = diag([BFres * ones(1,2) 0.025 * (mapping(2)-mapping(1)) 1]);
% mri2.volres = sqrt(sum(mri2.vox2ras0(1:3,1:3).^2));
% mri2.vol = [];

% Stack of histology: flip and/or rotation?
Hstack = {};
HGRAYstack = {};
Mstack = {};
for j = 1 : length(mapping)

    H = imread([histoBlockDir filesep blockName '_' stain '_' num2str(j,'%.2d') '.jpg']);
    Hresized = imresize(H,Hres/BFres);
    M = imread([histoBlockDir filesep blockName '_' stain '_' num2str(j,'%.2d') '.mask.png']);
    Mresized = imresize(M,[size(Hresized,1) size(Hresized,2)]);
    
    for it_c=1:3
        Hresized(:,:,it_c) = uint8(double(Hresized(:,:,it_c)).*double(Mresized(:,:,1)>127));
    end
    
    HGRAYresized = rgb2gray(Hresized);
    Hstack{j} = Hresized;
    HGRAYstack{j} = HGRAYresized;
    Mstack{j} = Mresized;
    
end

%% Initialization
Bmri = myMRIread(blockVol);
orig_vox2ras0 = diag([ORIG_VOLRES 1]);
T = Bmri.vox2ras0 * inv(orig_vox2ras0);

volInit = zeros([size(HGRAYresized) length(HGRAYstack)]);
for it_stack=1:length(HGRAYstack)
    volInit(:,:,it_stack)=HGRAYstack{it_stack};
end
% volInit = zeros([ size(HGRAYresized) size(Bmri.vol,3) ]);
% for it_stack=1:length(HGRAYstack)
%     for it_stack_2=1:10
%         volInit(:,:,9+(it_stack-1)*10 + it_stack_2)=HGRAYstack{it_stack};
%     end
% end
mriInit = [];
mriInit.vox2ras0 = diag([TARGET_RES 1]); %diag([BFres * ones(1,3) 1]);
mriInit.vox2ras0(3,4) = (min(mapping)-4.5)*ORIG_VOLRES(3);
mriInit.vox2ras0 = T * mriInit.vox2ras0;
mriInit.volres = sqrt(sum(mriInit.vox2ras0(1:3,1:3).^2));
mriInit.vol = volInit;
myMRIwrite(mriInit,initBlockVol);

% mriInit=Bmri;
% mriInit.vol = volInit;

%% Affine
%Affine to previous slice
flo=[tempdir filesep 'flo.png'];
florgb=[tempdir filesep 'florgb.png'];
flomask=[tempdir filesep 'flo.mask.png'];

ref=[tempdir filesep 'ref.png'];
refmask=[tempdir filesep 'ref.mask.png'];

aff=[tempdir filesep 'aff.txt'];
res=[tempdir filesep 'res.png'];

mri2 = mriInit;
mri2.vol = [];
mri2mask = mriInit;
mri2mask.vol = [];
mri2rgb = mriInit;
mri2rgb.vol = [];

mri2.vol = zeros([size(HGRAYresized)  length(mapping)]);
mri2mask.vol = mri2.vol;
mri2rgb.vol = repmat(mri2.vol,[1 1 1 3]);
% and assign
if need_to_flip
    mri2.vol(:,:,end-(j-1)) = HGRAYstack{1};
    mri2mask.vol(:,:,end-(j-1))= Mstack{1};
    REGRGB = Hstack{1}; 
    for c=1:3, mri2rgb.vol(:,:,end-(j-1))=REGRGB(:,:,c); end
else
    mri2.vol(:,:,j)=HGRAYstack{1};
    mri2mask.vol(:,:,j)=Mstack{1};
    REGRGB = Hstack{1};
    for c=1:3, mri2rgb.vol(:,:,j)=REGRGB(:,:,c); end

end


%% NR
for j = 2:length(mapping)
    disp(['Registering slice ' num2str(j) '/' num2str(length(mapping))])
    REF = HGRAYstack{j-1};
    FLO = HGRAYstack{j};
    FLOMASK = Mstack{j};
    FLOrgb = Hstack{j};
    
    imwrite(REF,ref);
    imwrite(FLO,flo);
    imwrite(FLOMASK,flomask);
    imwrite(FLOrgb,florgb);
    
    ALADINcmd=[ALADIN ' -ref ' ref ' -flo ' flo ' -aff ' aff ' -res ' res ' -rigOnly -pad 0 >/dev/null'];
    REScmd=[RESAMPLE ' -ref ' ref ' -flo ' flo ' -aff ' aff ' -res ' res ' -pad 0 >/dev/null'];
    system(ALADINcmd);
    system(REScmd);
    REG = imread(res);

    REScmd=[RESAMPLE ' -ref ' ref ' -flo ' flomask ' -aff ' aff ' -res ' res ' -pad 0 >/dev/null'];
    system(REScmd);
    REGMASK = imread(res);

    REGRGB=repmat(REG,[1 1 3]);
    for c=1:3
        imwrite(FLOrgb(:,:,c),florgb);
        REScmd=[RESAMPLE ' -ref ' ref ' -flo ' florgb ' -aff ' aff ' -res ' res ' -pad 0 >/dev/null'];
        system(REScmd);
        REGRGB(:,:,c) = imread(res);
    end

    % Allocate if necessary
    if isempty(mri2.vol)
        mri2.vol = zeros([size(REG) size(Bmri.vol,3)]);
        % And for the mask and rgb
        mri2mask.vol = mri2.vol;
        mri2rgb.vol = repmat(mri2.vol,[1 1 1 3]);
    end

    % and assign
    if need_to_flip
        mri2.vol(:,:,end-(j-1))=REG;
        mri2mask.vol(:,:,end-(j-1))=REGMASK;
        for c=1:3, mri2rgb.vol(:,:,end-(j-1))=REGRGB(:,:,c); end
    else
        mri2.vol(:,:,j)=REG;
        mri2mask.vol(:,:,j)=REGMASK;
        for c=1:3, mri2rgb.vol(:,:,j)=REGRGB(:,:,c); end

    end

end


% Bmri = myMRIread(blockVol);
% orig_vox2ras0 = diag([ORIG_VOLRES 1]);
% T = Bmri.vox2ras0 * inv(orig_vox2ras0);
% 
% % Tscaling = [1 0 0 0; 0 1 0 0; 0 0 (mapping(2)-mapping(1)) 0; 0 0 0 1];
% volInit = zeros([size(HGRAYresized) length(HGRAYstack)]);
% for it_stack=1:length(HGRAYstack)
%     volInit(:,:,it_stack)=HGRAYstack{it_stack};
% end
% mriInit = [];
% mriInit.vox2ras0 = diag([BFres * ones(1,3) 1]);
% mriInit.vox2ras0(3,4) = (min(mapping)-4.5)*ORIG_VOLRES(3);
% mriInit.vox2ras0 = T * mri2.vox2ras0;
% mriInit.volres = sqrt(sum(mri2.vox2ras0(1:3,1:3).^2));



myMRIwrite(mri2,outputVol);
myMRIwrite(mri2mask,outputVolMask);
myMRIwrite(mri2rgb,outputVolRGB);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Affine using NR to the mid slice
% Stack them all
% What vox2ras to use?



