% This script tains a segmentation network (fully convolutional network or FCN)
% from the training examples gathered by GenerateTrainingData.m, to segment tissue vs wax
% The output is a mat file with the network weights; the latest one we have is
% FCN_resc2_Nov18.mat. This code requires:
% 1. A GPU.
% 2. Modifying Matlab's imageDataAugmenter class with the code in
%    BrightContrAugmenter.txt (you may want to make a backup first)
clear
clc

% Input directory with training data
inputDir='./trainingDataSegmentBlockFace';
% I downsample images by this factor
rescalingFactor=2;
% Output mat file with network / weights
outputFile='FCN_resc2_Nov18.mat';
% GPU to use
gpuDevInd=1;


%%%%%%%%%%%%%%%%%%%%%%%%%%
% Don't touch from here on
%%%%%%%%%%%%%%%%%%%%%%%%%%

% Temporary directories with images and labels
imageDir=[tempdir filesep 'trainingImages'];
labelDir=[tempdir filesep 'trainingLabels'];

if exist(imageDir,'dir'), system(['rm -rf ' imageDir]); end
if exist(labelDir,'dir'), system(['rm -rf ' labelDir]); end

mkdir(imageDir);
mkdir(labelDir);

% GPU device
gpuDevice(gpuDevInd);

% Gather images
disp('Preparing input files');
d=dir([inputDir filesep '*.mask.png']);
for i=1:length(d)
    
    if rescalingFactor==1
        copyfile([inputDir filesep d(i).name],[labelDir filesep 'im_' num2str(i,'%.4d') '.png']);
        copyfile([inputDir filesep d(i).name(1:end-9) '.png'],[imageDir filesep 'im_' num2str(i,'%.4d') '.png']);
    else
        I=imread([inputDir filesep d(i).name(1:end-9) '.png']);
        M=imread([inputDir filesep d(i).name]);
        
        Ir=imresize(I,1/rescalingFactor);
        Mr=uint8(255*double(double(imresize(M,1/rescalingFactor))>128));
        
        imwrite(Ir,[imageDir filesep 'im_' num2str(i,'%.4d') '.png']);
        imwrite(Mr,[labelDir filesep 'im_' num2str(i,'%.4d') '.png']);
    end
end

% Classes and sizes of dataset
classes = [
    "Background"
    "Brain"
    ];
imageSize = [260 370 3];
numClasses = numel(classes);
labelIDs=[0 255];

% data stores
imds = imageDatastore(imageDir);
pxds = pixelLabelDatastore(labelDir,classes,labelIDs);

% Create net
disp('Preparing network architecture and preloading VGG weights');
lgraph = segnetLayers(imageSize,numClasses,'vgg16');
pxLayer = pixelClassificationLayer('Name','labels','ClassNames', classes);
lgraph = removeLayers(lgraph, 'pixelLabels');
lgraph = addLayers(lgraph, pxLayer);
lgraph = connectLayers(lgraph, 'softmax' ,'labels');

% surgery to increase learning rates of the 2 rightmost convlutional layers
% For some reason matlab doesn't let you change the LR directly, so you
% have to extact the layer, change the learning rate, and plug it back in

% figure, plot(lgraph), title('Before surgery')

decoder1_conv1=lgraph.Layers(87);
decoder1_conv1.BiasLearnRateFactor=20;
decoder1_conv1.WeightLearnRateFactor=20;
lgraph = removeLayers(lgraph, 'decoder1_conv1');
lgraph = addLayers(lgraph, decoder1_conv1);
lgraph = connectLayers(lgraph, 'decoder1_relu_2' ,'decoder1_conv1');
lgraph = connectLayers(lgraph, 'decoder1_conv1' ,'decoder1_bn_1');

decoder1_conv2=lgraph.Layers(84);
decoder1_conv2.BiasLearnRateFactor=20;
decoder1_conv2.WeightLearnRateFactor=20;
lgraph = removeLayers(lgraph, 'decoder1_conv2');
lgraph = addLayers(lgraph, decoder1_conv2);
lgraph = connectLayers(lgraph, 'decoder1_unpool' ,'decoder1_conv2');
lgraph = connectLayers(lgraph, 'decoder1_conv2' ,'decoder1_bn_2');


decoder2_conv1=lgraph.Layers(80);
decoder2_conv1.BiasLearnRateFactor=20;
decoder2_conv1.WeightLearnRateFactor=20;
lgraph = removeLayers(lgraph, 'decoder2_conv1');
lgraph = addLayers(lgraph, decoder2_conv1);
lgraph = connectLayers(lgraph, 'decoder2_relu_2' ,'decoder2_conv1');
lgraph = connectLayers(lgraph, 'decoder2_conv1' ,'decoder2_bn_1');

decoder2_conv2=lgraph.Layers(77);
decoder2_conv2.BiasLearnRateFactor=20;
decoder2_conv2.WeightLearnRateFactor=20;
lgraph = removeLayers(lgraph, 'decoder2_conv2');
lgraph = addLayers(lgraph, decoder2_conv2);
lgraph = connectLayers(lgraph, 'decoder2_unpool' ,'decoder2_conv2');
lgraph = connectLayers(lgraph, 'decoder2_conv2' ,'decoder2_bn_2');

% figure, plot(lgraph), title('After surgery')


% Training options
options = trainingOptions('sgdm', ...
    'Momentum', 0.9, ...
    'InitialLearnRate', 5e-2, ... %1e-4   actually, even 0.1 works well ... 
    'L2Regularization', 0.0001, ...
    'MaxEpochs', 5000, ...
    'MiniBatchSize', 10, ...
    'Shuffle', 'every-epoch', ...
    'VerboseFrequency', 2, ...
    'Plots','training-progress');

% Augmentation
augmenter = imageDataAugmenter('RandXReflection',true,...
    'RandYReflection',true, ...
    'RandContrast',[0.85 1.15], ...
    'RandBrightness',[-20 20],...
    'RandXTranslation', [-8 8], ...
    'RandYTranslation',[-8 8], ...
    'RandXScale',[0.95 1.05], ...
    'RandYScale',[0.95 1.05], ...
    'RandRotation',[-5 5], ...
    'RandXShear',[-3 3], ...
    'RandYShear',[-3 3]);


datasource = pixelLabelImageSource(imds,pxds,'DataAugmentation',augmenter);


% Actual training
[trainedNet, info] = trainNetwork(datasource,lgraph,options);

disp('Training done! Saving to disk');
save(outputFile,'rescalingFactor','trainedNet');
disp('Saved!'); disp(' ');


% Show outputs
disp('Showing outputs for the heck of it; feel free to stop when you want');
figure
imds = imageDatastore(imageDir);
while true
    I = read(imds);
    POST = activations(trainedNet,I,'softmax');
    [~,idx]=max(POST,[],3);
    SEG=labelIDs(idx);
    
    subplot(1,3,1), imshow(I)
    subplot(1,3,2), imshow(uint8(255*POST(:,:,2)))
    CONTOUR=SEG & ~imerode(SEG,[0 1 0; 1 1 1; 0 1 0]);
    Icontour=zeros(size(I),'uint8');
    red=I(:,:,1); red(CONTOUR)=255; Icontour(:,:,1)=red;
    green=I(:,:,2); green(CONTOUR)=0; Icontour(:,:,2)=green;
    blue=I(:,:,3); blue(CONTOUR)=0; Icontour(:,:,3)=blue;
    subplot(1,3,3), imshow(Icontour);
    pause
end

