% Goes around a bunch of volumes, randomly extracts slices, and asks the 
% user to annotate masks
% Just put a bunch of rgb blockface volumes in an input directory, specify the
% input and output directories below, and label the images when prompted
clear
close all
figure

% Input directory with a bunch of *.rgb.nii.gz blockface volumes
inputDir='~/Downloads/';
% Output directory
outputDir=[pwd() filesep 'trainingDataSegmentBlockface' filesep];
% How many randomly selected images you'll be  asked to label from each volume
IMS_PER_VOL=3;

%%%%%%%%%  don't touch from here on   %%%%%%%%%%%%

addpath(genpath(['..' filesep 'functions' filesep]));

if exist(outputDir,'dir')==0
    mkdir(outputDir);
end

strel=[0 1 0; 1 1 1; 0 1 0]>0;

d=dir([inputDir filesep '*.nii.gz']);
for i=1:length(d)
    
    disp(['Volume ' num2str(i) ' of ' num2str(length(d))]);
    
    disp('  Reading in volume');
    A=myMRIread([inputDir filesep d(i).name]);
    
    rp=randperm(size(A.vol,3));
    for j=1:IMS_PER_VOL
        disp(['    Section ' num2str(j) ' of ' num2str(IMS_PER_VOL)]);
        
        IM=uint8(squeeze(A.vol(:,:,rp(j),:)));
        M=zeros([size(A.vol,1), size(A.vol,2)])>0;
        IMm=IM;
                        
        imshow(IM);
        more='Yes';
        while strcmp(more,'Yes')           
             more = questdlg('Do you want to add another connected component to the mask?',' ','Yes','No','Yes');
             if strcmp(more,'Yes')
                 imshow(IMm);
                 h=impoly();
                 position=wait(h);
                 mask=poly2mask(position(:,1), position(:,2),size(A.vol,1),size(A.vol,2));
                 M=M | mask;
                 ME = imerode(imerode(imerode(M,strel),strel),strel);
                 Mb = M & ~ME;
                 aux=IM(:,:,1); aux(Mb)=255; IMm(:,:,1)=aux;
                 aux=IM(:,:,2); aux(Mb)=0;   IMm(:,:,2)=aux;
                 aux=IM(:,:,3); aux(Mb)=0;   IMm(:,:,3)=aux;
                 imshow(IMm);
             end
        end
        
        f=find(d(i).name=='_');
        f=f(2);
        imwrite(IM,[outputDir filesep d(i).name(1:f) num2str(rp(j),'%.3d') '.png']);
        imwrite(uint8(255*double(M>0)),[outputDir filesep d(i).name(1:f) num2str(rp(j),'%.3d') '.mask.png']);
        
    end    
    disp(' ');
end
disp('All done!')
close all


