% This script goes over the pictures of an input directory, and lets you
% double check that there are no duplicates / missing photos while renaming
% the files. At the end, it also prompts the user to select a reference photo 
% for the block, as well as to mark the corners of the green cassette on it: 
% a small effort providing increased robustness later down the (processing) road.
clear, close all, clc

addpath(genpath(['..' filesep 'functions' filesep]));


inputDirectory = uigetdir('','Please select input directory:');
outputDirectory = [inputDirectory filesep 'renamed' filesep];

if exist(outputDirectory,'dir')==0
    mkdir(outputDirectory);
end


% Go over images, copying / renaming
locationMatFile=[outputDirectory filesep 'location.mat'];
if exist(locationMatFile,'file')>0
    load(locationMatFile,'blockName','d','i','count','iHistory','countHistory');
     happy = questdlg('Did you make a mistake and want to go back?',' ','Yes','No','No');
     if strcmp(happy,'Yes')
         prompt = {'How many images back?'};
         dlg_title = ' ';
         num_lines = 1;
         defaultans = {'5'};
         answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
         goback=str2double(answer{1});
         countHistory=countHistory(1:end-goback);
         iHistory=iHistory(1:end-goback);
         count=countHistory(end);
         i=iHistory(end);
     end
else
    blockName = inputdlg('Please provide block name (no spaces!)');
    blockName=blockName{1};
    if any(blockName==' ')
        error('Seriously, no spaces, please');
    end
    d=dir([inputDirectory filesep '*.JPG']);
    count=0;
    i=0;
    countHistory=[];
    iHistory=[];
end
while i<length(d)
    i=i+1;
    
    outputFileName=[outputDirectory filesep blockName '_' num2str(count+1,'%.3d') '.JPG'];
    if exist(outputFileName,'file')>0
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': output already there']);
    else
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': working on it']);
    end
    
    A=imread([inputDirectory filesep d(i).name]);
    figure(1), imshow(A),
    expected=mod(count,10)+1;
    prev=mod(count-1,10)+1;
    next=mod(count+1,10)+1;
    choice = menu(['I am expecting number ' num2str(expected)],...
        ['This is ' num2str(expected)],...
        ['Shoot, I missed ' num2str(expected) ', this is a later one'],...
        ['Shoot, this is ' num2str(prev) '; I must have have taken multiple pictures of the previous one']);
    
    switch choice
        case 1, % if everything went well
            count=count+1;
            copyfile([inputDirectory filesep d(i).name],outputFileName);
            
        case 2, % I skipped a photo: copy the previous one (which is always there
            count=count+1;
            copyfile([outputDirectory filesep blockName '_' num2str(count-1,'%.3d') '.JPG'],[outputDirectory filesep blockName '_' num2str(count,'%.3d') '.JPG']);
            i=i-1; % I'll have to revisit this one
            
        case 3, % I took this photo more than once...
            % do nothing!
         
        otherwise,
            error('No proper choice given');
    end
    
    countHistory(end+1)=count;
    iHistory(end+1)=i;
    
    save(locationMatFile,'blockName','d','i','count','iHistory','countHistory');

end

% Almost done! Here we just find a nice reference image toward the center
% of the stack, and label the four corners on it
button = questdlg('We are done with the images!',' ','OK','OK');
outputMatFile=[outputDirectory filesep 'REFdata.mat'];
if exist(outputMatFile,'file')==0
    button = questdlg('We now need to find a clean reference image for this block',' ','OK','OK');
    d=dir([outputDirectory filesep '*.JPG']);
    i=round(length(d)/2)-1;
    happy='No';
    close all
    while strcmp(happy,'No')
        i=i+1;
        imageFilename=d(i).name;
        A=imread([outputDirectory filesep imageFilename]);
        figure(1), imshow(A)
        happy = questdlg('Are you happy with this one?',' ','Yes','No','Yes');
    end
    close all
    
    happy = 'No';
    while strcmp(happy,'No')
        close all, figure, imshow(A)
        button = questdlg('Please click on the four corners of the green cassette',' ','OK','OK');
        cornersREF=zeros(2,4);
        for c=1:4
            aux=ginput(1);
            cornersREF(:,c)=flipud(aux');
            hold on, plot(aux(1),aux(2),'r.','markersize',40); hold off
        end
        happy = questdlg('Are you happy with your selection?',' ','Yes','No','Yes');
    end
    close all
    
    % Reorder: NW, NE, SE, SW (Hungarian algorithm)
    NWness=cornersREF(1,:)+cornersREF(2,:);
    NEness=cornersREF(1,:)-cornersREF(2,:);
    SEness=-NWness;
    SWness=-NEness;
    [idx,cost] = munkres([NWness; NEness; SEness; SWness]);
    cornersREF=round(cornersREF(:,idx));
    
    save(outputMatFile, 'cornersREF','imageFilename');
end

disp('All done!')



