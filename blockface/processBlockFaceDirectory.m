% This is a long script that, without any user interaction, takes a bunch
% of photographs renamed by RenameBlockfacePhotos.m, and builds three
% volumes: one grayscale, one RBG, and one probabilistic segmentation.
% It has 3 distinct parts: the first one registers the photos; the second
% resamples and segments with a fully convolutional network; and the third
% simply stacks photos to build volumes.
clear
close all
clc

%%%%%%%%%%%%%%%%%

addpath(genpath(['..' filesep 'functions' filesep]));

% Set to 1 if you want to see some intermediate results
DEBUG=0;
% points that deviate more than this many pixels from global transform are outliers (Inf = no cleanup)
MAX_PRED_ERR=100;
% A bunch of reference files with photos of the microtome, masks, etc
globalRefImageFile=['.' filesep 'ref_images' filesep 'reference2.jpg'];
globalRefMaskFileSliding=['.' filesep 'ref_images' filesep 'reference2_mask_sliding.png'];
globalRefMaskFilePlateEdges=['.' filesep 'ref_images' filesep 'reference2_mask_plate.png'];
globalRefMaskFileBladeAndSupport=['.' filesep 'ref_images' filesep 'reference2_mask_bladeAndSupport.png'];
globalRefMaskFileOnlyBlade=['.' filesep 'ref_images' filesep 'reference2_mask_blade.png'];
% Manually annotated coordinates of the corners of the cassette in the reference image
cornersGlobalREF=[1760 2439 ; 1764 3596 ; 2607 3588 ; 2598 2435 ]';

% Get input directory
inputDir = uigetdir('','Please select input directory (typically: ''XXXX/renamed''):');
inputDir(end+1)=filesep;
outputDir=[inputDir filesep 'registered' filesep];

% Is this the most posterior block of the cerebrum, or the most lateral
% of a cerebellum?
NeedToFlip = questdlg(['Is this the most posterior block of the cerebrum, ' ...
    'or the most lateral of a cerebellum?'],' ','Yes','No','No');
save NeedToFlip.mat NeedToFlip

if exist(outputDir,'dir')==0
    mkdir(outputDir);
end

if exist([inputDir filesep 'REFdata.mat' ],'file')
    copyfile([inputDir filesep 'REFdata.mat'], outputDir);
end


disp(' ');
disp('****************************');
disp('*  PART 1/3:  REGISTRATION *');
disp('****************************');
disp(' ');


% read in global reference
disp('Reading in global reference image / masks');
globalREFrgb=imread(globalRefImageFile);
globalREF=rgb2gray(globalREFrgb);
globalREFmaskSliding=imread(globalRefMaskFileSliding)>0;
globalREFmaskPlateEdges=imdilate(imread(globalRefMaskFilePlateEdges)>0,strel('disk',20));
globarREFmaskPlateEdgesDilated=imdilate(globalREFmaskPlateEdges,strel('disk',20));
globalREFmaskBladeAndSupport=imread(globalRefMaskFileBladeAndSupport)>0;
globalREFmaskBlade=imread(globalRefMaskFileOnlyBlade)>0;

% Detect SURF points for global reference (I guess I could save them to a
% file, but whatever... it's jut 2 seconds)
disp('Detecting SURF points for global reference');
points1gr = detectSURFFeatures(globalREF,'NumOctaves',5);
[f1gr,vpts1gr] = extractFeatures(globalREF,points1gr);
idx1a=find(globalREFmaskSliding(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1aGr=points1gr(idx1a); f1aGr=f1gr(idx1a,:); vpts1aGr=vpts1gr(idx1a);
idx1b=find(globalREFmaskPlateEdges(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1bGr=points1gr(idx1b); f1bGr=f1gr(idx1b,:); vpts1bGr=vpts1gr(idx1b);
idx1aa=find(globalREFmaskBladeAndSupport(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1aaGr=points1gr(idx1aa); f1aaGr=f1gr(idx1aa,:); vpts1aaGr=vpts1gr(idx1aa);
idx1bb=find(globalREFmaskBlade(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1bbGr=points1gr(idx1bb); f1bbGr=f1gr(idx1bb,:); vpts1bbGr=vpts1gr(idx1bb);

% Detect points for block-specific reference, with two purposes:
% a) Matching to global reference
% b) Matching to all other photos in this block
if exist([outputDir filesep 'REFdata.mat' ],'file')
    load([outputDir filesep 'REFdata.mat' ],'imageFilename');
    [pathstr,name,ext] = fileparts(imageFilename);
    REFrgb=imread([inputDir filesep name  ext]);
else
    
    disp('Picking a suitable reference image for this block');
    d=dir([inputDir  filesep '*.JPG']);
    i=round(length(d)/2)-1;
    happy='No';
    while strcmp(happy,'No')
        i=i+1;
        imageFilename=[inputDir filesep d(i).name];
        REFrgb=imread(imageFilename);
        figure(1), imshow(REFrgb)
        happy = questdlg('Are you happy with this one?',' ','Yes','No','Yes');
    end
    close all
end
REF=rgb2gray(REFrgb);

disp('Detecting SURF points for block reference');
points1 = detectSURFFeatures(REF,'NumOctaves',5);
[f1,vpts1] = extractFeatures(REF,points1);

disp('Matching global and block reference: sliding part')
% matching based on whole sliding part
indexPairs = matchFeatures(f1aGr,f1) ;
matchedPoints1 = vpts1aGr(indexPairs(:,1));
matchedPoints2 = vpts1(indexPairs(:,2));
if DEBUG
    close all, figure; showMatchedFeatures(globalREF,REF,matchedPoints1,matchedPoints2);
    legend('matched points 1','matched points 2');
    title('Alignment Global Reference vs. Block reference using sliding part of microtome');
    pause(3)
end
tform = estimateGeometricTransform(matchedPoints1,matchedPoints2,'affine','MaxNumTrials',10000,'Confidence',99.9999);
globalREFw = imwarp(globalREF,tform,'OutputView',imref2d(size(REF)));
REFmaskA = imwarp(globalREFmaskSliding,tform,'OutputView',imref2d(size(REF)))>0;

if DEBUG
    close all, figure, for k=1:3, imshow(REF), pause(0.5), imshow(REFmaskA), pause(0.5), end
end

disp('Matching global and block reference: blade and support')
indexPairs = matchFeatures(f1aaGr,f1) ;
matchedPoints1 = vpts1aaGr(indexPairs(:,1));
matchedPoints2 = vpts1(indexPairs(:,2));
if DEBUG
    close all, figure; showMatchedFeatures(globalREF,REF,matchedPoints1,matchedPoints2);
    legend('matched points 1','matched points 2');
    title('Alignment Global Reference vs. Block reference using blade and its support frame');
    pause(3)
end
tform = estimateGeometricTransform(matchedPoints1,matchedPoints2,'affine','MaxNumTrials',10000,'Confidence',99.9999);
globalREFww = imwarp(globalREF,tform,'OutputView',imref2d(size(REF)));
REFmaskAA = imwarp(globalREFmaskBladeAndSupport,tform,'OutputView',imref2d(size(REF)))>0;
if DEBUG
    close all, figure, for k=1:3, imshow(REF), pause(0.5), imshow(REFmaskAA), pause(0.5), end
end

disp('   (refinement using only the blade)')
idx1a=find(globalREFmaskSliding(sub2ind(size(globalREF),round(points1gr.Location(:,2)),round(points1gr.Location(:,1)))));
points1aGr=points1gr(idx1a); f1aGr=f1gr(idx1a,:); vpts1aGr=vpts1gr(idx1a);

idxblade=find(REFmaskAA(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
f1blade=f1(idxblade,:);
vpts1blade=vpts1(idxblade);

indexPairs = matchFeatures(f1bbGr,f1blade) ;
matchedPoints1 = vpts1bbGr(indexPairs(:,1));
matchedPoints2 = vpts1blade(indexPairs(:,2));
if DEBUG
    close all, figure; showMatchedFeatures(globalREF,REF,matchedPoints1,matchedPoints2);
    legend('matched points 1','matched points 2');
    title('Alignment Global Reference vs. Block reference using only the blade');
    pause(3)
end
tform = estimateGeometricTransform(matchedPoints1,matchedPoints2,'affine','MaxNumTrials',10000,'Confidence',99.9999);
REFmaskBB = imwarp(globalREFmaskBlade,tform,'OutputView',imref2d(size(REF)))>0;
REFmaskBBdilated=imdilate(REFmaskBB,strel('disk',20));
if DEBUG
    close all, figure, for k=1:3, imshow(REF), pause(0.5), imshow(REFmaskBB), pause(0.5), end
end


% Get four points from user
if exist([outputDir filesep 'REFdata.mat' ], 'file')
    load([outputDir filesep 'REFdata.mat' ],'cornersREF');
else
    happy = 'No';
    while strcmp(happy,'No')
        close all, figure, imshow(REFrgb)
        button = questdlg('Please click on the four corners of the green cassette',' ','OK','OK');
        cornersREF=zeros(2,4);
        for c=1:4
            aux=ginput(1);
            cornersREF(:,c)=flipud(aux');
            hold on, plot(aux(1),aux(2),'r.','markersize',40); hold off
        end
        happy = questdlg('Are you happy with your selection?',' ','Yes','No','Yes');
    end
    close all
    
    % Reorder: NW, NE, SE, SW (Hungarian algorithm)
    NWness=cornersREF(1,:)+cornersREF(2,:);
    NEness=cornersREF(1,:)-cornersREF(2,:);
    SEness=-NWness;
    SWness=-NEness;
    [idx,cost] = munkres([NWness; NEness; SEness; SWness]);
    cornersREF=round(cornersREF(:,idx));
    save([outputDir filesep 'REFdata.mat' ], 'cornersREF','imageFilename');
end

% We can now easily warp the mask following the edges of the plate / cassette
tform = estimateGeometricTransform(flipud(cornersGlobalREF)',flipud(cornersREF)','affine');
REFmaskB = imwarp(globalREFmaskPlateEdges,tform,'OutputView',imref2d(size(REF)))>0;
REFmaskBdilated=imdilate(REFmaskB,strel('disk',20));

% We can now mask the points!
idx1a=find(REFmaskA(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1a=points1(idx1a); f1a=f1(idx1a,:); vpts1a=vpts1(idx1a);
idx1b=find(REFmaskB(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1b=points1(idx1b); f1b=f1(idx1b,:); vpts1b=vpts1(idx1b);

idx1aa=find(REFmaskAA(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1aa=points1(idx1aa); f1aa=f1(idx1aa,:); vpts1aa=vpts1(idx1aa);
idx1bb=find(REFmaskBB(sub2ind(size(REF),round(points1.Location(:,2)),round(points1.Location(:,1)))));
points1bb=points1(idx1bb); f1bb=f1(idx1bb,:); vpts1bb=vpts1(idx1bb);



% Go over target images (and save output base for later)
d=dir([inputDir filesep '*.JPG']);

baseoutputnameVolumes=[inputDir filesep  d(1).name(1:end-8)];
save baseoutputnameVolumes.mat baseoutputnameVolumes

for i=1:length(d)
    
    outputImageFile=[outputDir filesep d(i).name(1:end-4) '.warped.jpg'];
    outputFactorFile=[outputDir filesep d(i).name(1:end-4) '.ScalingFactors.mat'];
    
    if exist(outputImageFile,'file') && exist(outputFactorFile,'file')
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': output already there']);
    else
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': working on it']);
        
        % Read in
        I2rgb=imread([inputDir filesep    d(i).name]);
        I2=rgb2gray(I2rgb);
        
        % Detect points
        points2 = detectSURFFeatures(I2,'NumOctaves',5);
        [f2,vpts2] = extractFeatures(I2,points2);
        
        % matching based on whole sliding part
        indexPairs = matchFeatures(f1a,f2) ;
        matchedPoints1 = vpts1a(indexPairs(:,1));
        matchedPoints2 = vpts2(indexPairs(:,2));
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Alignment sliding part image');
            pause(3)
        end
        tform = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);
        I2w = imwarp(I2,tform,'OutputView',imref2d(size(REF)));
        if DEBUG
            close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w), pause(0.5), end
        end
        
        % Second pass: recompute feature, but use only points on edge of green cassette
        points2b = detectSURFFeatures(I2w,'NumOctaves',5);
        [f2b,vpts2b] = extractFeatures(I2w,points2b);
        idx2=find(REFmaskBdilated(sub2ind(size(REF),round(points2b.Location(:,2)),round(points2b.Location(:,1)))));
        points2b=points2b(idx2); f2b=f2b(idx2,:); vpts2b=vpts2b(idx2);
        indexPairs = matchFeatures(f1b,f2b) ;
        matchedPoints1 = vpts1b(indexPairs(:,1));
        matchedPoints2 = vpts2b(indexPairs(:,2));
        
        % Clean up (first transform should have provided good alignment)
        predErr=sqrt(sum((matchedPoints1.Location-matchedPoints2.Location).^2,2));
        mask=predErr<MAX_PRED_ERR;
        matchedPoints1=matchedPoints1(mask);
        matchedPoints2=matchedPoints2(mask);
        
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2w,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Aligment of plate');
            pause(3)
        end
        tform2 = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);
        
        % Composition
        TFORM=tform;
        TFORM.T=tform.T*tform2.T;
        if DEBUG
            I2w2 = imwarp(I2,TFORM,'OutputView',imref2d(size(REF)));
            close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w2), pause(0.5), end
        end
        
        WARPED=zeros(size(REFrgb),'uint8');
        for c=1:3
            WARPED(:,:,c)= imwarp(I2rgb(:,:,c),TFORM,'OutputView',imref2d(size(REF)));
        end
        imwrite(WARPED,outputImageFile);
        
        ScalingFactorCassette=det(TFORM.T(1:3,1:3));
        
        
        % Here we do something similar, but focusing on the alignment of
        % the blade
        indexPairs = matchFeatures(f1aa,f2) ;
        matchedPoints1 = vpts1aa(indexPairs(:,1));
        matchedPoints2 = vpts2(indexPairs(:,2));
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Alignment: blade and its support part');
            pause(3)
        end
        tform = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);
        I2w = imwarp(I2,tform,'OutputView',imref2d(size(REF)));
        if DEBUG
            close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w), pause(0.5), end
        end
        
        % Second pass for blade: recompute features, but use only points on the actual blade
        points2bb = detectSURFFeatures(I2w,'NumOctaves',5);
        [f2bb,vpts2bb] = extractFeatures(I2w,points2bb);
        idx2=find(REFmaskBBdilated(sub2ind(size(REF),round(points2bb.Location(:,2)),round(points2bb.Location(:,1)))));
        points2bb=points2bb(idx2); f2bb=f2bb(idx2,:); vpts2bb=vpts2bb(idx2);
        indexPairs = matchFeatures(f1bb,f2bb) ;
        matchedPoints1 = vpts1bb(indexPairs(:,1));
        matchedPoints2 = vpts2bb(indexPairs(:,2));
        
        % Clean up (first transform should have provided good alignment)
        predErr=sqrt(sum((matchedPoints1.Location-matchedPoints2.Location).^2,2));
        mask=predErr<MAX_PRED_ERR;
        matchedPoints1=matchedPoints1(mask);
        matchedPoints2=matchedPoints2(mask);
        
        if DEBUG
            close all, figure; showMatchedFeatures(REF,I2w,matchedPoints1,matchedPoints2);
            legend('matched points 1','matched points 2');
            title('Aligment of blade');
            pause(3)
        end
        tform2 = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);
        
        % Composition
        TFORM=tform;
        TFORM.T=tform.T*tform2.T;
        if DEBUG
            I2w2 = imwarp(I2,TFORM,'OutputView',imref2d(size(REF)));
            close all, for k=1:3, imshow(REF), pause(0.5), imshow(I2w2), pause(0.5), end
        end
        
        ScalingFactorBlade=det(TFORM.T(1:3,1:3));
        
        
        save(outputFactorFile,'ScalingFactorCassette','ScalingFactorBlade');
        
    end
end
disp('All done for Part 1!');


disp(' ');
disp('*********************************************');
disp('*   PART 2/3:  RESAMPLING  AND SEGMENTATION *');
disp('*********************************************');
disp(' ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This piping to glue a bunch of old scripts is ugly but works
inputDir=outputDir;

save inputDir.mat inputDir
clear
load inputDir.mat
delete inputDir.mat
DLmatFile='FCN_resc2_Nov18.mat';
load(DLmatFile,'rescalingFactor','trainedNet');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SIZ=[740 520]; % actual dimensions of cassette 74 x 52 mm

outputDirCropped=[inputDir filesep 'cropped' filesep];
if exist(outputDirCropped,'dir')==0
    mkdir(outputDirCropped);
end

load([inputDir filesep 'REFdata.mat'],'cornersREF');

% Part 1: compute zoom factors
disp('We first read in the zoom factors, and smooth them');
d=dir([inputDir filesep '*.jpg']);
factorsBlade=zeros([1,length(d)]);
factorsCassette=zeros([1,length(d)]);
for i=1:length(d)
    factorFile=[inputDir filesep d(i).name(1:end-11) '.ScalingFactors.mat'];
    load(factorFile,'ScalingFactorCassette','ScalingFactorBlade');
    factorsBlade(i)=ScalingFactorBlade;
    factorsCassette(i)=ScalingFactorCassette;
    %factors(i)=ScalingFactorBlade/ScalingFactorCassette;
end
% Bear in mind that the factors are area ratios; we take square root so
% they are per dimension
factors=sqrt(factorsBlade./factorsCassette);

% We need not only to map cornersREF to cornersTarget, but also enlarge
% image i by factors(i)

% Estimate affine transform
for i=1:length(d)
    
    outputFileCropped=[outputDirCropped filesep d(i).name(1:end-4) '.cropped.jpg'];
    outputFileCroppedMask=[outputDirCropped filesep d(i).name(1:end-4) '.cropped.mask.png'];
    
    if exist(outputFileCropped,'file') && exist(outputFileCroppedMask,'file')
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': output already there']);
    else
        disp(['Image ' num2str(i) ' of ' num2str(length(d)) ': working on it']);
        
        I=imread([inputDir filesep d(i).name]);
        
        margin=SIZ*(factors(i)-1)/2;
        
        cornersTarget=[1-margin(2) 1-margin(1); 1-margin(2) SIZ(1)+margin(1);...
            SIZ(2)+margin(2) SIZ(1)+margin(1); SIZ(2)+margin(2) 1-margin(1)]';
        
        A=[cornersREF(2,:)' cornersREF(1,:)' zeros(4,2) ones(4,1) zeros(4,1);
            zeros(4,2) cornersREF(2,:)' cornersREF(1,:)'  zeros(4,1) ones(4,1)];
        b=[cornersTarget(2,:)'; cornersTarget(1,:)'];
        aux=A\b;
        tform=affine2d([aux(1) aux(3) 0; aux(2) aux(4) 0; aux(5) aux(6) 1]);
        
        WARPED = imwarp(I,tform,'OutputView',imref2d(fliplr(SIZ)));
        
        imwrite(uint8(WARPED),outputFileCropped);
        
        % Segmentation part
        Ij=uint8(uint8(WARPED));
        Ij=imresize(Ij,1/rescalingFactor);
        [labelsJ,~,scoresJ]=semanticseg(Ij,trainedNet);
        Lj=uint8(255*double(scoresJ(:,:,2)));
        Lj=imresize(Lj,[size(WARPED,1) size(WARPED,2)]);
        imwrite(Lj,outputFileCroppedMask);
    end
end

disp('All done for Part 2!');



disp(' ');
disp('*******************************');
disp('* PART 3/3:  BUILDING VOLUMES *');
disp('*******************************');
disp(' ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This piping to glue a bunch of old scripts is ugly but works
save inputDir.mat inputDir
clear
load inputDir.mat
delete inputDir.mat
idir=inputDir;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(['..' filesep 'niftitools']);

load baseoutputnameVolumes.mat
delete baseoutputnameVolumes.mat

inputDir=[idir filesep 'cropped' filesep];
outputVolumeGrayscale=[baseoutputnameVolumes '_volume.gray.nii.gz'];
outputVolumeRGB=[baseoutputnameVolumes '_volume.rgb.nii.gz'];
outputVolumeMask=[baseoutputnameVolumes '_volume.mask.nii.gz'];

% Go over target images
d=dir([inputDir filesep '*.jpg']);
for i=1:length(d)
    disp(['  Image ' num2str(i) ' of ' num2str(length(d)) ': working on it']);
    I=imread([inputDir filesep d(i).name]);
    M=imread([inputDir filesep d(i).name(1:end-4) '.mask.png']);
    if i==1
        V=zeros([size(I,1) size(I,2) length(d) 3],'uint8');
        Vm=zeros([size(I,1) size(I,2) length(d)],'uint8');
    end
    V(:,:,i,:)=reshape(I,[size(I,1) size(I,2) 1 3]);
    Vm(:,:,i)=M;
end
V=permute(V,[2 1 3 4]);
Vm=permute(Vm,[2 1 3]);

% For the most posterior block of the cerebrum or most lateral of the
% cerebellum, we need to flip volume left right and top bottom
load NeedToFlip.mat NeedToFlip
if strcmp(NeedToFlip,'Yes')
    V=flip(V,3);
    Vm=flip(Vm,3);
    V=flip(V,2);
    Vm=flip(Vm,2);
end
% This is a bit stupid, but directory names with spaces were giving me trouble ...
% RGB
nii=make_nii(V,[0.1 0.1 0.025],[1 1 1],2);
curDir = pwd;
[pathstr, name, ext] = fileparts(outputVolumeRGB);
cd(pathstr);
save_nii(nii,[name ext]);
cd(curDir);

% Mask
nii=make_nii(Vm,[0.1 0.1 0.025],[1 1 1],2);
curDir = pwd;
[pathstr, name, ext] = fileparts(outputVolumeMask);
cd(pathstr);
save_nii(nii,[name ext]);
cd(curDir);

% Grayscale
nii=make_nii(uint8(mean(single(V),4)),[0.1 0.1 0.025],[1 1 1],2);
curDir = pwd;
[pathstr, name, ext] = fileparts(outputVolumeGrayscale);
cd(pathstr);
save_nii(nii,[name ext]);
cd(curDir);

% Delete temporary files 
delete([pathstr filesep 'registered' filesep 'cropped' filesep '*.*']);
rmdir([pathstr filesep 'registered' filesep 'cropped']);
delete([pathstr filesep 'registered' filesep '*.*']);
rmdir([pathstr filesep 'registered']);

disp('All done for Part 3!');
disp(' ');
disp('ALL DONE!');
disp(' ');
disp(['Output is in ' idir]);

