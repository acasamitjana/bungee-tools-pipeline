# HISTO-RECON
> Functions to register the histology to the MRI in meaningful RAS space

## USERS 

RegisterHistoBlock.m

This is the main script. It essentially goes over a block:

- asking the user for the rotation and/or flip to match blockface (MRI) & histo

- matching the histogram of the histo sections to that of the MRI

- using the technique from the 2018 MICCAI paper (Iglesias et al) to register 
  the stacks of histo and (resampled) MRI, using downsampled images

- composes a (slice-wise) equalized histo volume at lowres.

- produces a set of niftis (one per section) with the registered histology at full-res.


DeformHistoLabels.m:

This script deforms the manual segmentations of a block. This is an alternative to
doing it along with the intensities in RegisterHistoBlock.m; it may be a good idea 
to merge them both in the future!




## DEVELOPERS 

deformHighRes.m

This script deforms a region of a microscopically scanned section using
a field estimated by RegisterHistoBlock.m. Good for presentations, etc.


## PACKAGES 

refineStackRegistration

Implementation of MICCAI 2018 algorithm. This implementation is publicly 
available at www.jeiglesias.com.


## AUXILIARY 

matchHistoMasked.m

Matches the histogram of an image to either: (a) a reference histogram; or
(b) the histogram of other image. It ignores pixels/voxels equal to zero.

niftyConfig.txt

Configuration file for the package RefineStackRegistration mentined above.


## TODO 

It may be clever to write a separate script that goes over blocks asking the
user to provide the rotation / flips w.r.t. the blockface / MRI, so that you
can then run multiple blocks without any interaction.

We eventually need to add the code to warp labels. It will be very similar to
the code in lines (approx) 200-250 in RegisterHistoBlock.m.

And of course, there's plenty of space for improvement in the registration.



