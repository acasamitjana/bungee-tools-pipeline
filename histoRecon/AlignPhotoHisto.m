%  This script essentially goes over a block:
% - asking the user for the rotation and/or flip to match blockface (MRI) & histo
% - matching the histogram of the histo sections to that of the MRI
% - using the technique from the 2018 MICCAI paper (Iglesias et al) to register 
%   the stacks of histo and (resampled) MRI, using downsampled images
% - composes a (slice-wise) equalized histo volume at lowres.
% - producing a set of niftis (one per section) with the registered histology at full-res.

% Nominal resolutions of blockface and histo (you probably shouldn't touch these)
BFres=0.1;
Hres=3.9688e-3; % = 25.4/6400;

if exist([pwd() filesep '..' filesep 'configFile.yaml'])
    YamlStruct = ReadYaml([pwd() filesep '..' filesep 'configFile.yaml']);
    histoDir = YamlStruct.HistoDir;
    reconDir = YamlStruct.RegisteredBlocks;

else
    histoDir = uigetdir('','Please select the histology directory:');
    reconDir = uigetdir('','Please select the blockface directory:');
end


% Experiment parameters
if exist('run_options.mat')
    load('run_options.mat')
else
    blockName = inputdlg('Please provide block name, including case name (e.g., P57-16_A1.2)');
    blockName = blockName{1};
    stain = questdlg('Is this LFB or H&E?','Stain','LFB','HE','LFB');
end
disp(['Processing block ' blockName])

% data directories and files
histoBlockDir=[histoDir filesep blockName filesep stain];

load([histoBlockDir filesep blockName '_' stain '_mapping.mat'],'mapping');
BFmriName=[reconDir filesep blockName '_volume.gray.reg.mgz'];
BFmriMaskName=[reconDir filesep blockName '_volume.mask.reg.mgz'];
BFrotName=[reconDir filesep blockName '_volume.mri.rot.mat'];

% We use the middle section to ask the user for the right rotation
mid=round(length(mapping)/2);

if ~exist(BFrotName,'file')

    % input data for histo recon code
    disp('Reading in MRI and histo data');

    disp('    MRI ... ');
    Imri=myMRIread(BFmriName);
    Mmri=myMRIread(BFmriMaskName);
    Msoft = Mmri.vol / max(Mmri.vol(:));
    Mhard = Msoft > 0.5;
    % If we run out of mask for whatever reason, we copy from previous slices
    % Not the pretties, but it works  TODO: improve?
    aux=squeeze(sum(sum(Msoft,1),2));
    ok = find(aux>=5000);
    notok = find(aux<5000);
    for j = 1 : length(notok)
        f=notok(j);
        dist=abs(ok-f);
        [~,idx]=min(dist);
        ref=ok(idx);
        Msoft(:,:,f)=Msoft(:,:,ref);
    end

    % Now we stretch histogram and (soft) mask
    aux=Imri.vol(Mhard);
    mini=quantile(aux,0.001);
    maxi=quantile(aux,0.999);
    I = Msoft .* (255 * (Imri.vol - mini) / (maxi-mini));
    I = uint8(I);

    % MRI
    if strcmp(blockName(end-3:end), 'P9.1') || strcmp(blockName(end-3:end), 'C4.1')
        mapping = size(I,3) - mapping; 
    end
    Is=I(:,:,mapping(mid));

    % Histo
    H = imread([histoBlockDir filesep blockName '_' stain '_' num2str(mid,'%.2d') '.jpg']);
    Hresized = imresize(H,Hres/BFres);
    M = imread([histoBlockDir filesep blockName '_' stain '_' num2str(mid,'%.2d') '.mask.png']);
    Mresized = imresize(M,[size(Hresized,1) size(Hresized,2)]);

    LRflips=[0 1];
    rots=[0 90 180 270];
    disp('Please, look the figures')
    close all
    figure(1), imshow(Hresized); 
    figure(2),
    axes=zeros(1,8);
    for f=1:length(LRflips)
        for r=1:length(rots)  
            subplot(2,4,(f-1)*4+r);
            if LRflips(f)
                aux=imrotate(fliplr(Is),rots(r));
            else
                aux=imrotate(Is,rots(r));
            end
            aux(1,1)=(f-1)*4+r;
            imshow(aux);
        end
    end
    uiwait(msgbox('Please click on subfigure of Fig 2 with rotation/flip that matches Fig 1'));
    figure(2); ginput(1);
    aux=get(get(gca,'Children'),'Cdata');
    idx=aux(1,1);
    close all

    LRflip=LRflips(1+double(idx>4));
    if idx<=4, rot=rots(idx); else, rot=rots(idx-4); end
    save(BFrotName,'LRflip','rot');
end

disp('All done!');