Histology reconstruction with refinements of nonlinear registration
Juan Eugenio Iglesias
May 31st 2018
Based on MICCAI submission: 
"Model-based refinement of nonlinear registrations in 3D histology reconstruction"

*************************

The main entry point of the code is HistoMRIrecon. This function receives five arguments:

1. MRIlistFile
This is just a text file with a list of file names, listing the slices of the 
reference MRI scan. Please note that all images must have the same size in pixels.
An example of what this file looks like is: exampleMRIlist.txt

2. HISTOlistFile
This is another text file, very similar to the previous one, but with the 
corresponding histological sections. Please note that all histological sections
do *NOT* need to have the same size in pixels. However, we recommend that the 
pixel size (in mm) is not too different from that of the MRI data; otherwise,
the intermodality registration might fail. An example of what this file looks 
like is: exampleHISTOlist.txt

3. NiftyRegConfigFile: text file with location of NiftyReg, registration
parameters, etc. The format of this file is the following:
- First line: location of NiftyReg installation. This directory is expected to
   contain a subdirectory named "reg-apps", which in turn contains the registration
   executables: reg_aladin, reg_f3d, etc.
- Second line: parameters of affine registration (reg_aladin). See the help of 
   reg_aladin for more information.
- Third line: parameters of nonlinear registration (reg_f3d) for histo-MRI 
   registrations. See help of reg_f3d for more information.
- Fourth line: parameters of nonlinear registration, for intramodality registration.
You can find an example of this configuration file in this directory: 
exampleNiftyConfig.txt.

4. outputFileNameBase: prefix for the output. The code will write files that
look like this: 
  [base]_section_0001.png (deformed image) 
  [base]_section_0001.noFiltering.png (output of direct registration, without filtering) 
  [base]_section_0001.mat (mat file with nonlinear deformation fiedld: 'FIELD')
Once the code is done running, you can load [base]_section_000.mat, and use the provided
function deform2D (in directory "functions") to apply the deformation to, for example, 
a segmentation of the histological section.

5. NeighborNumberIntra: number of registered neighbors intramodality. A good value to 
start is 2 (see paper).


NOTE THAT THE CODE REQUIRES AN INSTALLATION OF NIFTYREG: 

http://sourceforge.net/projects/niftyreg/




*************************

So, you could run the example with the provided data as follows:
(note that the "histology" is actually a synthetically deformed T2 volume)

mkdir('output');
MRIlistFile='./exampleMRIlist.txt';
HISTOlistFile='./exampleHISTOlist.txt';
NiftiRegConfigFile='./exampleNiftyConfig.txt';
outputFileNameBase='./output/test';
NeighborNumberIntra=2;

HistoMRIrecon(MRIlistFile,HISTOlistFile,NiftiRegConfigFile,...
    outputFileNameBase,NeighborNumberIntra);

*************************

Current limitations of the code

- Because of the I/O functions for Nifti data, the code only supports Linux 
  and MAC at this point.

- The pixel dimensions need to be similar for both modalities. We could solve
  this moving from standard image formats to nifti

- We are working with full velocity fields, which could be avoided in NiftyReg:
  since the velocity field at any location is a linear combination of the field
  at the control points, we could apply our technique to the deformations at the 
  control points. On the other hand, the implementation we have right now might
  be easier to adapt to other methods (DARTEL? LogDemons?), which do not assume
  that the velocity field is parameterized by a grid of control points.



*************************
UPDATE: January, 2020 

  1.- We have included a modification of the initial algorithm, which uses a modified 
      Spanning T(h)ree by including both LFB and HE stains in the optimization.
