% Reads a list of files from a txt file, and gives an error
% if any of the files doesn't exist
function fileList=readFileList(filename)

fileList=[];
fid=fopen(filename);
while 1
    fname = fgetl(fid);
    if ~ischar(fname), break, end
    if exist(fname,'file')
        fileList{end+1}=fname;
    else
        error(['Error, file does not exist: ' fname]);
    end
end
fclose(fid);