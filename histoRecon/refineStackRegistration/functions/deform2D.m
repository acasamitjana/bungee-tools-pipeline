% DEFORMED = deform2D(IM,FIELD,mode) 
% Applies a deformation field
% IM: image to deform, of size M x N
% field: field to apply, of size I x J x 2
% mode: can be any of the model from interp2: 'nearest', 'linear', 'spline', or 'cubic'
% DEFORMED: the output, of size I x J
function DEF = deform2D(IM,FIELD,mode,padval)

if nargin<3
    mode='linear';
end
if nargin<4
    padval=0;
end

Dx=FIELD(:,:,1);
Dy=FIELD(:,:,2);


DEF=zeros([size(FIELD,1),size(FIELD,2),size(IM,3)]);
[XX,YY]=ndgrid(1:size(FIELD,1),1:size(FIELD,2));
XXd=XX+Dx;
YYd=YY+Dy;
for c=1:size(IM,3)
    aux=zeros([size(DEF,1),size(DEF,2)]);
    aux(:)=interp2(double(IM(:,:,c)),YYd(:),XXd(:),mode,padval);
    DEF(:,:,c)=aux;
end

    

