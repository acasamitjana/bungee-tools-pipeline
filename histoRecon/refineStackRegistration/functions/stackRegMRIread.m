% This is an adaptation of MRIread from FreeSurfer
function MRI = stackRegMRIread(filename,justheader,tempdir)

if(exist('justheader','var')~=1) || isempty(justheader), justheader = 0; end
if(exist('tempdir','var')~=1) || isempty(tempdir), tempdir = '/tmp/'; end
if tempdir(end)~='/', tempdir=[tempdir '/']; end
if exist(tempdir,'dir')==0
    error('Error in myMRIread: temporary directory does not exist')
end

cl=clock();
p1=num2str(round(1e6*cl(end)));
[~,aux]=fileparts(filename);
aux(aux=='.')='_';
p2=aux;
p3=getenv('PBS_JOBID');
cl=clock();
rng(round(1e6*cl(end))+sum(double(filename)),'twister');
p4=num2str(round(1000000*rand(1)));
fname2 = [tempdir p1 '_' p2 '_' p3 '_' p4 '.nii'];
while exist(fname2,'file')>0
    pause(0.1*rand(1));
    cl=clock();
    p1=num2str(round(1e6*cl(end)));
    cl=clock();
    rng(round(1e6*cl(end))+sum(double(filename)));
    p4=num2str(round(1000000*rand(1)));
    fname2 = [tempdir p1 '_' p2 '_' p3 '_' p4 '.nii'];
end
system(['gunzip -c ' filename ' > ' fname2]);
MRI=myMRIread_aux(fname2,justheader);
delete(fname2);





function mri = myMRIread_aux(fstring,justheader)


mri = [];

[fspec fstem fmt] = MRIfspec(fstring);
if(isempty(fspec))
    err = sprintf('ERROR: cannot determine format of %s (%s)\n',fstring,mfilename);
    error(err);
    return;
end

mri.srcbext = '';    % empty be default
mri.analyzehdr = []; % empty be default
mri.bhdr = []; % empty be default


switch(fmt)
   
        %------- nifti nii -------------------------------------
    case {'nii'}
        hdr = load_nifti(fspec,justheader);
        if(isempty(hdr))
            fprintf('ERROR: loading %s as analyze\n',fspec);
            mri = [];
            return;
        end
        volsz = hdr.dim(2:end);
        indnz = find(volsz~=0);
        volsz = volsz(indnz);
        volsz = volsz(:)'; % just make sure it's a row vect
        if(~justheader) mri.vol = permute(hdr.vol,[2 1 3 4 5]);
        else            mri.vol = [];
        end
        volsz([1 2]) = volsz([2 1]); % Make consistent. No effect when rows=cols
        tr = hdr.pixdim(5); % already msec
        flip_angle = 0;
        te = 0;
        ti = 0;
        hdr.vol = []; % already have it above, so clear it
        M = hdr.vox2ras;
        mri.niftihdr = hdr;
        %---------------------------------------------------
    otherwise
        fprintf('ERROR: format %s not supported\n',fmt);
        mri = [];
        return;
end
%--------------------------------------%

mri.fspec = fspec;
mri.pwd = pwd;

mri.flip_angle = flip_angle;
mri.tr  = tr;
mri.te  = te;
mri.ti  = ti;

% Assumes indices are 0-based. See vox2ras1 below for 1-based.  Note:
% MRIwrite() derives all geometry information (ie, direction cosines,
% voxel resolution, and P0 from vox2ras0. If you change other geometry
% elements of the structure, it will not be reflected in the output
% volume. Also note that vox2ras still maps Col-Row-Slice and not
% Row-Col-Slice.  Make sure to take this into account when indexing
% into matlab volumes (which are RCS).
mri.vox2ras0 = M;

% Dimensions not redundant when using header only
volsz(length(volsz)+1:4) = 1; % Make sure all dims are represented
mri.volsize = volsz(1:3); % only spatial components
mri.height  = volsz(1);   % Note that height (rows) is the first dimension
mri.width   = volsz(2);   % Note that width (cols) is the second dimension
mri.depth   = volsz(3);
mri.nframes = volsz(4);

%--------------------------------------------------------------------%
% Everything below is redundant in that they can be derivied from
% stuff above, but they are in the MRI struct defined in mri.h, so I
% thought I would add them here for completeness.  Note: MRIwrite()
% derives all geometry information (ie, direction cosines, voxel
% resolution, and P0 from vox2ras0. If you change other geometry
% elements below, it will not be reflected in the output volume.

mri.vox2ras = mri.vox2ras0;

mri.nvoxels = mri.height * mri.width * mri.depth; % number of spatial voxles
mri.xsize = sqrt(sum(M(:,1).^2)); % Col
mri.ysize = sqrt(sum(M(:,2).^2)); % Row
mri.zsize = sqrt(sum(M(:,3).^2)); % Slice

mri.x_r = M(1,1)/mri.xsize; % Col
mri.x_a = M(2,1)/mri.xsize;
mri.x_s = M(3,1)/mri.xsize;

mri.y_r = M(1,2)/mri.ysize; % Row
mri.y_a = M(2,2)/mri.ysize;
mri.y_s = M(3,2)/mri.ysize;

mri.z_r = M(1,3)/mri.zsize; % Slice
mri.z_a = M(2,3)/mri.zsize;
mri.z_s = M(3,3)/mri.zsize;

ic = [(mri.width)/2 (mri.height)/2 (mri.depth)/2 1]';
c = M*ic;
mri.c_r = c(1);
mri.c_a = c(2);
mri.c_s = c(3);
%--------------------------------------------------%

%-------- The stuff here is for convenience --------------

% 1-based vox2ras. Good for doing transforms in matlab
mri.vox2ras1 = vox2ras_0to1(M);

% Matrix of direction cosines
mri.Mdc = [M(1:3,1)/mri.xsize M(1:3,2)/mri.ysize M(1:3,3)/mri.zsize];

% Vector of voxel resolutions (Row-Col-Slice)
mri.volres = [mri.xsize mri.ysize mri.zsize];

% Have to swap rows and columns back
voldim = [mri.volsize(2) mri.volsize(1) mri.volsize(3)]; %[ncols nrows nslices]
volres = [mri.volres(2)  mri.volres(1)  mri.volres(3)];  %[dcol drow dslice]
mri.tkrvox2ras = vox2ras_tkreg(voldim,volres);


return;


 

function hdr = load_nifti(niftifile,hdronly)

hdr = [];

if(nargin < 1 | nargin > 2)
    fprintf('hdr = load_nifti(niftifile,<hdronly>)\n');
    return;
end

if(~exist('hdronly','var')) hdronly = []; end
if(isempty(hdronly)) hdronly = 0; end

hdr = load_nifti_hdr(niftifile);
if(isempty(hdr))
    return;
end

% Check for ico7
nspatial = prod(hdr.dim(2:4));
IsIco7 = 0;
if(nspatial == 163842) IsIco7 = 1; end

% If only header is desired, return now
if(hdronly)
    if(IsIco7)
        % Reshape
        hdr.dim(2) = 163842;
        hdr.dim(3) = 1;
        hdr.dim(4) = 1;
    end
    return;
end

% Get total number of voxels
dim = hdr.dim(2:end);
ind0 = find(dim==0);
dim(ind0) = 1;
nvoxels = prod(dim);

% Open to read the pixel data
fp = fopen(niftifile,'r',hdr.endian);

% Get past the header
fseek(fp,round(hdr.vox_offset),'bof');

switch(hdr.datatype)
    % Note: 'char' seems to work upto matlab 7.1, but 'uchar' needed
    % for 7.2 and higher.
    case   2, [hdr.vol nitemsread] = fread(fp,inf,'uchar');
    case   4, [hdr.vol nitemsread] = fread(fp,inf,'short');
    case   8, [hdr.vol nitemsread] = fread(fp,inf,'int');
    case  16, [hdr.vol nitemsread] = fread(fp,inf,'float');
    case  64, [hdr.vol nitemsread] = fread(fp,inf,'double');
    case 512, [hdr.vol nitemsread] = fread(fp,inf,'ushort');
    case 768, [hdr.vol nitemsread] = fread(fp,inf,'uint');
    otherwise,
        fprintf('ERROR: data type %d not supported',hdr.datatype);
        hdr = [];
        return;
end

fclose(fp);


% Check that that many voxels were read in
if(nitemsread ~= nvoxels)
    fprintf('ERROR: %s, read in %d voxels, expected %d\n',...
        niftifile,nitemsread,nvoxels);
    hdr = [];
    return;
end

if(IsIco7)
    %fprintf('load_nifti: ico7 reshaping\n');
    hdr.dim(2) = 163842;
    hdr.dim(3) = 1;
    hdr.dim(4) = 1;
    dim = hdr.dim(2:end);
end

hdr.vol = reshape(hdr.vol, dim');
if(hdr.scl_slope ~= 0)
    % fprintf('Rescaling NIFTI: slope = %g, intercept = %g\n',...
    % hdr.scl_slope,hdr.scl_inter);
    %fprintf('    Good luck, this has never been tested ... \n');
    hdr.vol = hdr.vol * hdr.scl_slope  + hdr.scl_inter;
end

return;








function [fspec, fstem, fmt] = MRIfspec(fstring,checkdisk)


fspec = [];
fstem = [];
fmt   = [];

if(nargin < 1 | nargin > 2)
  fprintf('[fspec fstem fmt] = MRIfspec(fstring,<checkdisk>)\n');
  return;
end

% If checkdisk is not passed, then do a check disk
if(~exist('checkdisk','var')) checkdisk = 1; end

inddot = max(findstr(fstring,'.'));
if(isempty(inddot))
  ext = ' ';
else
  ext = fstring(inddot+1:end);
end
    
switch(ext)
 case 'nii',
  fspec = fstring;
  fmt = 'nii';
  fstem = fstring(1:end-4);
  return;
 case 'gz',
  ind = findstr(fstring,'nii.gz');
  if(~isempty(ind))
    fspec = fstring;
    fmt = 'nii.gz';
    fstem = fstring(1:ind-2);
    return;
  end
end

% If it gets here, then it cannot determine the format from an
% extension, so fstring could be a stem, so see if a file with
% stem.fmt exists. Order is imporant here.

% Do this only if checkdisk is on
if(~checkdisk) return; end

fstem = fstring;

fmt = 'nii';
fspec = sprintf('%s.%s',fstring,fmt);
if(fast_fileexists(fspec)) return; end

fmt = 'nii.gz';
fspec = sprintf('%s.%s',fstring,fmt);
if(fast_fileexists(fspec)) return; end

% If it gets here, then could not determine format
fstem = [];
fmt = [];
fspec = [];

return;





function hdr = load_nifti_hdr(niftifile)

hdr = [];

if(nargin ~= 1)
  fprintf('hdr = load_nifti_hdr(niftifile)\n');
  return;
end

% Try opening as big endian first
fp = fopen(niftifile,'r','b');
if(fp == -1) 
  fprintf('ERROR: could not read %s\n',niftifile);
  return;
end

hdr.sizeof_hdr  = fread(fp,1,'int');
if(hdr.sizeof_hdr ~= 348)
  fclose(fp);
  % Now try opening as little endian
  fp = fopen(niftifile,'r','l');
  hdr.sizeof_hdr  = fread(fp,1,'int');
  if(hdr.sizeof_hdr ~= 348)
    fclose(fp);
    fprintf('ERROR: %s: hdr size = %d, should be 348\n',...
	    niftifile,hdr.sizeof_hdr);
    hdr = [];
    return;
  end
  hdr.endian = 'l';
else
  hdr.endian = 'b';
end

hdr.data_type       = fscanf(fp,'%c',10);
hdr.db_name         = fscanf(fp,'%c',18);
hdr.extents         = fread(fp, 1,'int');
hdr.session_error   = fread(fp, 1,'short');
hdr.regular         = fread(fp, 1,'char');
hdr.dim_info        = fread(fp, 1,'char');
hdr.dim             = fread(fp, 8,'short');
hdr.intent_p1       = fread(fp, 1,'float');
hdr.intent_p2       = fread(fp, 1,'float');
hdr.intent_p3       = fread(fp, 1,'float');
hdr.intent_code     = fread(fp, 1,'short');
hdr.datatype        = fread(fp, 1,'short');
hdr.bitpix          = fread(fp, 1,'short');
hdr.slice_start     = fread(fp, 1,'short');
hdr.pixdim          = fread(fp, 8,'float'); % physical units
hdr.vox_offset      = fread(fp, 1,'float');
hdr.scl_slope       = fread(fp, 1,'float');
hdr.scl_inter       = fread(fp, 1,'float');
hdr.slice_end       = fread(fp, 1,'short');
hdr.slice_code      = fread(fp, 1,'char');
hdr.xyzt_units      = fread(fp, 1,'char');
hdr.cal_max         = fread(fp, 1,'float');
hdr.cal_min         = fread(fp, 1,'float');
hdr.slice_duration  = fread(fp, 1,'float');
hdr.toffset         = fread(fp, 1,'float');
hdr.glmax           = fread(fp, 1,'int');
hdr.glmin           = fread(fp, 1,'int');
hdr.descrip         = fscanf(fp,'%c',80);
hdr.aux_file        = fscanf(fp,'%c',24);
hdr.qform_code      = fread(fp, 1,'short');
hdr.sform_code      = fread(fp, 1,'short');
hdr.quatern_b       = fread(fp, 1,'float');
hdr.quatern_c       = fread(fp, 1,'float');
hdr.quatern_d       = fread(fp, 1,'float');
hdr.quatern_x       = fread(fp, 1,'float');
hdr.quatern_y       = fread(fp, 1,'float');
hdr.quatern_z       = fread(fp, 1,'float');
hdr.srow_x          = fread(fp, 4,'float');
hdr.srow_y          = fread(fp, 4,'float');
hdr.srow_z          = fread(fp, 4,'float');
hdr.intent_name     = fscanf(fp,'%c',16);
hdr.magic           = fscanf(fp,'%c',4);

fclose(fp);

% This is to accomodate structures with more than 32k cols
% FreeSurfer specific. See also mriio.c.
if(hdr.dim(2) < 0) 
  hdr.dim(2) = hdr.glmin; 
  hdr.glmin = 0; 
end

% look at xyz units and convert to mm if needed
xyzunits = bitand(hdr.xyzt_units,7); % 0x7
switch(xyzunits)
 case 1, xyzscale = 1000.000; % meters
 case 2, xyzscale =    1.000; % mm
 case 3, xyzscale =     .001; % microns
 otherwise, 
  % fprintf('WARNING: xyz units code %d is unrecognized\n',xyzunits);
  xyzscale = 10^10; % Make it silly
end
hdr.pixdim(2:4) = hdr.pixdim(2:4) * xyzscale;
hdr.srow_x = hdr.srow_x * xyzscale;
hdr.srow_y = hdr.srow_y * xyzscale;
hdr.srow_z = hdr.srow_z * xyzscale;

% look at time units and convert to msec if needed
tunits = bitand(hdr.xyzt_units,3*16+8); % 0x38 
switch(tunits)
 case  8, tscale = 1000.000; % seconds
 case 16, tscale =    1.000; % msec
 case 32, tscale =     .001; % microsec
 otherwise,  tscale = 0; 
end
hdr.pixdim(5) = hdr.pixdim(5) * tscale;

% Change value in xyzt_units to reflect scale change
hdr.xyzt_units = bitor(2,16); % 2=mm, 16=msec

% Sform matrix
hdr.sform =  [hdr.srow_x'; 
	      hdr.srow_y'; 
	      hdr.srow_z';
	      0 0 0 1];

% Qform matrix - not quite sure how all this works,
% mainly just copied CH's code from mriio.c
b = hdr.quatern_b;
c = hdr.quatern_c;
d = hdr.quatern_d;
x = hdr.quatern_x;
y = hdr.quatern_y;
z = hdr.quatern_z;
a = 1.0 - (b*b + c*c + d*d);
if(abs(a) < 1.0e-7)
  a = 1.0 / sqrt(b*b + c*c + d*d);
  b = b*a;
  c = c*a;
  d = d*a;
  a = 0.0;
else
  a = sqrt(a);
end
r11 = a*a + b*b - c*c - d*d;
r12 = 2.0*b*c - 2.0*a*d;
r13 = 2.0*b*d + 2.0*a*c;
r21 = 2.0*b*c + 2.0*a*d;
r22 = a*a + c*c - b*b - d*d;
r23 = 2.0*c*d - 2.0*a*b;
r31 = 2.0*b*d - 2*a*c;
r32 = 2.0*c*d + 2*a*b;
r33 = a*a + d*d - c*c - b*b;
if(hdr.pixdim(1) < 0.0)
  r13 = -r13;
  r23 = -r23;
  r33 = -r33;
end
qMdc = [r11 r12 r13; r21 r22 r23; r31 r32 r33];
D = diag(hdr.pixdim(2:4));
P0 = [x y z]';
hdr.qform = [qMdc*D P0; 0 0 0 1];

if(hdr.sform_code ~= 0)
  % Use sform first
  hdr.vox2ras = hdr.sform;
elseif(hdr.qform_code ~= 0)
  % Then use qform first
  hdr.vox2ras = hdr.qform;
else
  %fprintf('WARNING: neither sform or qform are valid in %s\n', ...
%	  niftifile);
  D = diag(hdr.pixdim(2:4));
  P0 = [0 0 0]';
  hdr.vox2ras = [eye(3)*D P0; 0 0 0 1];
end

return;




function M1 = vox2ras_0to1(M0)

M1 = [];

if(nargin ~= 1)
  fprintf('M1 = vox2ras_0to1(M0)\n');
  return;
end

Q = zeros(4);
Q(1:3,4) = ones(3,1);

M1 = inv(inv(M0)+Q);

return;





function T = vox2ras_tkreg(voldim, voxres)


T = [];

if(nargin ~= 2)
  fprintf('T = vox2ras_tkreg(voldim, voxres)\n');
  return;
end

T = zeros(4,4);
T(4,4) = 1;

T(1,1) = -voxres(1);
T(1,4) = voxres(1)*voldim(1)/2;

T(2,3) = voxres(3);
T(2,4) = -voxres(3)*voldim(3)/2;


T(3,2) = -voxres(2);
T(3,4) = voxres(2)*voldim(2)/2;


return;










