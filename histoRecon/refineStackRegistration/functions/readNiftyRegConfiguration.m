% Reads in the location where NiftyReg is installed, and also the
% parameters for linear and nonlinear registration
function [ALADINcmd,F3Dcmd,TRANSFORMcd,ALADINparams,F3DparamsInter,F3DparamsIntra] =...
    readNiftyRegConfiguration(NiftyRegConfigFile)

fid=fopen(NiftyRegConfigFile);

NiftyRegPath=fgetl(fid);
ALADINparams=fgetl(fid);
F3DparamsInter=fgetl(fid);
F3DparamsIntra=fgetl(fid);

fclose(fid);

ALADINcmd=[NiftyRegPath filesep 'reg-apps' filesep 'reg_aladin '];
F3Dcmd=[NiftyRegPath filesep 'reg-apps' filesep 'reg_f3d '];
TRANSFORMcd=[NiftyRegPath filesep 'reg-apps' filesep 'reg_transform '];



