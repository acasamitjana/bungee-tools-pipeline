function [cost,grad]=computeCostAndGradSigmas(vars,npix,E,D,DELTA)

grad=zeros(2,1);
cost=0;
for k=1:length(D)
    
    varsK=D(k)*vars(1)+DELTA(k)*vars(2);
    
    cost = cost + npix*log(2*pi*varsK) + 0.5*E(k)/varsK;
    
    aux=npix/varsK-0.5*E(k)/varsK/varsK;
    grad(1)=grad(1)+aux*D(k);
    grad(2)=grad(2)+aux*DELTA(k);
    
end
cost=double(cost/npix/length(D));
grad=double(grad/npix/length(D));