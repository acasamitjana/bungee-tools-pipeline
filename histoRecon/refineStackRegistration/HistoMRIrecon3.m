% HistoMRIrecon3(MRIlistFile,LFBlistFile,HEListFile,...
%     NiftiRegConfigFile, outputFileNameBase,NeighborNumberIntra, use_L1)
%
% MRIlistFile: text file with list of MRI images
%
% HISTOlistFile: text file with corresponding histological images, in same order
%
% NiftiRegConfigFile: text file with location of NiftiReg, registration
% parameters, etc (see README.txt for details)
%
% outputFileNameBase: prefix for the output. The code will write files that
% look like:
% XXX_section_0001.png (deformed image)
% XXX_section_0001.noFiltering.png (deformed image without filtering, i.e., direct registration)
% XXX_section_0001.mat (mat file with nonlinear deformation FIELD, which can be applied with deform2D)
%
% NeighborNumberIntra: number of registered neighbors intramodality. Two is
% a good value for this (see paper).
%
function HistoMRIrecon3(MRIlistFile ,LFBlistFile, HElistFile, ...
    NiftiRegConfigFile, outputFileNameBase,NeighborNumberIntra, use_L1)

% addpath ./functions
factor = 5; 

% temporary directory
tempdir=[outputFileNameBase '_temp' filesep];
% TODELETE
if exist(tempdir,'dir')==0, mkdir(tempdir); end
% if exist(tempdir,'dir'), rmdir(tempdir,'s'); end
% mkdir(tempdir);

% read in lists of files
MRIlist=readFileList(MRIlistFile);
LFBlist=readFileList(LFBlistFile);
HElist=readFileList(HElistFile);
if length(MRIlist)~=length(LFBlist) || length(MRIlist)~=length(HElist) ...
        || length(HElist)~=length(LFBlist)
    error('Error: different number of files in MRI and histology');
end
Nims=length(MRIlist);

% Read in configuration of NiftiReg / registration commands
[ALADINcmd,F3Dcmd,TRANSFORMcmd,ALADINparams,F3DparamsInter,F3DparamsIntra] =...
    readNiftyRegConfiguration(NiftiRegConfigFile);

% Input images
disp('Reading in images');

LFBimage=cell([1,Nims]);
HEimage=cell([1,Nims]);
for z=1:Nims
    aux=imread(MRIlist{z});
    if size(aux,3)>1, aux=rgb2gray(aux);  end
    if z==1
        sizeMRI=size(aux);
        MRIimage=zeros([sizeMRI Nims],'uint8');
    end
    MRIimage(:,:,z)=aux;
    
    aux=imread(LFBlist{z});
    if size(aux,3)>1, aux=rgb2gray(aux);  end
    LFBimage{z}=aux;
    
    aux=imread(HElist{z});
    if size(aux,3)>1, aux=rgb2gray(aux);  end
    HEimage{z}=aux;
end


% Registration from histo to mri
disp('Computing inter-modality registrations');
for z=1:Nims
    
    disp(['  Slice ' num2str(z) ' of ' num2str(Nims)]);
    
    aux=double(MRIimage(:,:,z));
    aux=aux-min(aux(:));
    aux=aux/max(aux(:));
    aux=aux*255;
    aux=uint8(aux);
    imwrite(aux,[tempdir filesep 'image_' num2str(z,'%.4d') '.MRI.png']);

    aux=double(LFBimage{z});
    aux=aux-min(aux(:));
    aux=aux/max(aux(:));
    aux=aux*255;
    aux=uint8(aux);
    imwrite(aux,[tempdir filesep 'image_' num2str(z,'%.4d') '.LFB.png']);
    
    aux=double(HEimage{z});
    aux=aux-min(aux(:));
    aux=aux/max(aux(:));
    aux=aux*255;
    aux=uint8(aux);
    imwrite(aux,[tempdir filesep 'image_' num2str(z,'%.4d') '.HE.png']);
        
    for modalityIndex=1:3
        
        if modalityIndex ==1
            modality_flo = 'LFB';
            modality_ref = 'MRI';
            floFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.LFB.png'];
            refFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.MRI.png'];
        
        elseif modalityIndex==2
            modality_flo = 'HE';
            modality_ref = 'MRI';
            floFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.HE.png'];
            refFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.MRI.png'];
        
        else
            modality_flo = 'HE';
            modality_ref = 'LFB';
            floFile=[tempdir filesep 'reg_' num2str(z,'%.4d') '.HE_to_MRI.lin.png'];
            refFile=[tempdir filesep 'reg_' num2str(z,'%.4d') '.LFB_to_MRI.lin.png'];
        
        end
    
        affineFile=[tempdir filesep 'def_' num2str(z,'%.4d')  '.' modality_flo '_to_' modality_ref '.aff'];
        nonlinearFile=[tempdir filesep 'def_' num2str(z,'%.4d') '.' modality_flo '_to_' modality_ref '.nii.gz'];
        outputFileLin=[tempdir filesep 'reg_' num2str(z,'%.4d') '.' modality_flo '_to_' modality_ref '.lin.png'];
        outputFileNonLin=[tempdir filesep 'reg_' num2str(z,'%.4d') '.' modality_flo '_to_' modality_ref '.nonlin.png'];
        
        dummyFileNifti=[tempdir filesep 'dummyNifti.nii.gz'];

        cmd1=[ALADINcmd ' -ref ' refFile ' -flo ' floFile ' -aff ' affineFile ...
            ' -res ' outputFileLin ' ' ALADINparams];

        cmd2=[F3Dcmd  ' -ref ' refFile ' -flo ' outputFileLin ...
            ' -res ' outputFileNonLin ' -cpp ' dummyFileNifti ' ' F3DparamsInter ' -vel '];

        cmd3=[TRANSFORMcmd  ' -ref ' refFile ' -flow ' dummyFileNifti ' ' nonlinearFile];


        system([cmd1 ' >/dev/null']);
        system([cmd2 ' >/dev/null']);
        system([cmd3 ' >/dev/null']);

        delete(dummyFileNifti);
    
    end
    
end


% Registration from histo to histo and from mri to mri
disp('Computing intra-modality registrations');
for z=1:Nims
    
    disp(['  Slice ' num2str(z) ' of ' num2str(Nims)]);
    
    % Register to NeighborNumberIntra neighbors (mri and histo)
    for zz=z+1:min(Nims,z+NeighborNumberIntra)
        
        for modalityIndex=1:3
            
            if modalityIndex==1
                modality='MRI';
                floFile=[tempdir filesep 'image_' num2str(zz,'%.4d') '.MRI.png'];
                refFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.MRI.png'];
            elseif modalityIndex==2
                modality='LFB';
                floFile=[tempdir filesep 'reg_' num2str(zz,'%.4d') '.LFB_to_MRI.lin.png'];
                refFile=[tempdir filesep 'reg_' num2str(z,'%.4d') '.LFB_to_MRI.lin.png'];
            else
                modality='HE';
                floFile=[tempdir filesep 'reg_' num2str(zz,'%.4d') '.HE_to_MRI.lin.png'];
                refFile=[tempdir filesep 'reg_' num2str(z,'%.4d') '.HE_to_MRI.lin.png'];
            end
            
            nonlinearFile=[tempdir filesep 'def_' num2str(zz,'%.4d') '_to_'  num2str(z,'%.4d') '.' modality '.nii.gz'];
            outputFile=[tempdir filesep 'reg_' num2str(zz,'%.4d') '_to_'  num2str(z,'%.4d') '.' modality '.png'];
            
            dummyFileNifti=[tempdir filesep 'dummyNifti.nii.gz'];
            
            cmd1=[F3Dcmd  ' -ref ' refFile ' -flo ' floFile ...
                ' -res ' outputFile ' -cpp ' dummyFileNifti ' ' F3DparamsIntra ' -vel '];
            
            cmd2=[TRANSFORMcmd  ' -ref ' refFile ' -flow ' dummyFileNifti ' ' nonlinearFile];
            
            system([cmd1 ' >/dev/null']);
            system([cmd2 ' >/dev/null']);
            
            delete(dummyFileNifti);
            
            
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is the main filtering part %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This chunk of code:
% - builds matrix with registration paths (W in the paper)
%     W structure: rows: for each image:
%                        intermodal MRI-LFB,MRI-HE,LFB-HE, intramodal MRI,LFB,HE
%                  columns: 1:Nims (MRIref --> LFBflo) 
%                           Nims:2*Nims (MRIref --> LFBflo) 
%                           2*Nims:3*Nims-1 (MRIref --> MRI_(+1)ref)

% - builds vector with separations A,B,C (d_k in the paper) accounting for
%   intramodality variance by counting the number of slices that it 
%   traverses: MRI, LFB, HE respectively.
% 
% - builds indicator function A_DELTA, B_DELTA, C_DELTA (c_k in the paper)
%   accounting for transformations across modalities: MRI-LFB, MRI-HE,
%   LFB-HE respectively.
% 
% - and all while reading in the SVFs
disp('Reading in SVFs');

W=zeros(Nims*(3+3*NeighborNumberIntra),3*Nims-1,'single');
D_MRI=zeros(1,Nims*(3+3*NeighborNumberIntra),'single');
D_LFB=zeros(1,Nims*(3+3*NeighborNumberIntra),'single');
D_HE=zeros(1,Nims*(3+3*NeighborNumberIntra),'single');
DELTA=zeros(1,Nims*(3+3*NeighborNumberIntra),'single');


NK=0;
svf=zeros([sizeMRI 2]);
[XX,YY]=ndgrid(1:sizeMRI(1),1:sizeMRI(2));
PHI=zeros([sizeMRI 2 Nims*(1+2*NeighborNumberIntra)]);

for z=1:Nims
    
    disp(['  Section ' num2str(z) ' of ' num2str(Nims)]);
    
    % Intermodality
    for modalityIndex=1:2
        
        NK=NK+1;   
        if modalityIndex ==1
            modality_flo = 'LFB';
            modality_ref = 'MRI';
            znum=z;
            
        elseif modalityIndex==2
            modality_flo = 'HE';
            modality_ref = 'MRI';
            znum=z+Nims;
            
        end
        
             
        mri=stackRegMRIread([tempdir filesep 'def_' num2str(z,'%.4d') ...
            '.' modality_flo '_to_' modality_ref '.nii.gz']);
        mri.vol(isnan(mri.vol))=0;
        svf(:,:,1)=mri.vol(:,:,:,:,2)-XX+1;
        svf(:,:,2)=mri.vol(:,:,:,:,1)-YY+1;
        PHI(:,:,:,NK)=single(svf);
        W(NK,znum)=1;
        DELTA(NK)= 1;

        
    end
    
    
    NK=NK+1;
    mri=stackRegMRIread([tempdir filesep 'def_' num2str(z,'%.4d') ...
        '.HE_to_LFB.nii.gz']);
    mri.vol(isnan(mri.vol))=0;
    svf(:,:,1)=mri.vol(:,:,:,:,2)-XX+1;
    svf(:,:,2)=mri.vol(:,:,:,:,1)-YY+1;
    PHI(:,:,:,NK)=single(svf);
    W(NK,Nims+z)=1;
    W(NK,z)=-1;
    DELTA(NK)= 1;


    
    % Intramodality
    for zz=z+1:min(Nims,z+NeighborNumberIntra)
        
        % MRI
        NK=NK+1;
        mri=stackRegMRIread([tempdir filesep 'def_' num2str(zz,'%.4d') ...
            '_to_' num2str(z,'%.4d') '.MRI.nii.gz']);
        mri.vol(isnan(mri.vol))=0;
        svf(:,:,1)=mri.vol(:,:,:,:,2)-XX+1;
        svf(:,:,2)=mri.vol(:,:,:,:,1)-YY+1;
        PHI(:,:,:,NK)=single(svf);

        W(NK,2*Nims+z:2*Nims+zz-1)=1; % combine all transforms I need to go across sections
        D_MRI(NK)=zz-z;

        
        % LFB: very similar but we need to add histo-mri back and forth to W
        NK=NK+1;
        mri=stackRegMRIread([tempdir filesep 'def_' num2str(zz,'%.4d') ...
            '_to_' num2str(z,'%.4d') '.LFB.nii.gz']);
        mri.vol(isnan(mri.vol))=0;
        svf(:,:,1)=mri.vol(:,:,:,:,2)-XX+1;
        svf(:,:,2)=mri.vol(:,:,:,:,1)-YY+1;
        PHI(:,:,:,NK)=single(svf);
        
        W(NK,2*Nims+z:2*Nims+zz-1)=1;
        W(NK,z)=-1;
        W(NK,zz)=1;
        D_LFB(NK)=zz-z;
        
        % HE: very similar but we need to add histo-mri back and forth to W
        NK=NK+1;
        mri=stackRegMRIread([tempdir filesep 'def_' num2str(zz,'%.4d') ...
            '_to_' num2str(z,'%.4d') '.HE.nii.gz']);
        mri.vol(isnan(mri.vol))=0;
        svf(:,:,1)=mri.vol(:,:,:,:,2)-XX+1;
        svf(:,:,2)=mri.vol(:,:,:,:,1)-YY+1;
        PHI(:,:,:,NK)=single(svf);
        
        W(NK,2*Nims+z:2*Nims+zz-1)=1;
        W(NK,Nims+z)=-1;
        W(NK,Nims+zz)=1;
        D_HE(NK)=zz-z;    
        
    end
end

PHI=PHI(:,:,:,1:NK);
W=W(1:NK,:);
D_MRI = D_MRI(1:NK);
D_LFB = D_LFB(1:NK);
D_HE = D_HE(1:NK);
DELTA = DELTA(1:NK);

sizRES = round(sizeMRI/factor);
npix=prod(sizRES);
PHI_res = zeros([sizRES 2 NK],'single');
for j=1:2
    for k=1:NK
        PHI_res(:,:,j,k)=imresize(PHI(:,:,j,k),sizRES);
    end
end
 
% This is the main algorithm described in the paper
disp('Filtering transforms')

if use_L1 <= 0 % default is L2
    
    % ---- Initialize transformations
    Tres=zeros([sizRES 2 3*Nims-1],'single');
    for n=1:3*Nims-1
        f=find(W(:,n)==1); %which PHI (Rk) are involucrated in Tn
        ff=find(sum(abs(W(f,:)),2)==1); %find(number of registers == 1) which is the direct one
        idx=f(ff);
        Tres(:,:,:,n)=PHI_res(:,:,:,idx);
    end
    
    for iter=1:5 % 5 iterations is usually enough 
        
        % ---- Inference
        fprintf('     Iteration %d \n',iter); drawnow;
        fprintf('        Computing energy of residual \n');  drawnow;
         
        E=zeros(size(D_MRI));
        for k=1:NK
            err=PHI_res(:,:,:,k);
            for n=find(W(k,:))
                err=err-W(k,n)* Tres(:,:,:,n);
            end
            E(k)=sum(sum(sum(sum(err.*err))));
        end
        if iter==1  % initialize, in first iteration
            npix=prod(sizRES);
            vars=zeros(4,1);
            vars(1)=1;
            vars(2)=1;
            vars(3)=1;
            vars(4)=10;
            
            varsK=vars(1)*D_MRI + vars(2)*D_LFB + vars(3)*D_HE + vars(4)*DELTA;
        
        else
            
            fprintf('        Updating variances \n');  drawnow;
            % ---- Update variances
            vars(1) = 1/(npix*2*sum(D_MRI)) * (D_MRI>0)*E';
            vars(2) = 1/(npix*2*sum(D_LFB)) * (D_LFB>0)*E';
            vars(3) = 1/(npix*2*sum(D_HE)) * (D_HE>0)*E';
            vars(4) = 1/(npix*2*sum(DELTA)) * (DELTA>0)*E';
            
            varsK=vars(1)*D_MRI + vars(2)*D_LFB + vars(3)*D_HE + vars(4)*DELTA;

        end
        cost = sum(npix*log(2*pi*varsK) + 0.5*E/varsK);
        fprintf('        Cost at this iteration: %f\n',cost);  drawnow;   
        fprintf('        VARS: %f, %f, %f, %f, %f\n',vars(1),vars(2),vars(3),vars(4));  drawnow;  
        
        % ---- Update transformations 
        fprintf('        Computing weights and updating transforms\n');   drawnow;
        % lambda=inv(W'*diag(1./varsK)*W)*W'*diag(1./varsK);
        % Much faster
        Ws=sparse(double(W));
        aux=bsxfun(@rdivide,W',varsK);
        auxS=sparse(double(aux));
        lambda=single(full(inv(auxS*Ws)*auxS));

        for n=1:3*Nims-1
            if mod(n,50)==0
                fprintf(' %d/%d ',n,3*Nims-1); drawnow;
            end
            if mod(n,400)==0
                fprintf('\n'); drawnow;
            end
            Tres(:,:,:,n)=0;
            for k=1:NK
                if lambda(n,k)~=0
                    Tres(:,:,:,n)=Tres(:,:,:,n)+lambda(n,k)*PHI_res(:,:,:,k);
                end
            end
        end
        fprintf('\n\n');
       
    end
    
else
    
    Tres = zeros(size(PHI_res,1),size(PHI_res,2),2,size(W,2));
    A_lp = sparse(double([-eye(NK) -W; -eye(NK) W]));  % inequality matrix for linear program
    c_lp = [ones(NK,1); zeros(size(W,2),1)];
    for r=1:size(PHI_res,1)
        disp(['Working on row ' num2str(r) ' of ' num2str(size(PHI_res,1))]);
        for c=1:size(PHI_res,2)
            for d=1:2
                
                reg = double(reshape(PHI_res(r,c,d,:),[NK 1]));
                b_lp = [-reg; reg];
                
                % options = optimoptions('linprog','Algorithm','interior-point','display','off','MaxIterations',1000);
                options = optimoptions('linprog','Algorithm','dual-simplex','display','off');
                x = linprog(c_lp,A_lp,b_lp,[],[],[],[],options);
                
                Tres(r,c,d,:)=x(NK+1:end);
                
            end
        end
    end

    
end
        
disp('      Transforms ready!');

T=zeros([sizeMRI 2 size(Tres,4)],'single');
for j=1:2
    for k=1:size(Tres,4)
        T(:,:,j,k)=imresize(Tres(:,:,j,k),sizeMRI);
    end
end

% Now we integrate the velocity fields to compute the deformations
disp('      Computing deformations and applying them to data (and save results)');
nstep=10;

for stainIndex=1:2
    if stainIndex == 1
        stain = 'LFB';
        FLO = LFBimage;
    else
        stain = 'HE';
        FLO = HEimage;
    end
    
    for z=1:Nims

        Tz=T(:,:,:,z+(stainIndex-1)*Nims);

        DISPnl=Tz/2^nstep;
        for s=1:nstep
            x=XX+DISPnl(:,:,1);
            y=YY+DISPnl(:,:,2);
            incx=interp2(DISPnl(:,:,1),y(:),x(:));
            incy=interp2(DISPnl(:,:,2),y(:),x(:));
            DISPnl(:,:,1)=DISPnl(:,:,1)+reshape(incx,size(x));
            DISPnl(:,:,2)=DISPnl(:,:,2)+reshape(incy,size(y));
        end

        % Now, we need to concatenate the affine transform! 
        
        refFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.MRI.png'];
        affineFile=[tempdir filesep 'def_' num2str(z,'%.4d')  '.' stain '_to_MRI.aff'];
        dummyFileNifti=[tempdir filesep 'dummyNifti.nii.gz'];
        cmd1=[TRANSFORMcmd  ' -ref ' refFile ' -disp ' affineFile ' ' dummyFileNifti];
        cmd2=['rm ' dummyFileNifti];
        system([cmd1 ' >/dev/null']);
        aux=stackRegMRIread(dummyFileNifti);
        aux.vol(isnan(aux.vol))=0;
        system([cmd2 ' >/dev/null']);
        DISPaff=zeros(size(DISPnl));
        DISPaff(:,:,1)=squeeze(aux.vol(:,:,:,:,2));
        DISPaff(:,:,2)=squeeze(aux.vol(:,:,:,:,1));

        XX2=XX+DISPnl(:,:,1);
        YY2=YY+DISPnl(:,:,2);
        incx=interp2(DISPaff(:,:,1),YY2(:),XX2(:));
        incy=interp2(DISPaff(:,:,2),YY2(:),XX2(:));
        XX3=XX2+reshape(incx,size(XX2));
        YY3=YY2+reshape(incy,size(YY2));

        DISP=zeros([sizeMRI 2]);
        DISP(:,:,1)=XX3-XX;
        DISP(:,:,2)=YY3-YY;

        RES=deform2D(FLO{z},DISP);

        % Save results
        imwrite(uint8(RES),[outputFileNameBase '_section_' num2str(z,'%.4d') '.' stain '.png']);

        FIELD=DISP;
        save([outputFileNameBase '_section_' num2str(z,'%.4d') '.' stain '.mat'],'FIELD');

        outputFileNonLin=[tempdir filesep 'reg_' num2str(z,'%.4d') '.LFB_to_MRI.nonlin.png'];
        copyfile(outputFileNonLin,[outputFileNameBase '_section_' num2str(z,'%.4d') '.' stain '.noFiltering.png']);
        outputFileNonLin=[tempdir filesep 'reg_' num2str(z,'%.4d') '.HE_to_MRI.nonlin.png'];
        copyfile(outputFileNonLin,[outputFileNameBase '_section_' num2str(z,'%.4d') '.' stain '.noFiltering.png']);
        outputFileNonLin=[tempdir filesep 'reg_' num2str(z,'%.4d') '.HE_to_LFB.nonlin.png'];
        copyfile(outputFileNonLin,[outputFileNameBase '_section_' num2str(z,'%.4d') '.' stain '.noFiltering.png']);

    end
end

system(['rm -rf ' tempdir ' >/dev/null']);

disp('All done!');


