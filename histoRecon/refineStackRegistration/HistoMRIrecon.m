% HistoMRIrecon(MRIlistFile,HISTOlistFile,NiftiRegConfigFile,...
%    outputFileNameBase,NeighborNumberIntra)
%
% MRIlistFile: text file with list of MRI images
%
% HISTOlistFile: text file with corresponding histological images, in same order
%
% NiftiRegConfigFile: text file with location of NiftiReg, registration
% parameters, etc (see README.txt for details)
%
% outputFileNameBase: prefix for the output. The code will write files that
% look like:
% XXX_section_0001.png (deformed image)
% XXX_section_0001.noFiltering.png (deformed image without filtering, i.e., direct registration)
% XXX_section_0001.mat (mat file with nonlinear deformation FIELD, which can be applied with deform2D)
%
% NeighborNumberIntra: number of registered neighbors intramodality. Two is
% a good value for this (see paper).
%
function HistoMRIrecon(MRIlistFile,HISTOlistFile,NiftiRegConfigFile,...
    outputFileNameBase,NeighborNumberIntra, use_L1)

factor = 5; 

% temporary directory
tempdir=[outputFileNameBase '_temp' filesep];
if exist(tempdir,'dir'), rmdir(tempdir,'s'); end
mkdir(tempdir);

% read in lists of files
MRIlist=readFileList(MRIlistFile);
HISTOlist=readFileList(HISTOlistFile);
if length(MRIlist)~=length(HISTOlist)
    error('Error: different number of files in MRI and histology');
end

% Read in configuration of NiftiReg / registration commands
[ALADINcmd,F3Dcmd,TRANSFORMcmd,ALADINparams,F3DparamsInter,F3DparamsIntra] =...
    readNiftyRegConfiguration(NiftiRegConfigFile);

% Input images
disp('Reading in images');
Nims=length(MRIlist);
FLO=cell([1,Nims]);
for z=1:Nims
    aux=imread(MRIlist{z});
    if size(aux,3)>1, aux=rgb2gray(aux);  end
    if z==1
        sizREF=size(aux);
        REF=zeros([sizREF Nims],'uint8');
    end
    REF(:,:,z)=aux;
    
    aux=imread(HISTOlist{z});
    if size(aux,3)>1, aux=rgb2gray(aux);  end
    FLO{z}=aux;
end


% Registration from histo to mri
disp('Computing inter-modality registrations');
for z=1:Nims
    
    disp(['  Slice ' num2str(z) ' of ' num2str(Nims)]);
    
    affineFile=[tempdir filesep 'def_' num2str(z,'%.4d')  '.aff'];
    nonlinearFile=[tempdir filesep 'def_' num2str(z,'%.4d') '.nii.gz'];
    outputFileLin=[tempdir filesep 'reg_' num2str(z,'%.4d') '.lin.png'];
    outputFileNonLin=[tempdir filesep 'reg_' num2str(z,'%.4d') '.nonlin.png'];
    
    floFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.histo.png'];
    refFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.mri.png'];
    dummyFileNifti=[tempdir filesep 'dummyNifti.nii.gz'];
    
    aux=double(REF(:,:,z));
    aux=aux-min(aux(:));
    aux=aux/max(aux(:));
    aux=aux*255;
    aux=uint8(aux);
    imwrite(aux,refFile);
    
    aux=double(FLO{z});
    aux=aux-min(aux(:));
    aux=aux/max(aux(:));
    aux=aux*255;
    aux=uint8(aux);
    imwrite(aux,floFile);
    
    
    cmd1=[ALADINcmd ' -ref ' refFile ' -flo ' floFile ' -aff ' affineFile ...
        ' -res ' outputFileLin ' ' ALADINparams];
    
    cmd2=[F3Dcmd  ' -ref ' refFile ' -flo ' outputFileLin ...
        ' -res ' outputFileNonLin ' -cpp ' dummyFileNifti ' ' F3DparamsInter ' -vel '];
    
    cmd3=[TRANSFORMcmd  ' -ref ' refFile ' -flow ' dummyFileNifti ' ' nonlinearFile];
    
    
    system([cmd1 ' >/dev/null']);
    system([cmd2 ' >/dev/null']);
    system([cmd3 ' >/dev/null']);
    
    delete(dummyFileNifti);
    
end


% Registration from histo to histo and from mri to mri
disp('Computing intra-modality registrations');
for z=1:Nims
    
    disp(['  Slice ' num2str(z) ' of ' num2str(Nims)]);

    % Register to NeighborNumberIntra neighbors (mri and histo)
    for zz=z+1:min(Nims,z+NeighborNumberIntra)
        
        for modalityIndex=1:2
            
            if modalityIndex==1
                modality='histo';
                floFile=[tempdir filesep 'reg_' num2str(zz,'%.4d') '.lin.png'];
                refFile=[tempdir filesep 'reg_' num2str(z,'%.4d') '.lin.png'];
            else
                modality='mri';
                floFile=[tempdir filesep 'image_' num2str(zz,'%.4d') '.mri.png'];
                refFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.mri.png'];
            end
            nonlinearFile=[tempdir filesep 'def_' num2str(zz,'%.4d') '_to_' ...
                num2str(z,'%.4d') '.' modality '.nii.gz'];
            outputFile=[tempdir filesep 'reg_' num2str(zz,'%.4d') '_to_' ...
                num2str(z,'%.4d') '.' modality '.png'];
            
            dummyFileNifti=[tempdir filesep 'dummyNifti.nii.gz'];
            
            cmd1=[F3Dcmd  ' -ref ' refFile ' -flo ' floFile ...
                ' -res ' outputFile ' -cpp ' dummyFileNifti ' ' F3DparamsIntra ' -vel '];
            
            cmd2=[TRANSFORMcmd  ' -ref ' refFile ' -flow ' dummyFileNifti ' ' nonlinearFile];
            
            system([cmd1 ' >/dev/null']);
            system([cmd2 ' >/dev/null']);
            
            delete(dummyFileNifti);
            
            
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is the main filtering part %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This chunk of code:
% - builds matrix with registration paths (W in the paper)
% - builds vector with separations D (d_k in the paper)
% - builds indicator function DELTA (c_k in the paper)
% - and all while reading in the SVFs
disp('Reading in SVFs');

W=zeros(Nims*(1+2*NeighborNumberIntra),2*Nims-1,'single');
D=zeros(1,Nims*(1+2*NeighborNumberIntra),'single');
DELTA=zeros(1,Nims*(1+2*NeighborNumberIntra),'single');

NK=0;
svf=zeros([sizREF 2]);
[XX,YY]=ndgrid(1:sizREF(1),1:sizREF(2));
PHI=zeros([sizREF 2 Nims*(1+2*NeighborNumberIntra)]);

for z=1:Nims
    
    disp(['  Section ' num2str(z) ' of ' num2str(Nims)]);
    
    % histo-mri
    NK=NK+1;
    mri=stackRegMRIread([tempdir filesep 'def_' num2str(z,'%.4d') '.nii.gz']);
    mri.vol(isnan(mri.vol))=0;
    svf(:,:,1)=mri.vol(:,:,:,:,2)-XX+1;
    svf(:,:,2)=mri.vol(:,:,:,:,1)-YY+1;
    PHI(:,:,:,NK)=single(svf);
    
    W(NK,z)=1;
    D(NK)=0;
    DELTA(NK)=1;
    
    % Intramodality
    for zz=z+1:min(Nims,z+NeighborNumberIntra)
        
        % mri
        NK=NK+1;
        mri=stackRegMRIread([tempdir filesep 'def_' num2str(zz,'%.4d') ...
            '_to_' num2str(z,'%.4d') '.mri.nii.gz']);
        mri.vol(isnan(mri.vol))=0;
        svf(:,:,1)=mri.vol(:,:,:,:,2)-XX+1;
        svf(:,:,2)=mri.vol(:,:,:,:,1)-YY+1;
        PHI(:,:,:,NK)=single(svf);

        W(NK,Nims+z:Nims+zz-1)=1; % combine all transforms I need to go across sections
        D(NK)=zz-z;
        DELTA(NK)=0;
        
        % histo: very similar but we need to add histo-mri back and forth to W
        NK=NK+1;
        mri=stackRegMRIread([tempdir filesep 'def_' num2str(zz,'%.4d') ...
            '_to_' num2str(z,'%.4d') '.histo.nii.gz']);
        mri.vol(isnan(mri.vol))=0;
        svf(:,:,1)=mri.vol(:,:,:,:,2)-XX+1;
        svf(:,:,2)=mri.vol(:,:,:,:,1)-YY+1;
        PHI(:,:,:,NK)=single(svf);
        
        W(NK,Nims+z:Nims+zz-1)=1;
        W(NK,z)=-1;
        W(NK,zz)=1;
        D(NK)=zz-z;
        DELTA(NK)=1;
        
    end
end

PHI=PHI(:,:,:,1:NK);
W=W(1:NK,:);
D=D(1:NK);
DELTA=DELTA(1:NK);

sizRES = round(sizREF/factor);
PHI_res = zeros([sizRES 2 NK],'single');
for j=1:2
    for k=1:NK
        PHI_res(:,:,j,k)=imresize(PHI(:,:,j,k),sizRES);
    end
end
 
% This is the main algorithm described in the paper
disp('Filtering transforms')

if use_L1 <= 0 % default is L2
    
    % Initialize transforms
    Tres=zeros([sizRES 2 2*Nims-1],'single');
    for n=1:2*Nims-1
        f=find(W(:,n)==1);
        ff=find(sum(abs(W(f,:)),2)==1);
        idx=f(ff);
        Tres(:,:,:,n)=PHI_res(:,:,:,idx);
    end
    
    for iter=1:5 % usually enough
        
        fprintf('     Iteration %d \n',iter); drawnow;
        fprintf('        Computing energy of residual \n');  drawnow;
        E=zeros(size(D));
        for k=1:NK
            err=PHI_res(:,:,:,k);
            for n=find(W(k,:))
                err=err-W(k,n)* Tres(:,:,:,n);
            end
            E(k)=sum(sum(sum(sum(err.*err))));
        end
        fprintf('        Updating variances \n');  drawnow;
        if iter==1  % initialize, in first iteration
            npix=prod(sizRES);
            vars=zeros(2,1);
            vars(1)=mean(E)/npix/3;
            vars(2)=mean(E)/npix*3;
        end
        % options = optimset('MaxIter',100,'LargeScale','off','GradObj','on','HessUpdat','bfgs','display','iter','TolFun',1e-6,'TolX',1e-6);
        options = optimset('MaxIter',100,'LargeScale','off','GradObj','on','HessUpdat','bfgs','display','off','TolFun',1e-6,'TolX',1e-6);
        varsOld=vars;
        vars = double( fminunc(@(xxx) computeCostAndGradSigmas(xxx,npix,E,D,DELTA),varsOld,options) );
        cost=computeCostAndGradSigmas(vars,npix,E,D,DELTA);
        fprintf('        Cost at this iteration: %f\n',cost);  drawnow;

        fprintf('        Computing weights and updating transforms\n');   drawnow;
        varsK=vars(1)*D+vars(2)*DELTA;

        % lambda=inv(W'*diag(1./varsK)*W)*W'*diag(1./varsK);
        % Much faster
        Ws=sparse(double(W));
        aux=bsxfun(@rdivide,W',varsK);
        auxS=sparse(double(aux));
        lambda=single(full(inv(auxS*Ws)*auxS));

        for n=1:2*Nims-1
            if mod(n,50)==0
                fprintf(' %d/%d ',n,2*Nims-1); drawnow;
            end
            if mod(n,400)==0
                fprintf('\n'); drawnow;
            end
            Tres(:,:,:,n)=0;
            for k=1:NK
                if lambda(n,k)~=0
                    Tres(:,:,:,n)=Tres(:,:,:,n)+lambda(n,k)*PHI_res(:,:,:,k);
                end
            end
        end
        fprintf('\n\n');

    end
    
else
    
    Tres = zeros(size(PHI_res,1),size(PHI_res,2),2,size(W,2));
    A_lp = sparse(double([-eye(NK) -W; -eye(NK) W]));  % inequality matrix for linear program
    c_lp = [ones(NK,1); zeros(size(W,2),1)];
    for r=1:size(PHI_res,1)
        disp(['Working on row ' num2str(r) ' of ' num2str(size(PHI_res,1))]);
        for c=1:size(PHI_res,2)
            for d=1:2
                
                reg = double(reshape(PHI_res(r,c,d,:),[NK 1]));
                b_lp = [-reg; reg];
                
                % options = optimoptions('linprog','Algorithm','interior-point','display','off','MaxIterations',1000);
                options = optimoptions('linprog','Algorithm','dual-simplex','display','off');
                x = linprog(c_lp,A_lp,b_lp,[],[],[],[],options);
                
                Tres(r,c,d,:)=x(NK+1:end);
                
            end
        end
    end

    
end
        
disp('      Transforms ready!');

T=zeros([sizREF 2 size(Tres,4)],'single');
for j=1:2
    for k=1:size(Tres,4)
        T(:,:,j,k)=imresize(Tres(:,:,j,k),sizREF);
    end
end

% Now we integrate the velocity fields to compute the deformations
disp('      Computing deformations and applying them to data (and save results)');
nstep=10;
for z=1:Nims
    
    Tz=T(:,:,:,z);
    
    DISPnl=Tz/2^nstep;
    for s=1:nstep
        x=XX+DISPnl(:,:,1);
        y=YY+DISPnl(:,:,2);
        incx=interp2(DISPnl(:,:,1),y(:),x(:));
        incy=interp2(DISPnl(:,:,2),y(:),x(:));
        DISPnl(:,:,1)=DISPnl(:,:,1)+reshape(incx,size(x));
        DISPnl(:,:,2)=DISPnl(:,:,2)+reshape(incy,size(y));
    end
    
    % Now, we need to concatenate the affine transform!
    refFile=[tempdir filesep 'image_' num2str(z,'%.4d') '.mri.png'];
    affineFile=[tempdir filesep 'def_' num2str(z,'%.4d')  '.aff'];
    dummyFileNifti=[tempdir filesep 'dummyNifti.nii.gz'];
    cmd1=[TRANSFORMcmd  ' -ref ' refFile ' -disp ' affineFile ' ' dummyFileNifti];
    cmd2=['rm ' dummyFileNifti];
    system([cmd1 ' >/dev/null']);
    aux=stackRegMRIread(dummyFileNifti);
    aux.vol(isnan(aux.vol))=0;
    system([cmd2 ' >/dev/null']);
    DISPaff=zeros(size(DISPnl));
    DISPaff(:,:,1)=squeeze(aux.vol(:,:,:,:,2));
    DISPaff(:,:,2)=squeeze(aux.vol(:,:,:,:,1));
    
    XX2=XX+DISPnl(:,:,1);
    YY2=YY+DISPnl(:,:,2);
    incx=interp2(DISPaff(:,:,1),YY2(:),XX2(:));
    incy=interp2(DISPaff(:,:,2),YY2(:),XX2(:));
    XX3=XX2+reshape(incx,size(XX2));
    YY3=YY2+reshape(incy,size(YY2));
    
    DISP=zeros([sizREF 2]);
    DISP(:,:,1)=XX3-XX;
    DISP(:,:,2)=YY3-YY;
    
    RES=deform2D(FLO{z},DISP);
    
    % Save results
    imwrite(uint8(RES),[outputFileNameBase ...
        '_section_' num2str(z,'%.4d') '.png']);
    
    FIELD=DISP;
    save([outputFileNameBase '_section_' num2str(z,'%.4d') '.mat'],'FIELD');
    
    outputFileNonLin=[tempdir filesep 'reg_' num2str(z,'%.4d') '.nonlin.png'];
    copyfile(outputFileNonLin,[outputFileNameBase ...
        '_section_' num2str(z,'%.4d') '.noFiltering.png']);
    
end


system(['rm -rf ' tempdir ' >/dev/null']);

disp('All done!');


