% This script deforms the manual segmentations of a block.
% This is an alternative to doing it along with the intensities in 
% RegisterHistoBlock.m; it may be a good idea to merge them both in the 
% future!
clear

%%%%%%%%%%%%%%%%%
% INPUT VARIABLES

% output directory
outputDir='/home/acasamitjana/Data/P41-16/outputHistoRecon/LFB/L1_P3.3/';

% directory with deformation fields
fieldDir='/home/acasamitjana/Data/P41-16/outputHistoRecon/LFB/L1_P3.3/P41-16_P3.3_LFB_deformationFields';

% mosaic / jigsaw puzzle reconstruction dir
reconDir='/home/acasamitjana/Data/P41-16/mosaicPuzzle/registeredBlocks0.5mm/'; 

% block name, stain, and histo directory
histoDir='/home/acasamitjana/Data/P41-16/ScannedSlides';
histoLabelDir='/home/acasamitjana/Data/P41-16/Segmentation/InterestingBlocks';
stain='LFB';
blockName='P41-16_P3.3';

% nominal resolutions of blockface and histo (you probably shouldn't touch these)
BFres=0.1;
Hres=3.9688e-3; % = 25.4/6400; 

% Downsample factor for annotations of histology.
% I think we should upsample the histo and set this to 1... 
downsampleFactorHistoLabels=4;

%%%%%%%%%%%%%%%%%
% CODE 

addpath(genpath([pwd() filesep '..' filesep 'functions']));
addpath(genpath([pwd() filesep 'refineStackRegistration']));

% data directories and files
histoBlockDir=[histoDir filesep blockName filesep stain];
histoLabelBlockDir=[histoLabelDir filesep blockName filesep];
load([histoBlockDir filesep blockName  '_' stain '_mapping.mat'],'mapping');
BFmriName=[reconDir filesep blockName '_volume.mri.reg.mgz'];
BFrotName=[reconDir filesep blockName '_volume.mri.rot.mat'];
load(BFrotName,'LRflip','rot');

% Read in MRI block, as reference (only header)
Imri=myMRIread(BFmriName,1);

 
% Now we can warp histo labels at low-res and high-res
disp('Warping images');

% First, prepare mri structure for 3D low-res
mri=[];
mri.vol=zeros([Imri.volsize(1) Imri.volsize(2) length(mapping)]);
mri.vox2ras0=Imri.vox2ras0;
mri.vox2ras0(1:3,4)=mri.vox2ras0(1:3,4)+mri.vox2ras0(1:3,1:3)*[0; 0; mapping(1)-1];
mri.vox2ras0(1:3,3)=mri.vox2ras0(1:3,3)*(mapping(2)-mapping(1));
aux=sqrt(sum(mri.vox2ras0.^2));
mri.volres=aux(1:3); 

% Go over images
for m=1:length(mapping)
    
%     try

    disp(['   Working on image ' num2str(m) ' of ' num2str(length(mapping))]);
    
    % Read in and rotate / flip if needed
    Lmri=MRIread([histoLabelBlockDir filesep blockName '_' stain '_' num2str(m,'%.2d') '_seg.nii.gz']);
    L=imrotate(Lmri.vol,180); % This is to undo the 180 deg rotation in the jpg->nifti conversion (Nellie)
    if rot~=0, L = imrotate(L,-rot); end
    if LRflip>0, L=fliplr(L); end
    
    Lresized = imresize(L,Hres/BFres*downsampleFactorHistoLabels,'nearest');
    
    load([fieldDir filesep 'output_section_' num2str(m,'%.4d') '.mat']);
    
    LDresized=deform2D(Lresized,FIELD,'nearest'); 
    LDresized(isnan(LDresized))=0;
    mri.vol(:,:,m)=LDresized;
%     myMRIwrite(mri,[outputDir filesep blockName '_' stain '_' num2str(m,'%.2d') '.labels.mgz']);
    
%     % Deform highres
%     F1=imresize(FIELD(:,:,1),BFres/Hres/downsampleFactorHistoLabels)*BFres/Hres/downsampleFactorHistoLabels;
%     F2=imresize(FIELD(:,:,2),BFres/Hres/downsampleFactorHistoLabels)*BFres/Hres/downsampleFactorHistoLabels;
%     FIELDlarge=cat(3,F1,F2);
%     LD=deform2D(L,FIELDlarge,'nearest');
%     LD(isnan(LD))=0;
%     
%     
%     % Write deformed highres (we do one at the time)
%     mr=[];
%     mr.vox2ras0=mri.vox2ras0;
%     mr.vox2ras0(1:3,1:2)=mr.vox2ras0(1:3,1:2)*Hres/BFres*downsampleFactorHistoLabels;
%     mr.vox2ras0(1:3,4)=mr.vox2ras0(1:3,4)+mr.vox2ras0(1:3,1:3)*...
%         [-0.5*(BFres/Hres/downsampleFactorHistoLabels-1); -0.5*(BFres/Hres/downsampleFactorHistoLabels-1); m-1];
%     aux=sqrt(sum(mr.vox2ras0.^2));
%     mr.volres=aux(1:3);
%     mr.vol=LD;
%         
%     % This is way too large; we can crop a bit using the labels
%     [LDcrop,cropping]=cropLabelVol(LD);
%     mr.vol=LDcrop;
%     mr.vox2ras0(1:3,4)=mr.vox2ras0(1:3,4)+mr.vox2ras0(1:3,1:3)*[cropping(2)-1; cropping(1)-1; 0];
%     
%     % write to disk
%     myMRIwrite(mr,[outputDir filesep blockName '_' stain '_' num2str(m,'%.2d') '.labels.mgz']);

%     catch
%         disp('      No labels available for this section');
%     end
end

myMRIwrite(mri,[outputDir filesep blockName '_' stain  '.volume.labels.mgz']);

% % It makes sense to crop the 3D volume to make it lighter
% disp('Cropping label volume and writing them to disk');
% [Lcrop,cropping]=cropLabelVol(mri.vol);
% mri.vox2ras0(1:3,4)=mri.vox2ras0(1:3,4)+mri.vox2ras0(1:3,1:3)*[cropping(2)-1; cropping(1)-1; cropping(3)-1];
% mri.vol=Lcrop;
% myMRIwrite(mri,[outputDir filesep blockName '_' stain '.volume.labels.mgz']);
% 
% disp('All done!');

