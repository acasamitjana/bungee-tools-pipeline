%  This script essentially goes over a block:
% - asking the user for the rotation and/or flip to match blockface (MRI) & histo
% - matching the histogram of the histo sections to that of the MRI
% - using the technique from the 2018 MICCAI paper (Iglesias et al) to register 
%   the stacks of histo and (resampled) MRI, using downsampled images
% - composes a (slice-wise) equalized histo volume at lowres.
% - producing a set of niftis (one per section) with the registered histology at full-res.
clear
subject = 'P41-16';
if  exist([pwd() filesep '..' filesep 'experiment_options' filesep subject '.mat'])
    load([pwd() filesep '..' filesep 'experiment_options' filesep subject '.mat'], 'options');
    
    for taskID=1:length(options)
    
    stain = options{taskID}{1};
    if strcmp(stain, 'LFB_HE')
        continue
    end
    blockName = options{taskID}{2};
    if strcmp(blockName(end-3:end-2), 'BS')
        continue
    end
    USE_L1 = options{taskID}{3};
    if USE_L1 == 1
        loss='L2';
    else
        continue
    end
    save('run_options.mat', 'stain', 'blockName', 'loss', 'USE_L1')
    try

        disp(['Processing ' loss '_' stain '_' blockName])
        AlignPhotoHisto

    catch
        fileID=fopen('missing_blocks_20200131.txt','a');
        fprintf(fileID,'%s, %s, %s\n',stain, blockName,loss);
        fclose(fileID);
        continue
    end
    delete('run_options.mat')
    
    end

else
    if exist('run_options.mat')
        delete('run_options.mat')
    end
    AlignPhotoHisto
end