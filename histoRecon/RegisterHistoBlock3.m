%  This script essentially goes over a block:
% - asking the user for the rotation and/or flip to match blockface (MRI) & histo
% - matching the histogram of the histo sections to that of the MRI
% - using the technique from the 2018 MICCAI paper (Iglesias et al) to register 
%   the stacks of histo and (resampled) MRI, using downsampled images
% - composes a (slice-wise) equalized histo volume at lowres.
% - producing a set of niftis (one per section) with the registered histology at full-res.

%%%%%%%%%%%%%%%%%
% INPUT VARIABLES
YamlStruct = ReadYaml([fileparts(mfilename('fullpath')) filesep '..' filesep 'configFile.yaml']);

% Input MRI
inputMRI=YamlStruct.mriFile;

% output directory
outputHistoRecDir=YamlStruct.HistoRecDir;

% mosaic / jigsaw puzzle reconstruction dir
reconDir=YamlStruct.RegisteredBlocksDir;

% block name, stain, and histo directory
histoDir=YamlStruct.HistoDir;

% number of neighbors for intramodality registrations (2-3 is good)
NeighborNumberIntra=3;

% nominal resolutions of blockface and histo (you probably shouldn't touch these)
BFres=0.1;
Hres=3.9688e-3; % = 25.4/6400;

% Add paths
addpath(genpath(YamlStruct.histoRecFunc));
addpath(genpath(YamlStruct.histoUtilsFunc));
addpath(genpath(YamlStruct.utilsFunc));


% NiftyReg
setenv('PATH', [getenv('PATH') ':' YamlStruct.niftyReg]);

% Experiment parameters
if exist('run3_options.mat')
    load('run3_options.mat')
else
    stain = 'LFB';
    blockName = 'P57-16_P1.2';
    USE_L1 = 0;
    loss = 'L2';
end


%%%%%%%%%%%%%%%%%
% CODE 
% Gather data and write to disk in preparation for call to
% "refineStackRegistration", which is the implementation of the method
% described in our MICCAI 2018 paper  "Model-based refinement of nonlinear 
% registrations in 3D histology reconstruction".

% output directory
outputDir = [outputHistoRecDir filesep stain filesep loss filesep blockName(end-3:end)];
if exist(outputDir,'dir')==0
    mkdir(outputDir);
%else
%    delete([outputDir filesep '*.*']);
end

% temporary directory
tempdir = [tempdir filesep 'histoRecon_' blockName filesep];
if exist(tempdir,'dir')==0
    mkdir(tempdir);
%else
%    delete(tempdir);
%    mkdir(tempdir);
end

% data directories and files
histoLFBBlockDir=[histoDir filesep blockName filesep 'LFB'];
histoHEBlockDir=[histoDir filesep blockName filesep 'HE'];

load([histoLFBBlockDir filesep blockName '_LFB_mapping.mat'],'mapping'); mappingLFB=mapping; clear mapping
load([histoHEBlockDir filesep blockName '_HE_mapping.mat'],'mapping'); mappingHE=mapping; clear mapping

BFmriName=[reconDir filesep blockName '_volume.mri.reg.mgz'];
BFmriMaskName=[reconDir filesep blockName '_volume.mask.reg.mgz'];
BFrotName=[reconDir filesep blockName '_volume.mri.rot.mat'];

% input data for histo recon code
disp('Reading in MRI and histo data');

disp('    MRI ... ');
Imri=myMRIread(BFmriName);
Mmri=myMRIread(BFmriMaskName);
Msoft = Mmri.vol / max(Mmri.vol(:));
Mhard = Msoft > 0.5;
% If we run out of mask for whatever reason, we copy from previous slices
% Not the pretties, but it works  TODO: improve?
aux=squeeze(sum(sum(Msoft,1),2));
ok = find(aux>=5000);
notok = find(aux<5000);
for j = 1 : length(notok)
    f=notok(j);
    dist=abs(ok-f);
    [~,idx]=min(dist);
    ref=ok(idx);
    Msoft(:,:,f)=Msoft(:,:,ref);
end
    
% Now we stretch histogram and (soft) mask
aux=Imri.vol(Mhard);
mini=quantile(aux,0.001);
maxi=quantile(aux,0.999);
I = Msoft .* (255 * (Imri.vol - mini) / (maxi-mini));
I = uint8(I);

% Address mapping issues.
if length(mappingLFB) == length(mappingHE)
    mapping = mappingLFB;
elseif length(mappingLFB) < length(mappingHE)
    mapping = mappingLFB;
else
    mapping = mappingLFB(1:length(mappingHE));
end


% And histo (we start from the middle to ask the user for the right rotation
if strcmp(blockName(end-3:end), 'P8.1') || strcmp(blockName(end-3:end), 'C5.1')
    mapping = size(I,3) - mapping; 
end
load(BFrotName,'LRflip','rot');
%for m = 1:length(mapping)
%
%    disp(['    Histo: section ' num2str(m) ' of ' num2str(length(mapping))]);
%    
%    % MRI
%    Is=I(:,:,mapping(m));
%
%    % Histo
%    HLFB = imread([histoLFBBlockDir filesep blockName '_LFB_' num2str(m,'%.2d') '.jpg']);
%    HLFBresized = imresize(HLFB,Hres/BFres);
%    MLFB = imread([histoLFBBlockDir filesep blockName '_LFB_' num2str(m,'%.2d') '.mask.png']);
%    MLFBresized = imresize(MLFB,[size(HLFBresized,1) size(HLFBresized,2)]),
%
%    HHE = imread([histoHEBlockDir filesep blockName '_HE_' num2str(m,'%.2d') '.jpg']);
%    HHEresized = imresize(HHE,Hres/BFres);
%    MHE = imread([histoHEBlockDir filesep blockName '_HE_' num2str(m,'%.2d') '.mask.png']);
%    MHEresized = imresize(MHE,[size(HHEresized,1) size(HHEresized,2)]);
%
%    % Rotate and flip as needed, and match histogram (using mask)
%    HLFBrot = uint8(double(rgb2gray(HLFBresized)).*double(MLFBresized(:,:,1)>127));
%    HLFBrot = imrotate(HLFBrot,-rot);
%    HHErot = uint8(double(rgb2gray(HHEresized)).*double(MHEresized(:,:,1)>127));
%    HHErot = imrotate(HHErot,-rot);
%    if LRflip>0
%        HLFBrot=fliplr(HLFBrot);
%        HHErot=fliplr(HHErot);
%    end
%    HHErot(HHErot==1) = 0;
%
%    HHEs = matchHistoMasked(HHErot, Is);
%    HLFBs = matchHistoMasked(HLFBrot, Is);
%
%    imwrite(Is,[tempdir filesep 'MRI_' num2str(m,'%.2d') '.tif']);
%    imwrite(HLFBs,[tempdir filesep 'LFB_' num2str(m,'%.2d') '.tif']);
%    imwrite(HHEs,[tempdir filesep 'HE_' num2str(m,'%.2d') '.tif']);
%end
%
%% write file lists
%fidM=fopen([tempdir filesep 'MRI_list.txt'],'w');
%fidHLFB=fopen([tempdir filesep 'LFB_list.txt'],'w');
%fidHHE=fopen([tempdir filesep 'HE_list.txt'],'w');
%for m=1:length(mapping)
%    fprintf(fidM,'%s\n',[tempdir filesep 'MRI_' num2str(m,'%.2d') '.tif']);
%    fprintf(fidHLFB,'%s\n',[tempdir filesep 'LFB_' num2str(m,'%.2d') '.tif']);
%    fprintf(fidHHE,'%s\n',[tempdir filesep 'HE_' num2str(m,'%.2d') '.tif']);
%end
%fclose(fidM);
%fclose(fidHLFB);
%fclose(fidHHE);
%
%% Histo-MRI registration
%disp('Starting refinement of registrations');
%HistoMRIrecon3([tempdir filesep 'MRI_list.txt'], ...
%    [tempdir filesep 'LFB_list.txt'], [tempdir filesep 'HE_list.txt'], ...
%    YamlStruct.niftyRegConfigFile,[outputDir filesep 'output'],...
%    NeighborNumberIntra,USE_L1);
%
%disp('Done with refinement of registrations');
% 
% Now we can warp histo at low-res and high-res
disp('Warping images');
load(BFrotName,'LRflip','rot');
% Go over images
for stainIndex=1:2
    % First, prepare mri structure for 3D low-res
    mri=[];
    mri.vol=zeros([size(Imri.vol,1) size(Imri.vol,2) length(mapping) 3]);
    mri.vox2ras0=Imri.vox2ras0;
    mri.vox2ras0(1:3,4)=mri.vox2ras0(1:3,4)+mri.vox2ras0(1:3,1:3)*[0; 0; mapping(1)-1];
    mri.vox2ras0(1:3,3)=mri.vox2ras0(1:3,3)*(mapping(2)-mapping(1));
    aux=sqrt(sum(mri.vox2ras0.^2));
    mri.volres=aux(1:3); 
    mriMask=mri;
    mriMask.vol=zeros([size(Imri.vol,1) size(Imri.vol,2) length(mapping)]);


    if stainIndex ==1
        stain = 'LFB';
        histoBlockDir = histoLFBBlockDir;
    else
        stain = 'HE';
        histoBlockDir = histoHEBlockDir;
    end
    for m=1:length(mapping)
        disp(['   Working on image ' num2str(m) ' of ' num2str(length(mapping))]);
    

        % Read in and rotate / flip if needed
        H = imread([histoBlockDir filesep blockName '_' stain '_' num2str(m,'%.2d') '.jpg']);
        M = imread([histoBlockDir filesep blockName '_' stain '_' num2str(m,'%.2d') '.mask.png']);
        if rot~=0, H = imrotate(H,-rot); M = imrotate(M,-rot); end
        if LRflip>0, H=fliplr(H); M=fliplr(M); end
        Hresized = imresize(H,Hres/BFres);
        Mresized = imresize(M,[size(Hresized,1) size(Hresized,2)]);
        load([outputDir filesep 'output_section_' num2str(m,'%.4d') '.' stain '.mat'], 'FIELD');

        % Deform lowres 
        HDresized=deform2D(Hresized,FIELD);
        MDresized=deform2D(Mresized,FIELD); 
        HDresized(isnan(HDresized))=0;
        MDresized(isnan(MDresized))=0;
        MDresized=repmat(MDresized/255,[1 1 3]);
        HDresized=HDresized.*MDresized;
        HDresized=HDresized + 255 * (1-MDresized); % uncomment for white background instead of black
        HDresized=uint8(HDresized);

        mri.vol(:,:,m,:)=reshape(HDresized,[size(mri.vol,1) size(mri.vol,2) 1 3]);
        mriMask.vol(:,:,m)=255*double(MDresized(:,:,1)>0.5); 

    %     % Deform highres
    %     F1=imresize(FIELD(:,:,1),BFres/Hres)*BFres/Hres;
    %     F2=imresize(FIELD(:,:,2),BFres/Hres)*BFres/Hres;
    %     FIELDlarge=cat(3,F1,F2);
    %     HD=deform2D(H,FIELDlarge);
    %     MD=deform2D(M,FIELDlarge);
    %     HD(isnan(HD))=0;
    %     MD(isnan(MD))=0;
    %     MD=repmat(MD/255,[1 1 3]);
    %     HD=HD.*MD;
    %     HD=HD + 255 * (1-MD); % uncomment for white background instead of black
    %     HD=uint8(HD);
    %     
    %     % TODO: eventually we'll need to warp labels here!
    %     
    %     % Write deformed highres (we do one at the time)
    %     mr=[];
    %     mr.vox2ras0=mri.vox2ras0;
    %     mr.vox2ras0(1:3,1:2)=mr.vox2ras0(1:3,1:2)*Hres/BFres;
    %     mr.vox2ras0(1:3,4)=mr.vox2ras0(1:3,4)+mr.vox2ras0(1:3,1:3)*[-0.5*(BFres/Hres-1); -0.5*(BFres/Hres-1); m-1];
    %     aux=sqrt(sum(mr.vox2ras0.^2));
    %     mr.volres=aux(1:3);
    %     
    %     % This is way too large; we can crop a bit using the mask
    %     Mhard=MD(:,:,1)>0.5;
    %     [Mcrop,cropping]=cropLabelVol(Mhard);
    %     cropping(6)=3; % for RGB
    %     mr.vol=reshape(applyCropping(HD,cropping),[size(Mcrop,1) size(Mcrop,2) 1 3]);
    %     mr.vox2ras0(1:3,4)=mr.vox2ras0(1:3,4)+mr.vox2ras0(1:3,1:3)*[cropping(2)-1; cropping(1)-1; 0];
    %     
    %     % And the mask
    %     mrMask=mr;
    %     mrMask.vol=uint8(255*double(Mcrop));
    %     
    %     % write to disk
    %     myMRIwrite(mr,[outputDir filesep blockName '_' stain '_' num2str(m,'%.2d') '.highres.rgb.mgz']);
    %     myMRIwrite(mrMask,[outputDir filesep blockName '_' stain '_' num2str(m,'%.2d') '.highres.mask.mgz']); 
    end


    disp('Equalizing');
    % build reference histogram
    mriEq=mri;
    maxi=max(mriMask.vol(:));
    for c=1:3
        aux=mri.vol(:,:,:,c);
        aux=aux(mriMask.vol>maxi/2);
        refHist= imhist(uint8(aux));
        refHist=refHist/sum(refHist);
        for j=1:size(mri.vol,3)
            mriEq.vol(:,:,j,c)= matchHistoMasked(uint8(mri.vol(:,:,j,c).*mriMask.vol(:,:,j)/maxi), refHist);
        end
    end

    % It makes sense to crop to make them lighter
    disp('Cropping volumes and writing them to disk');
    [Mcrop,cropping]=cropLabelVol(mriMask.vol>maxi/4); % conservative
    Icrop=zeros([size(Mcrop) 3]);
    for c=1:3, Icrop(:,:,:,c)=applyCropping(mriEq.vol(:,:,:,c),cropping); end
    mriEq.vox2ras0(1:3,4)=mriEq.vox2ras0(1:3,4)+mriEq.vox2ras0(1:3,1:3)*[cropping(2)-1; cropping(1)-1; cropping(3)-1];
    mriEq.vol=Icrop;
    mriMask=mriEq;
    mriMask.vol=Mcrop;
    myMRIwrite(mriEq,[outputDir filesep blockName '_' stain '.volume.rgb.mgz']);
    myMRIwrite(mriMask,[outputDir filesep blockName '_' stain '.volume.mask.mgz']);

    % Spare deformation fields, in case we want to deform labels later on
    mkdir([outputDir filesep blockName '_' stain '_deformationFields_' stain]);
    for m=1:length(mapping)
        movefile([outputDir filesep 'output_section_' num2str(m,'%.4d') '.' stain  '.mat'],...
        [outputDir filesep blockName '_' stain '_deformationFields_' stain filesep]);
    end
end

% Delete temporary stuff
%disp('Clearning up');
%delete([outputDir filesep '*.png']);
%delete([tempdir filesep '*.*']);
%try
%    rmdir(tempdir);
%catch
%    disp('Could not delete temporary directory:');
%    disp(tempdir);
%    disp('You may want to erase it manually');
%end
%
disp('All done!');

