clear
subject = 'P41-16';
YamlStruct = ReadYaml([fileparts(mfilename('fullpath')) filesep '..' filesep 'configFile.yaml']);

if  exist([pwd() filesep '..' filesep 'experiment_options' filesep subject '.mat'])
    load([pwd() filesep '..' filesep 'experiment_options' filesep subject '.mat'], 'options');
    
    for taskID=1:length(options)
    
        stain = options{taskID}{1};
        if strcmp(stain, 'LFB_HE') || strcmp(stain, 'HE')
            continue
        end
        blockName = options{taskID}{2}; 
        if strcmp(blockName(end-3:end-2), 'BS')% || strcmp(blockName(end-3), 'C')
            continue
        end        
            
        USE_L1 = options{taskID}{3};
        if USE_L1 == 1
            loss='L1';
        else
            continue
        end
        
        if exist([YamlStruct.HistoRecDir filesep stain filesep loss '_' blockName(end-3:end) filesep blockName '_' stain '_deformationFields'])
            continue
        end
        
        save('run_options.mat', 'stain', 'blockName', 'loss', 'USE_L1')
        
        try
            disp(['Processing ' loss '_' stain '_' blockName])
            RegisterHistoBlock
        catch
            fileID=fopen('missing_blocks_20200131.txt','a');
            fprintf(fileID,'%s, %s, %s\n',stain, blockName,loss);
            fclose(fileID);
            continue
        end
        delete('run_options.mat')
    end

else
    if exist('run_options.mat')
        delete('run_options.mat')
    end
    RegisterHistoBlock
end
