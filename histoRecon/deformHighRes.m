% This script deforms a region of a microscopically scanned section using
% a field estimated by RegisterHistoBlock.m
clear
close all
clc

% file with deformation field
fieldMatFile='/Users/iglesias/Downloads/outputHistoRecon_L1_A1.3/P57-16_A1.3_LFB_deformationFields/output_section_0007.mat';

% mat file with rotations / flips of blockface
BFrotName='/Users/iglesias/Downloads/reconBlocks0.5mm//P57-16_A1.3_volume.mri.rot.mat';

% original flatbed image
histoFlatBed='~/Downloads/histo/P57-16_A1.3_LFB/P57-16_A1.3_LFB_07.jpg';

% highres (microscopic) histology (parent file)
histoMicroscope='~/Downloads/SlidesForEugenio/__LFB.vsi';

% corresponding (registered) blockface volume, index of section, and
% frequency of sections ("skip", 10 for interesting blocks, 20 for boring)
BFvolume='/Users/iglesias/Downloads/reconBlocks0.5mm//P57-16_A1.3_volume.gray.reg.mgz';
BFindex=69; % 10 * n - 1
skip=10;

% level of magnification: 1 is maximum, 2 is next, etc, and output file name
% levelHisto=3;
% outputfilename='~/Downloads/outputHistoRecon_L1_A1.3/basalganglia_level3.mgz';
% levelHisto=1;
% outputfilename='~/Downloads/outputHistoRecon_L1_A1.3/hippo_molecular_layer_level1.mgz';
% levelHisto=2;
% outputfilename='~/Downloads/outputHistoRecon_L1_A1.3/cortex_level2.mgz';
levelHisto=2;
outputfilename='~/Downloads/outputHistoRecon_L1_A1.3/cortex_level2b.mgz';

% nominal resolutions of blockface and histo (you probably shouldn't touch these)
BFres=0.1;
Hres=3.9688e-3; % = 25.4/6400; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(genpath('refineStackRegistration'));
addpath(genpath('bfmatlab'));
addpath(genpath(['..' filesep 'functions']));

% Read histo and downsample to work resolution
disp('Reading and downsampling flatbed histology');
Hfb=imread(histoFlatBed);
Hfb = imresize(Hfb,Hres/BFres);

% Rotate/flip if needed
load(BFrotName,'LRflip','rot');
Hfb = imrotate(Hfb,-rot);
if LRflip>0
    Hfb=fliplr(Hfb);
end

% Get the Olumpus format reader, and go through the available images
disp('Opening microscopy data and finding suitable work resolution');
reader=bfGetReader(histoMicroscope);
numSeries = reader.getSeriesCount();
sizes=zeros([numSeries,2]);
for s=1:numSeries
    reader.setSeries(s - 1);
    sizes(s,:)=[reader.getSizeX() reader.getSizeY()];
end
ratios=sizes(:,1)./sizes(:,2);

[~,idHisto]=max(mean(sizes,2)); 
idHisto = idHisto + levelHisto - 1; 
targetRatio=ratios(idHisto);

ok=abs(ratios-targetRatio)<0.005;
aux=prod(sizes,2);
aux(ok==0)=0;
[~,idWork]=min(abs(aux-numel(Hfb)/3));

% Get whole image an screen resolution
reader.setSeries(idWork - 1);
ImcLR=zeros([sizes(idWork,2),sizes(idWork,1),3],'uint8');
for channel=1:3
    ImcLR(:,:,channel)=bfGetPlane(reader,channel,1,1,sizes(idWork,1),sizes(idWork,2));
end

% Registration: moving is ImcLR, reference is Hfb
disp('  Similarity registration with SURF');
pointsFB = detectSURFFeatures(rgb2gray(Hfb),'NumOctaves',5);
[fFB,vptsFB] = extractFeatures(rgb2gray(Hfb),pointsFB);
pointsMC = detectSURFFeatures(rgb2gray(ImcLR),'NumOctaves',5);
[fMC,vptsMC] = extractFeatures(rgb2gray(ImcLR),pointsMC);
indexPairs = matchFeatures(fFB,fMC) ;
matchedPoints1 = vptsFB(indexPairs(:,1));
matchedPoints2 = vptsMC(indexPairs(:,2));
tform = estimateGeometricTransform(matchedPoints2,matchedPoints1,'affine','MaxNumTrials',10000,'Confidence',99.9999);

% Inerface to select region; deform flatbed histology first
load(fieldMatFile,'FIELD');
FBdeformed=deform2D(Hfb,FIELD);
disp('Please select region to enlarge');
h = msgbox('Please select region to enlarge');
uiwait(h);
figure(1), imshow(uint8(FBdeformed));
h = imrect;
position = wait(h);
position=round(position);
r1=position(2); 
r2=position(2)+position(4); 
c1=position(1); 
c2=position(1)+position(3);

% Prepare deformation field
disp('Preparing deformation field');
s1=sizes(idHisto,:);
s2=sizes(idWork,:);
factorSize=mean(s1./s2); % between histo and work resolution
factorSampling=factorSize/sqrt(det(tform.T)); % also includes transform from flatbed to microscope
[RR,CC]=ndgrid(r1:1/factorSampling:r2,c1:1/factorSampling:c2);
RR2=RR+reshape(interpn(FIELD(:,:,1),RR(:),CC(:)),size(RR));
CC2=CC+reshape(interpn(FIELD(:,:,2),RR(:),CC(:)),size(RR));
aux=inv(tform.T')*[CC2(:)'; RR2(:)'; ones(1,numel(RR2))];
CC3=reshape(aux(1,:),size(CC2)); 
RR3=reshape(aux(2,:),size(RR2)); 
RR4=(RR3-0.5)*factorSize;
CC4=(CC3-0.5)*factorSize;

disp('Extracting bounding box from microscopy data');
minR=double(floor(min(RR4(:))));
maxR=double(floor(max(RR4(:))));
minC=double(ceil(min(CC4(:))));
maxC=double(ceil(max(CC4(:))));

Imag=zeros([maxR-minR+1,maxC-minC+1,3],'single');
reader.setSeries(idHisto - 1);
for channel=1:3
    Imag(:,:,channel)=bfGetPlane(reader,channel,minC,minR,maxC-minC+1,maxR-minR+1);
end
reader.close();

disp('Interpolating');
RR4m=RR4-minR+1;
CC4m=CC4-minC+1;
Imag2=zeros([size(RR4m) 3]);
for c=1:3
   Imag2(:,:,c)=interpn(Imag(:,:,c),RR4m,CC4m); 
end

disp('Building MRI structure and writing to disk');

BFmri=myMRIread(BFvolume,1);
mri=[];
% mri.vol=zeros([BFmri.volsize(1) BFmri.volsize(2) 1]);
mri.vox2ras0=BFmri.vox2ras0; 
mri.vox2ras0(1:3,4)=mri.vox2ras0(1:3,4)+mri.vox2ras0(1:3,1:3)*[0; 0; BFindex-1];
mri.volres=BFmri.volres;

mr=[];
mr.vox2ras0=mri.vox2ras0;
mr.vox2ras0(1:3,1:2)=mr.vox2ras0(1:3,1:2)/factorSampling;
mr.vox2ras0(1:3,4)=mr.vox2ras0(1:3,4)+mr.vox2ras0(1:3,1:3)*[-0.5*(factorSampling-1); -0.5*(factorSampling-1); 0];
mr.vox2ras0(1:3,4)=mr.vox2ras0(1:3,4)+mr.vox2ras0(1:3,1:3)*[(c1-.5)*factorSampling; (r1-.5)*factorSampling; 0];
aux=sqrt(sum(mr.vox2ras0.^2));
mr.volres=aux(1:3);
mr.vol=reshape(Imag2,[size(Imag2,1) size(Imag2,2) 1 3]);
myMRIwrite(mr,outputfilename);

close all

disp('All done!');

