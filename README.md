# LEMON-Pipeline


## Dissection images

Whole slice photos: files are renamed manually using this naming convention:

   CaseNumber_SliceNumber[Side]

and are placed under a directory named "Whole Slice". For instance: 

   Whole Slice/P57-16_A2[P].jpg for the posterior surface of a cerebrum slice

   Whole Slice/P57-16_A2[A].jpg for the anterior surface

   Whole Slice/P57-16_C3[M].jpg for the medial surface of a cerebellar slice

   Whole Slice/P57-16_C3[L].jpg for the lateral surface

   Whole Slice/P57-16_B1[R].jpg for the rostral surface of a brainstem slice

Photos of blocks: files are renamed manually using a very similar convention:

   CaseNumber_Blocks_SliceNumber[Side]

and are placed under a directory named "Blocks". For instance: 

   Blocks/P57-16_Blocks_A1[A].jpg

If a slice only has one block, there will be no specific photo of the blocks 
 
Processing: you can now run cutphotos/ProcessPhoto.m to detect the frame, 
correct pixel size, etc. It's an interactive script, but it doesn't require
too much manual work. You have to run it twice: once providing the directory
"Whole Slice" as input and another time for "Blocks". When you run it for 
whole slices, simply answer 1 when it asks how many blocks you have in the photo.
This script creates a subdirectory named "cornersAndMasks" that contains a 
bunch of mat files with coordinates for the corners of the frame:

   Blocks/cornersAndMasks/*.mat

   Whole Slice/cornersAndMasks/*.mat


Autopsy photo registration: you can now run this script to solve the "jigsaw
puzzle" for the autopsy photographs, between blocks and whole slices. It's 
fully automated, doesn't take long, and shows the outputs so you can check 
they're more or less fine. I've never seen it fail too badly because it uses
SIFT which is pretty robust. As inputs, you provide the directories "Blocks"
and "Whole Slice". It will create a directory "registrations" under "Blocks", 
with 3 files per slice photo: 

   case_slice.[transforms.mat/warped.mosaic.png/warped.target.png]

So, for instence:

   Blocks/registrations/P57-16_A4[A].transforms.mat

   Blocks/registrations/P57-16_A4[A].warped.mosaic.png

   Blocks/registrations/P57-16_A4[A].warped.target.png


Once you're done, please copy "Blocks" and "Whole Slice" with its child
directories to the external hard drive and also upload them to the FTP server


## Sectioning images

Renaming blockface photos: At the end of sectioning, you will have a directory   
with 100s of photos. You then run blockface/RenameBlockfacePhotos.m, and provide  
this directory as input. The script requires some interaction to ensure that there
are no missing or duplicate photographs, and also ensures that the naming of
the files is consistent. It will generate a directory "renamed" under the input
directory with the renamed image files, follwing the convention:

   CaseNumber_BlockName_photoNumber

For example: 

   renamed/P57-16_A1.3_254.jpg

At the end, it also prompts the user to select a reference photo for the block,
as well as to mark the corners of the green cassette on it: this is a small effort
that provides increased robustness later down the (processing) road. This 
information is stored in a file named:

   renamed/REFdata.mat


Building volumes: once you've renamed the blockface photos of a block, you 
need to run blockface/processBlockFaceDirectory.m. You will need to provide
the directory "renamed" as input. This is a long but fully automated script 
that takes the renamed photos and builds three volumes:one grayscale, one RBG, 
and one probabilistic segmentation. These volumes will be located under "renamed"
and will be named

   CaseNumber_BlockName_volume.rgb.nii.gz

   CaseNumber_BlockName_volume.gray.nii.gz

   CaseNumber_BlockName_volume.mask.nii.gz

For instance:

   renamed/P57-16_A1.3_volume.rgb.nii.gz

   renamed/P57-16_A1.3_volume.gray.nii.gz

   renamed/P57-16_A1.3_volume.mask.nii.gz


When you're done: each case must have a two directories named "Sectioning" and 
"BlockVols" in the FTP and the hard drive. If they don't exist, please create them.
Inside Sectioning, please create a directory with the name of the block 
(CaseNumber_BlockName), e.g., Sectioning/P57-16_A1.3, and copy renamed in there
with all its contents (renamed photos and mat file with corners), e.g.:

   Sectioning/P57-16_A1.3/renamed/P57-16_A1.3_*.jpg

   Sectioning/P57-16_A1.3/renamed/REFdata.mat

Finally, copy the reconstructed volumes (rgb/gray/mask) inside BlockVols, e.g.,

   BlockVols/P57-16_A1.3_volume.rgb.nii.gz

   BlockVols/P57-16_A1.3_volume.gray.nii.gz

   BlockVols/P57-16_A1.3_volume.mask.nii.gz

One more thing: for the brainstem, there will be a single block, with N (about 5)
different chunks. Please make N copies of it, e.g., 

   P57-16_B1.1_volume.*.nii.gz

   P57-16_B2.1_volume.*.nii.gz

   P57-16_B3.1_volume.*.nii.gz

   P57-16_B4.1_volume.*.nii.gz

   P57-16_B5.1_volume.*.nii.gz

and then edit the soft masks, eliminating in each file the N-1 components 
corresponding to the other chunks of tissue. It's annoying but you only need
to do it once per case. 



## Linear 3D registration (reconstruction) of blocks

The first step is running cutphotos/RegisterCutfaceToBlockfaceAllRegions.m.
This script registers the face of the block to the top/bottom of the 
corresponding blockface photo stack. It typically requires a fair amount of 
manual interaction. The script has a big outter loop for cerebrum, cerebellum 
and brainstem, each with its orientaton and peculiarity for the last slice:

- Cerebrum blocks. Note that the top of a block (i.e., first images in
the volume) correspond to the posterior side of the block, except for the
most posterior block, which is processed face down (on the flat side)
and is the other way around.

- Cerebellum blocks. Note that the top of a block (i.e., first images in
the volume) correspond to the lateral side, except for the most lateral
slice, which is the other way around, so the flat side can be down

- Brainstem block. Note that the top of a block (i.e., first images in
the volume) correspond to the superior (rostral) side. There are no
exceptions with the final block in the brainstem.

This script receives as input the "BlockVols" directory of the case, and 
creates a new directory BlockVols/registered with 3 files per block:

   BlockVols/registered/case_block_[target.png / warped.png / .mat]

For instance

   BlockVols/registered/P57-16_P5.1.target.png

   BlockVols/registered/P57-16_P5.1.warped.png

   BlockVols/registered/P57-16_P5.1.mat

Initialization: these registrations you've computed are used to initialize 
the position of the blocks in 3D, independently for cerebrum, cerebellum, and 
brainstem. You do this with the scripts: 

reconstruction/InitializeWithCutPhotosCerebrum.m

reconstruction/InitializeWithCutPhotosCerebellum.m

reconstruction/InitializeWithCutPhotosBrainstem.m

They require a bit of interaction if the registrations of the (top-of-block) 
blockface to the mosaic of the previous slice are not satisfactory.

The scripts will ask for a "parent" output directory; it will automatically 
append "initializedBlocks" to your choice. Please choose the same in all 
three scripts; files will not be overwritten. 

These scripts produce two things: 

a) a bunch of initialized blocks, e.g., 

      initializedBlocks/P57-16_A3.2_volume.[gray/rgb/mask].initialized.mgz

b) three sets of mosaics with the whole cerebrum/cerebellum/brainstem

      initializedBlocks/initialResampledCerebrum.[gray/rgb/mask].nii.gz

      initializedBlocks/initialResampledCerebellum.[gray/rgb/mask].nii.gz

      initializedBlocks/initialResampledBrainstem.[gray/rgb/mask].nii.gz

Now you need a bit of manual intervention: we'll need 3 transforms in 
FreeSurfer lta format that align the mosaics with the ex vivo MRI scan.
I normally use averageWithReg.reoriented.nii.gz, which is a rotated version 
of the (averaged) original acquisition, so that it is properly oriented (note
that this resampling happened with the header). So, for each of the 3 mosaics
(cerebrum, cerebellum, brainstem):

a) Open the MRI in Freeview, with the mosaic on top (grayscale version because)
   Freeview doesn't handle transparency with RGB too well. For instance:

     freeview averageWithReg.reoriented.nii.gz  \
              initializedBlocks/initialResampledBrainstem.gray.nii.gz  

b) Use the tools under Tools -> Transform Volume  and the transparency settings
   to align the mosaic to the MRI. 

c) When you are done, click on "Save Reg" and save the registration as:
   initializedBlocks/initialResampled[Cerebrum/Cerebellum/Brainstem].regToMRI.lta

And now, you're ready to run the "monster"! This is done with the script 
"mosaicPuzzle/RefineWithConstrainedJointRegistration.m". As input you provide:

- the directory with initialized blocks, i.e., "initializedBlocks"

- the reoriented MRI scan, which I normally name "averageWithReg.reoriented.nii.gz"

The output directory should be called "registeredBlocks". Depending on the
resolution, this may run for many hours (on my laptop, it takes about a whole 
day at 0.5 mm resolution). 

Once everything is done, please move to the hard drive and the FTP server the
following three directories: 

   BlockVols/registered/

   initializedBlocks

   registeredBlocks

At this point, we are done with blockface images (yay!)


## Stained sections

When scanning the sections, it is crucial that they are correctly oriented 
(see details in protocol). The file names will be determined by the scanner
software, but we need to rename the files and also generate automated
segmentations for them (we'll eventually use manual segmentations from Nellie,
but we need segmentations for the time being). This is all done with the script:
"scannedSlides/ProcessScannedSections.m". You essentially provide an input directory
with  the files for a block, and some additional information (name of block, type
of stain, the equivalency with the blockface photos), and it creates a  directory 
(named after the block and stain) under the input directory, with (1) the renamed 
images, in [case_block_stain_section] format; (2) the corresponding masks; and (3) a 
mat file ([block_name]_[stain]_mapping.mat) with the mapping between scanned 
sections and blockface photos. For instance:

      [inputDirectory]/P57-16_A1.2_HE/P57-16_A1.2_HE_01.jpg

      [inputDirectory]/P57-16_A1.2_HE/P57-16_A1.2_HE_01.mask.png

      ...

      [inputDirectory]/P57-16_A1.2_HE/P57-16_A1.2_HE_40.jpg

      [inputDirectory]/P57-16_A1.2_HE/P57-16_A1.2_HE_40.mask.png

      [inputDirectory]/P57-16_A1.2_HE/P57-16_A1.2_HE_mapping.mat

Please move the output directory into a directory named "ScannedSections"
under the case directory on the FTP (CyberDuck) and the hard drives. For instance:

      ScannedSections/P57-16_A1.2_HE/P57-16_A1.2_HE_mapping.mat

      ScannedSections/P57-16_A1.2_HE/P57-16_A1.2_HE_01.jpg

      ScannedSections/P57-16_A1.2_HE/P57-16_A1.2_HE_01.mask.png
      (and so forth)


## Histology reconstruction

We're finally here! You can run histoRecon/RegisterHistoBlock to reconstruct
the histology of a block. The first time you run it, it will ask you to manually
specify the rotation / flip between the histology and the MRI/blockface volume.
In addition to the input MRI scan, you need to provide:

- the directory with the jigsaw puzzle reconstruction, typically: 

    "registeredBlocks".

- the directory with the renamed histological sections, for example, 

    "ScannedSections/P57-16_A1.2_HE/"

- the name of the block (e.g., "P57-16") and the stain (e.g., "LFB").

- the name of the output directory. I suggest that this is called
  "HistoRecon/[BlockName]_[stain]", for instance:

    "HistoRecon/P57-16_A1.2_HE/"


When the code is done running (warning: it takes a couple of hours), please 
move these output directories to the  FTP / CyberDuck, e.g.:

HistoRecon/P57-16_A1.1_HE/

HistoRecon/P57-16_A1.1_LFB/

HistoRecon/P57-16_A1.2_HE/

HistoRecon/P57-16_A1.2_LFB/

and so forth.










