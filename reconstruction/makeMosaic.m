function mriMosaic=makeMosaic(mriImageWarped,mriMaskWarped,zshift,RES,sectionThickness)

% It works with cells, but let's make it compatible with a single mri
% struct, too
if isstruct(mriImageWarped)
    aux=mriImageWarped;
    mriImageWarped=cell(1);
    mriImageWarped{1}=aux;
end
if isstruct(mriMaskWarped)
    aux=mriMaskWarped;
    mriMaskWarped=cell(1);
    mriMaskWarped{1}=aux;
end

% Get maximum size
maxSiz=0;
for Bind=1:length(mriImageWarped)
    maxSiz=max(maxSiz,max(size(mriImageWarped{Bind}.vol)));
end

% Get corners of cuboid in RAS space
cornersRAS1=Inf*ones(4,1);
cornersRAS2=-Inf*ones(4,1);
for Bind=1:length(mriImageWarped)
    for i=[0 maxSiz-1]
        for j=[0 maxSiz-1]
            aux=mriImageWarped{Bind}.vox2ras0*[i; j; 0; 1];
            cornersRAS1=min(cornersRAS1,aux);
            cornersRAS2=max(cornersRAS2,aux);
        end
    end
end

% Define header and size
v2r0=[RES 0 0 cornersRAS1(1);
    0 RES 0 cornersRAS1(2);
    0 0 sectionThickness zshift;
    0 0 0 1];
vr=[RES RES sectionThickness];  % I don't think volres(3) matters...
siz=ceil((cornersRAS2(1:2)-cornersRAS1(1:2))/RES);
siz(3)=1;
mriMosaic=[];
mriMosaic.vol=zeros(siz');
mriMosaic.vox2ras0=v2r0;
mriMosaic.volres=vr;

% Resample
numerator=zeros(size(mriMosaic.vol));
denominator=eps+zeros(size(mriMosaic.vol));
[gr1,gr2]=ndgrid(1:size(mriMosaic.vol,1),1:size(mriMosaic.vol,2));
voxMosaic=[gr2(:)'; gr1(:)'; zeros(1,numel(gr1)); ones(1,numel(gr1))];
rasMosaic=mriMosaic.vox2ras0*voxMosaic;
for Bind=1:length(mriImageWarped)
    voxB=inv(mriImageWarped{Bind}.vox2ras0)*rasMosaic;
    v1=voxB(2,:);
    v2=voxB(1,:);
    v3=voxB(3,:);
    ok=v1>1 & v2>1 & v3>1 & v1<=size(mriImageWarped{Bind}.vol,1) & v2<=size(mriImageWarped{Bind}.vol,2) & v3<=size(mriImageWarped{Bind}.vol,3);
    vals=zeros(1,size(rasMosaic,2));
    vals(ok)=interp3(mriImageWarped{Bind}.vol,v2(ok),v1(ok),v3(ok));
    intensities=zeros(size(mriMosaic.vol));
    intensities(:)=vals;
    vals(ok)=interp3(mriMaskWarped{Bind}.vol,v2(ok),v1(ok),v3(ok));
    mask=zeros(size(mriMosaic.vol));
    mask(:)=vals>128;
    mask=logical(mask);
    numerator(mask)=numerator(mask)+intensities(mask);
    denominator(mask)=denominator(mask)+1; 
end
    
aux=numerator./denominator;
auxP=aux(aux>0);
auxPs=sort(auxP);
robMin=auxPs(round(0.025*length(auxPs)));
robMax=auxPs(round((1-0.025)*length(auxPs)));
mriMosaic.vol=aux-robMin;
mriMosaic.vol=255*mriMosaic.vol/robMax;
mriMosaic.vol(mriMosaic.vol<0)=0;
mriMosaic.vol(mriMosaic.vol>255)=255;
mriMosaic.vol=uint8(mriMosaic.vol);


