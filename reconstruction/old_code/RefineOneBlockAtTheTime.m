clear

initialDir='~/Downloads/initializedBlocks/';
outputDir='~/Downloads/refinedBlocks/';
coarselyAlignedMRI='~/Downloads/averageWithReg.manualCoarse.nii.gz';
regCMD='~/Software/niftyreg-git/build/reg-apps/reg_aladin ';
applyAffCMD='~/Software/niftyreg-git/build/reg-apps/reg_transform -updSform ';
invertAffCMD='~/Software/niftyreg-git/build/reg-apps/reg_transform -invAff ';
resampleCMD='~/Software/niftyreg-git/build/reg-apps/reg_transform  ';

if exist(outputDir,'dir')==0
    mkdir(outputDir);
end

% First alignment
disp('Aligning MRI to resampled images');
initialResampledVol=[initialDir filesep 'initialResampledVolume.gray.nii.gz'];
affFileMRI='/tmp/affine.txt'; if exist(affFileMRI,'file'), delete(affFileMRI); end
resampledMRIfile='/tmp/resampledMRI.nii.gz'; if exist(resampledMRIfile,'file'), delete(resampledMRIfile); end
cmd=[regCMD ' -ref ' initialResampledVol ' -flo ' coarselyAlignedMRI  ...
    ' -noSym -aff ' affFileMRI ' -nac  -ln 4 -res ' resampledMRIfile ' -speeeeed >/dev/null']; 
system(cmd);

% Align blocks one by one
d=dir([initialDir filesep '*noCorrection.gray.initialized.mgz']);
for i=1:length(d)
    disp(['  Block ' num2str(i) ' of ' num2str(length(d))]);
    affFile=['/tmp/affine_' num2str(i,'%.2d') '.txt']; if exist(affFile,'file'), delete(affFile); end
    affFileInverse=['/tmp/affine_' num2str(i,'%.2d') '.inverse.txt']; if exist(affFile,'file'), delete(affFile); end
    refFile=[initialDir filesep d(i).name];
    refFileNifty='/tmp/ref.nifty.nii.gz';
    mri=myMRIread(refFile); myMRIwrite(mri,refFileNifty);
    cmd=[regCMD ' -ref ' refFileNifty ' -flo ' coarselyAlignedMRI  ...
    ' -noSym -inaff ' affFileMRI ' -aff ' affFile ' -ln 1 -rigOnly -res /tmp/kk.nii.gz -speeeeed >/dev/null']; 
    system(cmd);
    system([invertAffCMD ' ' affFile ' ' affFileInverse ' >/dev/null']);
end

% Build mosaic with inverse transforms
aux=myMRIread(coarselyAlignedMRI);
numerator=zeros(size(aux.vol));
denominator=zeros(size(aux.vol));
for i=1:lenth(d)
    disp(['  Block ' num2str(i) ' of ' num2str(length(d))]);
    affFileInverse=['/tmp/affine_' num2str(i,'%.2d') '.inverse.txt']; if exist(affFile,'file'), delete(affFile); end
    moving=[initialDir filesep d(i).name];
    movingMask=[initialDir filesep d(i).name(1:end-21) '.mask.initialized.mgz'];
    movingNifty='/tmp/moving.nii.gz';
    movingMaskNifty='/tmp/moving.mask.nii.gz';
    mri=myMRIread(moving); myMRIwrite(mri,movingNifty);
    mri=myMRIread(movingMask); myMRIwrite(mri,movingMaskNifty);
    cmd=;
    system([cmd ' >/dev/null']);
    
end



