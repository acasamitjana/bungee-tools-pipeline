clear

%addpath('~/Desktop/OneDrive - University College London/MATLAB/myFunctions/');
%setenv('FREESURFER_HOME','/Applications/freesurfer60beta/freesurfer');
addpath ~/matlab/myFunctions
setenv('FREESURFER_HOME','/usr/local/freesurfer');

% initialDir='~/Downloads/initializedBlocks/';
% outputDir='~/Downloads/refinedBlocks/';
% coarselyAlignedMRI='~/Downloads/averageWithReg.manualCoarse.nii.gz';
% regCMD='~/Software/niftyreg-git/build/reg-apps/reg_aladin ';
% regCMDnonlin='~/Software/niftyreg-git/build/reg-apps/reg_f3d ';
% applyAffCMD='~/Software/niftyreg-git/build/reg-apps/reg_transform -updSform ';
% invertAffCMD='~/Software/niftyreg-git/build/reg-apps/reg_transform -invAff ';
% defCMD='~/Software/niftyreg-git/build/reg-apps/reg_transform -def ';
% resampleCMD='~/Software/niftyreg-git/build/reg-apps/reg_resample  ';
% convertCMD='/Applications/freesurfer60beta/freesurfer/bin/mri_convert ';
% tempdir='/tmp/';
% N_ITS=5;

initialDir='~/matlab/initializedBlocks/';
outputDir='~/matlab/refinedBlocks/';
coarselyAlignedMRI='~/matlab/averageWithReg.nii.gz';
regCMD='reg_aladin ';
regCMDnonlin='reg_f3d ';
applyAffCMD='reg_transform -updSform ';
invertAffCMD='reg_transform -invAff ';
defCMD='reg_transform -def ';
resampleCMD='reg_resample  ';
convertCMD='mri_convert ';
tempdir='/tmp/';
N_ITS=5;

if exist(outputDir,'dir')==0
    mkdir(outputDir);
end

% Linear alignment
for its=1:N_ITS
    
    disp('***************************');
    disp(['*      ITERATION   ' num2str(its) '      *']);
    disp('***************************');
    
    
    disp('Aligning MRI to resampled images');
    affFileMRI=[tempdir filesep 'affine.txt']; if exist(affFileMRI,'file'), delete(affFileMRI); end
    resampledMRIfile=[tempdir filesep 'resampledMRI.nii.gz']; if exist(resampledMRIfile,'file'), delete(resampledMRIfile); end
    if its==1
        targetResampledVol=[initialDir filesep 'initialResampledVolume.gray.nii.gz'];
        cmd=[regCMD ' -ref ' targetResampledVol ' -flo ' coarselyAlignedMRI  ...
            ' -noSym -aff ' affFileMRI ' -nac  -ln 4 -res ' resampledMRIfile ' -speeeeed >/dev/null'];
    else
        targetResampledVol=[tempdir filesep 'mosaic_its_' num2str(its-1) '.nii.gz'];
        targetResampledMask=[tempdir filesep 'mosaic_its_' num2str(its-1) '.mask.dilated.nii.gz'];  
        cmd=[regCMD ' -ref ' targetResampledVol ' -flo ' coarselyAlignedMRI ' -rmask ' targetResampledMask  ...
            ' -noSym -aff ' affFileMRI ' -nac  -ln 4 -res ' resampledMRIfile ' -affDirect -speeeeed >/dev/null'];
    end
    disp(cmd);
    system(cmd);
    
    % Nonlinear
    resampledMRIfileNL=[tempdir filesep 'resampledMRInonlin.nii.gz']; if exist(resampledMRIfileNL,'file'), delete(resampledMRIfileNL); end
    cppFile=[tempdir filesep 'nonlin.nii.gz'];
    cmd=[regCMDnonlin ' -ref ' targetResampledVol ' -flo ' coarselyAlignedMRI  ...
        ' -aff ' affFileMRI ' -res ' resampledMRIfileNL ' --lncc 7 -ln 4 -lp 3 -sx 20  -cpp ' cppFile];
    if its>1
        cmd=[cmd ' -rmask ' targetResampledMask];
    end
    disp(cmd);
    system([cmd ' >/dev/null']);
    
    % Convert to displacement
    defFile=[tempdir filesep 'def.nii.gz'];
    cmd=[defCMD ' ' cppFile ' ' defFile ' -ref ' targetResampledVol ' >/dev/null'];
    disp(cmd);
    system(cmd);
    
    % Go around blocks
    DF=myMRIread(defFile);
    d=dir([initialDir filesep '*noCorrection.gray.initialized.mgz']);
    for i=1:length(d)
        
        disp(['  Block ' num2str(i) ' of ' num2str(length(d))]);
        
        if its==1
            refFile=[initialDir filesep d(i).name];
            refMaskFile=[initialDir filesep d(i).name(1:end-21) '.mask.initialized.mgz'];
        else
            refFile=[tempdir filesep 'RF_' num2str(i) '_its_' num2str(its-1) '.mgz'];
            refMaskFile=[tempdir filesep 'RF_' num2str(i) '_its_' num2str(its-1) '.mask.mgz'];
        end
        
        %%%%%%%% This works, but is a bit stupid/inefficient, I'll do it in memory...
%         aux=DF;
%         for dd=1:3
%             aux.vol=DF.vol(:,:,:,:,dd);
%             myMRIwrite(aux,[tempdir filesep 'in.mgz']);
%             system([convertCMD ' ' tempdir filesep 'in.mgz ' tempdir filesep 'out.mgz -odt float -rl  ' refFile ' >/dev/null']);
%             kk=myMRIread([tempdir filesep 'out.mgz']);
%             if dd==1
%                 DEF=kk;
%             else
%                 DEF.vol(:,:,:,dd)=kk.vol;
%             end
%         end
        %%%%%%%%%%%%%%%%%%%%%%
        RF=myMRIread(refFile);
        [II,JJ,KK]=ndgrid(1:size(RF.vol,1),1:size(RF.vol,2),1:size(RF.vol,3));
        vox=[JJ(:)'-1; II(:)'-1; KK(:)'-1; ones(1,numel(II))];
        %ras=RF.vox2ras0*vox;
        %vox2=inv(DF.vox2ras0)*ras;
        vox2=1+(inv(DF.vox2ras0)*RF.vox2ras0)*vox;
        DEF=RF;
        DEF.vol=zeros([size(RF.vol) 3]);
        for dd=1:3
            aux=DF.vol(:,:,:,:,dd);
            data=interp3(aux,vox2(1,:),vox2(2,:),vox2(3,:)); % double inversion of coords 1 and 2: MRIread, and interp3
            data(isnan(data))=0;
            DEF.vol(:,:,:,dd)=reshape(data,size(RF.vol));
        end
        %%%%%%%%%%%%%%%%%%%%%%
        
        
        MASK=myMRIread(refMaskFile);
        
        % OK so no we need to look at the deformations inside the mask, and
        % approximate it by an affine transform. I need to be careful with X
        % and Y being flipped in the 4th dimension...
        [II,JJ,KK]=ndgrid(1:size(MASK.vol,1),1:size(MASK.vol,2),1:size(MASK.vol,3));
        aux=DEF.vol(:,:,:,1);
        newrasX=aux(MASK.vol>0);
        aux=DEF.vol(:,:,:,2);
        newrasY=aux(MASK.vol>0);
        aux=DEF.vol(:,:,:,3);
        newrasZ=aux(MASK.vol>0);
        
        voxI=II(MASK.vol>0)-1;
        voxJ=JJ(MASK.vol>0)-1;
        voxK=KK(MASK.vol>0)-1;
        
        ras=MASK.vox2ras0*[voxJ'; voxI'; voxK'; ones(1,length(voxJ))];
        
        rasX=ras(1,:);
        rasY=ras(2,:);
        rasZ=ras(3,:);
        
        
        % Fit affine transform, incorporate to header and save
        D=[rasX' rasY' rasZ' ones(length(rasX),1)];
        b=D\newrasX;
        A(1,1:3)=b(1:3);
        t(1)=b(4);
        b=D\newrasY;
        A(2,1:3)=b(1:3);
        t(2)=b(4);
        b=D\newrasZ;
        A(3,1:3)=b(1:3);
        t(3)=b(4);
        M=zeros(4); M(1:3,1:3)=A; M(1:3,4)=t'; M(4,4)=1;
        
%         RF=myMRIread(refFile);  % already read in
        RF.vox2ras0=M*RF.vox2ras0;
        RF.vox2ras1=[];
        RF.vox2ras=[];
        RF.tkrvox2ras=[];
        myMRIwrite(RF,[tempdir filesep 'RF_' num2str(i) '_its_' num2str(its) '.mgz']);
        RF.vol=MASK.vol;
        myMRIwrite(RF,[tempdir filesep 'RF_' num2str(i) '_its_' num2str(its) '.mask.mgz']);
        RF.vol=255*double(imdilate(MASK.vol>0,createSphericalStrel(25)));
        myMRIwrite(RF,[tempdir filesep 'RF_' num2str(i) '_its_' num2str(its) '.mask.dilated.mgz']);
        
    end
    
    % Now rebuild the mosaic
    disp('Building new resampled mosaic volume');
    
    mri=myMRIread(coarselyAlignedMRI);
    numerator=zeros(size(mri.vol));
    numeratorRGB=zeros([size(mri.vol) 3]);
    denominator=eps+zeros(size(mri.vol));
    
    [gr1,gr2,gr3]=ndgrid(1:size(mri.vol,1),1:size(mri.vol,2),1:size(mri.vol,3));
    vox=[gr2(:)'-1; gr1(:)'-1; gr3(:)'-1; ones(1,numel(gr1))];
    ras=mri.vox2ras0*vox;
    
    for i=1:length(d)
        
        disp(['  Volume ' num2str(i) ' of ' num2str(length(d))]);
        
        RF=myMRIread([tempdir filesep 'RF_' num2str(i) '_its_' num2str(its) '.mgz']);
        % RFmask=myMRIread([tempdir filesep 'RF_' num2str(i) '_its_' num2str(its) '.mask.mgz']);
        RFmask=myMRIread([tempdir filesep 'RF_' num2str(i) '_its_' num2str(its) '.mask.dilated.mgz']);
        
        voxB=1+RF.vox2ras0\ras;
        v1=voxB(2,:);
        v2=voxB(1,:);
        v3=voxB(3,:);
        ok=v1>1 & v2>1 & v3>1 & v1<=size(RF.vol,1) & v2<=size(RF.vol,2) & v3<=size(RF.vol,3);
        vals=zeros(1,size(ras,2));
        
        vals(ok)=interp3(RF.vol,v2(ok),v1(ok),v3(ok));
        intensities=zeros(size(mri.vol));
        intensities(:)=vals;
        
        vals(ok)=interp3(RFmask.vol,v2(ok),v1(ok),v3(ok));
        mask=zeros(size(mri.vol));
        mask(:)=vals>128;
        mask=logical(mask);
        
        numerator(mask)=numerator(mask)+intensities(mask);
        denominator(mask)=denominator(mask)+1;
    end
    
    mri.vol=numerator./denominator;
    myMRIwrite(mri,[tempdir filesep 'mosaic_its_' num2str(its) '.nii.gz']);
    mri.vol=255*double(denominator>0.5);
    myMRIwrite(mri,[tempdir filesep 'mosaic_its_' num2str(its) '.mask.dilated.nii.gz']);
    
end


% Move final mosaic to final destination
disp('Done iterating: copying over final blocks and mosaic to output directory')
disp('  Mosaic')
copyfile([tempdir filesep 'mosaic_its_' num2str(N_ITS) '.nii.gz'],[outputDir filesep 'mosaic.nii.gz']);
copyfile([tempdir filesep 'mosaic_its_' num2str(N_ITS) '.mask.nii.gz'],[outputDir filesep 'mosaic.mask.dilated.nii.gz']);

for i=1:length(d)
    
    disp(['  Block ' num2str(i) ' of ' num2str(length(d))]);
    
    copyfile([tempdir filesep 'RF_' num2str(i) '_its_' num2str(N_ITS) '.mgz'], ...
        [outputDir filesep d(i).name(1:end-21) '.refined.mgz']);
    copyfile([tempdir filesep 'RF_' num2str(i) '_its_' num2str(N_ITS) '.mask.mgz'], ...
        [outputDir filesep d(i).name(1:end-21) '.refined.mask.mgz']);
    copyfile([tempdir filesep 'RF_' num2str(i) '_its_' num2str(N_ITS) '.mask.dilated.mgz'], ...
        [outputDir filesep d(i).name(1:end-21) '.refined.mask.dilated.mgz']);
    
end

disp('All done!');



