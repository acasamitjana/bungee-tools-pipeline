clear

% For FreeSurfer stuff
%setenv('FREESURFER_HOME','/Applications/freesurfer/');
%setenv('SUBJECTS_DIR','/Applications/freesurfer/subjects');
setenv('FREESURFER_HOME','/usr/local/freesurfer/');
setenv('SUBJECTS_DIR','/usr/local/freesurfer/subjects');

% inputs & directories
initialDir='~/matlab/initializedBlocks/';
outputDir='~/matlab/refinedBlocks/';
coarselyAlignedMRI='~/matlab/averageWithReg.nii.gz';

% commands
regCMD='mri_robust_register ';

if exist(outputDir,'dir')==0
    mkdir(outputDir);
end

% First alignment: first rigid, then affine
disp('Aligning MRI to resampled images');
initialResampledVol=[initialDir filesep 'initialResampledVolume.gray.nii.gz'];

rigidFileMRI='/tmp/rigid.mri.lta'; if exist(rigidFileMRI,'file'), delete(rigidFileMRI); end
resampledRigidMRIfile='/tmp/resampledRigidMRI.mgz'; if exist(resampledRigidMRIfile,'file'), delete(resampledRigidMRIfile); end
cmd=[regCMD ' --dst ' initialResampledVol ' --mov ' coarselyAlignedMRI  ...
    ' --noSym --lta ' rigidFileMRI ' --noinit  --cost NMI --maxsize 200 --mapmov ' resampledRigidMRIfile]; 
% system(cmd);
system([cmd '  >/dev/null']);

affineFileMRI='/tmp/affine.mri.lta'; if exist(affineFileMRI,'file'), delete(affineFileMRI); end
resampledAffineMRIfile='/tmp/resampledAffineMRI.mgz'; if exist(resampledAffineMRIfile,'file'), delete(resampledAffineMRIfile); end
cmd=[regCMD ' --dst ' initialResampledVol ' --mov ' coarselyAlignedMRI  ...
    ' --noSym --lta ' affineFileMRI ' --ixform ' rigidFileMRI '  --cost NMI --maxsize 200 --affine --mapmov ' resampledAffineMRIfile]; 
% system(cmd);
system([cmd '  >/dev/null']);



% Align blocks one by one
d=dir([initialDir filesep '*noCorrection.gray.initialized.mgz']);
for i=1:length(d)
    disp(['  Block ' num2str(i) ' of ' num2str(length(d))]);
    affFile=['/tmp/affine_' num2str(i,'%.2d') '.lta']; if exist(affFile,'file'), delete(affFile); end
    refFile=[initialDir filesep d(i).name];
    cmd=[regCMD ' --dst ' refFile ' --mov ' resampledAffineMRIfile  ...
    ' --noSym --noinit  -lta ' affFile ' --cost NMI --nomulti --mapmov /tmp/kkk.mgz']; 
    % system(cmd);
    system([cmd '  >/dev/null']);
end

% Build mosaic with inverse transforms
aux=myMRIread(resampledAffineMRIfile);
numerator=zeros(size(aux.vol));
denominator=zeros(size(aux.vol))+eps;
for i=1:lenth(d)
    disp(['  Block ' num2str(i) ' of ' num2str(length(d))]);
    
    affFile=['/tmp/affine_' num2str(i,'%.2d') '.lta'];
    aligned='/tmp/aligned.mgz';
    
    moving=[initialDir filesep d(i).name];
    cmd=['mri_convert ' moving ' ' aligned ' -ait ' affFile ' -rl ' resampledAffineMRIfile ' -odt float'];  
    % system(cmd);
    sytem([cmd '  >/dev/null']);
    warpedImage=myMRIread(aligned);
    
        moving=[initialDir filesep d(i).name(1:end-21) '.mask.initialized.mgz'];
    cmd=['mri_convert ' moving ' ' aligned ' -ait ' affFile ' -rl ' resampledAffineMRIfile ' -rt nearest -odt float'];  
    % system(cmd);
    sytem([cmd '  >/dev/null']);
    warpedMask=myMRIread(aligned);
    
    numerator(warpedMask.vol>0)=numerator(warpedMask.vol>0)+warpedImage.vol(warpedMask.vol>0);
    denominator(warpedMask.vol>0)=denominator(warpedMask.vol>0)+1;
    
end
aux.vol=numerator./denominator;
myMRIwrite(aux,'/tmp/newMosaic.mgz');






