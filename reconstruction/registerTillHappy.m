% Rigid registration: it first tries automatically, and if you don't like
% it, you're prompted to manually annotate landmarks to fit transform. It
% then refines the transform automatically, and you get to keep the version
% before or after refinement
function tform = registerTillHappy(fixed,moving) 

if ~isdeployed
    addpath ../cutphotos/rigidTransform
end

[optimizer,metric] = imregconfig('multimodal');
optimizer.InitialRadius = 0.009;
optimizer.Epsilon = 1.5e-4;
optimizer.GrowthFactor = 1.01;
optimizer.MaximumIterations = 300;

moving=double(moving); moving=mean(moving,3); maskMoving=moving>0; moving=moving-min(moving(:));
moving=255*moving/max(moving(:)); moving=uint8(moving); moving(maskMoving==0)=nan;
fixed=double(fixed); fixed=mean(fixed,3); maskFixed=fixed>0; fixed=fixed-min(fixed(:));
fixed=255*fixed/max(fixed(:)); fixed=uint8(fixed); fixed(maskFixed==0)=nan;

tform = imregtform(moving,fixed, 'rigid', optimizer, metric);
warped = imwarp(moving,tform,'OutputView',imref2d(size(fixed)));

again='Yes';
close all, figure(1)
while strcmp(again,'Yes');
    for nview=1:5
        imshow(fixed), pause(0.5);
        imshow(warped), pause(0.5);
    end
    again = questdlg('Want to see registration again?',' ','Yes','No','Yes');
end
close all

happy = questdlg('Are you happy with the registration?',' ','Yes','No','Yes');
while strcmp(happy,'No');
    uiwait(msgbox('Please select a minimum of 3 corresponding landmarks and close the window when ready','modal'));
    close all
    [movP,fixP] = cpselect(moving,fixed,'Wait',true);
    close all
    
    np=size(movP,1);
    if np<3
        uiwait(msgbox(['I said at least 3 points; you only provided ' num2str(np)],'modal'));
    else
        aux=computeRigidTransformation( movP, fixP );
        tform=affine2d(aux');
        warped = imwarp(moving,tform,'OutputView',imref2d(size(fixed)));
        
        again='Yes';
        close all, figure(1)
        while strcmp(again,'Yes');
            for nview=1:5
                imshow(fixed), pause(0.5);
                imshow(warped), pause(0.5);
            end
            again = questdlg('Want to see registration again?',' ','Yes','No','Yes');
        end
        close all
        
        happy = questdlg('Are you happy with the registration?',' ','Yes','No','Yes');
    end
end