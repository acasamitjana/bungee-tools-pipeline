# RECONSTRUCTION
> Functions to initialize the linear reconstruction of blockface volumes.


## USERS 

InitializeWithCutPhotosCerebrum.m

InitializeWithCutPhotosCerebellum.m

InitializeWithCutPhotosBrainstem.m

These 3 parallel scripts initialize the reconstruction of the blockface stacks 
using the registrations between the cut photos and the blockface images at the
top of the blocks. It requires a bit of interaction if the registrations
of the (top-of-block) blockface to the mosaic of the previous slice are
not satisfactory





