# SCANNED-SLIDES
> Code process scanned histological sections

## USERS 

ProcessScannedSections.m

This is a script to rename and segment the scaned sections of a given block.
You provide:

- an input directory with the files for a block

- the name of the block

- the name of the stain (LFB/H&E)

- the equivalency with the blockface images, by specifying (a) the index
     of the section corresponding to the first slide; and (b) the spacing
     (in images) between glass slides.

The script will create a subdirectory inside the input directory (with the name
of the block and stain), containing the renamed files; corresponding automated 
segmentations, and a mat file ([block_name]_[stain]_mapping.mat) that stores 
the mapping between  slides and blockface photos.


## DEVELOPERS

These are functions to generate training data for a classifier (tissue vs glass)
and to train such a classifier. 

GenerateTrainingData.m

Goes around a bunch of volumes, randomly extracts slices, and asks the
user to annotate masks. The image / mask pairs are used as training data
to automatically segment the tissue from the glass background.
This will probably be unneccessary when we have all the segmentations 
from Nellie, but for now we need it. 


GenerateTrainingData_HEauto.m

This is a version of GenerateTrainingData.m that only works for H&E. It uses
simple thresholding and morphological operations to generate automated masks,
which the user can reject if he/she doesnt' like them.


TrainSegmentBFclassifierDL.m

Uses the training data from "GenerateTrainingData.m" to train a deep learning
classifier (a fully convolutional netrowk, or FCN) that segments tissue from 
glass. As mentioned above, we shouldn't need this when we have all the manual
segmentations, but for the time being, we need to run it. Note that we need 
a GPU to run this code!



## TODO

Train a network to segment H&Es




