% This is a script to rename the scaned sections of a given block
% - an input directory with the files for a block
% - the name of the block
% - the name of the stain (LFB/H&E)
% - the equivalency with the blockface images, by specifying (a) the index
%      of the section corresponding to the first slide; and (b) the spacing
%      (in images) between glass slides.
% The script will create a subdirectory inside the input directory (with the name
% of the block and stain), containing the renamed files; corresponding automated
% segmentations, and a mat file ([block_name]_[stain]_mapping.mat) that stores the
% mapping between slides and blockface photos.
% Task cells

clear all
clc

if exist([pwd() filesep '..' filesep 'configFile.yaml'])
    YamlStruct = ReadYaml([pwd() filesep '..' filesep 'configFile.yaml']);
    subjectName = YamlStruct.Subject;
else
    subjectName = inputdlg('Please provide subject name (e.g., P57-16)');
    subjectName = subjectName{1};
end

if  exist([pwd() filesep '..' filesep 'experiment_options' filesep subjectName '.mat'])
    load([pwd() filesep '..' filesep 'experiment_options' filesep subjectName '.mat'], 'options');
    
    for taskID=1:length(options)
    
        stain = options{taskID}{1};
        if strcmp(stain, 'LFB_HE')
            continue
        end
        if options{taskID}{3} == 0
            continue
        end

        blockName = options{taskID}{2};
        save('run_options.mat', 'stain', 'blockName')
        try
            disp(['Processing ' stain '_' blockName  ' ' num2str(taskID)])
            ProcessScannedSections
        catch
            fileID=fopen('missing_blocks_20200131.txt','a');
            fprintf(fileID,'%s, %s, %s\n',stain, blockName);
            fclose(fileID);
            continue
        end
        delete('run_options.mat')
    
    end

else
    if exist('run_options.mat')
        delete('run_options.mat')
    end
    ProcessScannedSections
end
