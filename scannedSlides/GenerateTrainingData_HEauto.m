% Goes around a bunch of volumes, randomly extracts slices, and asks the
% user to annotate masks. The image / mask pairs are used as training data
% to automatically segment the tissue from the glass background.
% This will probably be unneccessary when we have all the segmentations
% from Nellie, but for now we need it.
clear, close all, figure
% Options you would normally edit
N_IMS=25; % how many images you are willing to annotate
inputDir='~/Downloads/sampleHisto/'; % input directory

% Options you probably shouldn't touch
FACTOR=25; % makes pixel size 100 microns
outputDir=[pwd() filesep 'trainingDataSegmentationNovember2019_HE' filesep];


%%%%% CODE %%%%%%%

addpath(genpath(['..' filesep 'functions']));

if exist(outputDir,'dir')
    FACTOR_backup=FACTOR;
    load([outputDir filesep 'FACTOR.mat'],'FACTOR');
    if FACTOR_backup ~= FACTOR
        error(['Discrepancy between factor provided in code (' ...
            num2str(FACTOR_backup) ') and factor in FACTOR.mat (' ...
            num2str(FACTOR) ')']);
    end
else
    mkdir(outputDir);
    save([outputDir filesep 'FACTOR.mat'],'FACTOR');
end

% pattern to find files
searchPattern=['*HE*.jpg'];

% Go over files in random order
d=dir([inputDir filesep searchPattern]);
d=d(randperm(length(d)));
ncount = 0;
i=0;
ready=0;
while ready==0
    
    i=i+1;
    
    % input and output files
    outputimagefile=[outputDir filesep d(i).name(1:end-4) '.png'];
    outputsegfile=[outputDir filesep d(i).name(1:end-4) '.mask.png'];
    
    % only work on them if they haven't been segmented yet!
    if exist(outputimagefile,'file')==0 && exist(outputsegfile,'file')==0
        
        disp(['Section ' num2str(i)  ' (count = ' num2str(ncount) ' of ' num2str(N_IMS) ')']);
        
        disp('  Reading in and resizing section');
        IM=imread([inputDir filesep d(i).name]);
        IM=imresize(IM,1/FACTOR);
        
        disp('  Binarization and morphological processing');
        G=double(IM(:,:,2))/255;
        h = hist(G(:),[0:255]/255);
        t = otsuthresh(h);
        M = ~imbinarize(G,t);
        M2=imopen(M,strel('disk',3));
        [BW,n]=bwlabel(M2);
        M3=zeros(size(M));
        for c=1:n
            MM = BW==c;
            count=sum(MM(:));
            ok=1;
            if count<1e5/FACTOR/FACTOR % too small
                ok=0;
            end
            [R,C]=find(MM); R = mean(R); C=mean(C);
            % too close to corner, probably + mark
            conditions=[R<1500/FACTOR, C<1500/FACTOR, R>size(IM,1)-1500/FACTOR, c>size(IM,2)-1500/FACTOR];
            if sum(conditions)>=2
                ok=0;
            end
            if ok
                M3(MM)=1;
            end
        end
        M3=M3>0;
        M4=imclose(M3,strel('disk',1)); % for cracks, etc
        
        % Done! Display and decide if you want to keep it
        M=M4;
        
        IMm=zeros(size(IM));
        Mb = M & ~imerode(M, strel('disk',5));
        aux=IM(:,:,1); aux(Mb)=255; IMm(:,:,1)=aux;
        aux=IM(:,:,2); aux(Mb)=0;   IMm(:,:,2)=aux;
        aux=IM(:,:,3); aux(Mb)=0;   IMm(:,:,3)=aux;
        IMm=uint8(IMm);
        imshow(IMm);
        ok = questdlg('Do you like the mask?',' ','Yes','No','Yes');
        if strcmp(ok,'Yes')
            imwrite(IM,outputimagefile);
            imwrite(uint8(255*double(M>0)),outputsegfile);
            ncount=ncount+1;
        end
        
        disp(' ');
        if ncount>=N_IMS || i==length(d)
            ready=1;
        end
    end
end
disp('All done!')
close all


