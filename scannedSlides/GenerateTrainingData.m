% Goes around a bunch of volumes, randomly extracts slices, and asks the
% user to annotate masks. The image / mask pairs are used as training data
% to automatically segment the tissue from the glass background.
% This will probably be unneccessary when we have all the segmentations 
% from Nellie, but for now we need it.
clear, close all, figure

% Options you would normally edit 
N_IMS=25; % how many images you are willing to annotate
inputDir='~/Downloads/sampleHisto/'; % input directory

% Options you probably shouldn't touch 
FACTOR=25; % makes pixel size 100 microns
contrast='LFB';
outputDir=[pwd() filesep 'trainingDataSegmentationJanuary2019_' contrast filesep];


%%%%% CODE %%%%%%%

addpath(genpath(['..' filesep 'functions']));

if exist(outputDir,'dir')
    FACTOR_backup=FACTOR;
    load([outputDir filesep 'FACTOR.mat'],'FACTOR');
    if FACTOR_backup ~= FACTOR
        error(['Discrepancy between factor provided in code (' ...
            num2str(FACTOR_backup) ') and factor in FACTOR.mat (' ...
            num2str(FACTOR) ')']);
    end
else
    mkdir(outputDir);
    save([outputDir filesep 'FACTOR.mat'],'FACTOR');
end

% pattern to find files
searchPattern=['*' contrast '*.jpg'];
% Structuring element to erode masks
strel=[0 1 0; 1 1 1; 0 1 0]>0;

% Go over files in random order
d=dir([inputDir filesep searchPattern]);
d=d(randperm(length(d)));
ncount = 0;
i=0;
ready=0;
while ready==0
    
    i=i+1;
    
    % input and output files
    outputimagefile=[outputDir filesep d(i).name(1:end-4) '.png'];
    outputsegfile=[outputDir filesep d(i).name(1:end-4) '.mask.png'];
    
    % only work on them if they haven't been segmented yet!
    if exist(outputimagefile,'file')==0 && exist(outputsegfile,'file')==0
        
        ncount=ncount+1;
        
        disp(['Section ' num2str(i)  ' (count = ' num2str(ncount) ' of ' num2str(N_IMS) ')']);
        
        disp('  Reading in section');
        
        IM=imread([inputDir filesep d(i).name]);
        IM=imresize(IM,1/FACTOR);
        
        M=zeros([size(IM,1), size(IM,2)])>0;
        IMm=IM;
        
        imshow(IM);
        more='Yes';
        while strcmp(more,'Yes')
            more = questdlg('Do you want to add another connected component to the mask?',' ','Yes','No','Yes');
            if strcmp(more,'Yes')
                imshow(IMm);
                h=impoly();
                position=wait(h);
                mask=poly2mask(position(:,1), position(:,2),size(IM,1),size(IM,2));
                M=M | mask;
                ME = M;
                for kk=1:5
                    ME = imerode(ME,strel);
                end
                Mb = M & ~ME;
                aux=IM(:,:,1); aux(Mb)=255; IMm(:,:,1)=aux;
                aux=IM(:,:,2); aux(Mb)=0;   IMm(:,:,2)=aux;
                aux=IM(:,:,3); aux(Mb)=0;   IMm(:,:,3)=aux;
                imshow(IMm);
            end
        end
        
        % Write outputs
        imwrite(IM,outputimagefile);
        imwrite(uint8(255*double(M>0)),outputsegfile);
        
        disp(' ');
        if ncount>=N_IMS
            ready=1;
        end
    end
end
disp('All done!')
close all


