% Uses the training data from "GenerateTrainingData.m" to train a deep learning
% classifier (a fully convolutional network, or FCN) that segments tissue from 
% glass. We shouldn't need this when we have all the manual segmentations, 
% but for the time being, we need to run it. 
% A GPU is needed to run this code
clear
clc

%%%%%%%%%
% OPTIONS

% Directory with training data (i.e., output from GenerateTrainingData.m)
inputDir=[pwd() filesep 'trainingDataSegmentationJanuary2019_LFB' filesep];

% Rescaling factor. Higher means smaller images. Bear in mind that this 
% factor is on top of FACTOR in GenerateTrainingData.m, so 1 is generally ok
rescalingFactor=1;

% Output file with weights of trained network
outputFile='FCN.norescaling.LFB.mat';

% Number of random crops we generate per image
nCrops=4;

% Size of minibatch. 
minibatchsize=2;

% Index of GPU we're using (typically 1)
gpuDeviceNumber=1;


%%%%%%%%%%%%%%%%
% BEGINNING OF CODE

gpuDevice(gpuDeviceNumber);
imageDir=[tempdir filesep 'trainingImages'];
labelDir=[tempdir filesep 'trainingLabels'];
imageSize = [round(400/rescalingFactor) round(300/rescalingFactor) 3];

if exist(imageDir,'dir'), system(['rm -rf ' imageDir]); end
if exist(labelDir,'dir'), system(['rm -rf ' labelDir]); end
mkdir(imageDir);
mkdir(labelDir);

% Gather images
disp('Preparing input files');
d=dir([inputDir filesep '*.mask.png']);
for nc=1:nCrops
    for i=1:length(d)
        
        I=imread([inputDir filesep d(i).name(1:end-9) '.png']);
        M=imread([inputDir filesep d(i).name]);
        
        Ir=imresize(I,1/rescalingFactor);
        Mr=uint8(255*double(double(imresize(M,1/rescalingFactor))>128));
        
        skipR=randi(1+size(Ir,1)-imageSize(1),1)-1;
        skipC=randi(1+size(Ir,2)-imageSize(2),1)-1;
        Ir=Ir(1+skipR:imageSize(1)+skipR,1+skipC:imageSize(2)+skipC,:);
        Mr=Mr(1+skipR:imageSize(1)+skipR,1+skipC:imageSize(2)+skipC);
        
        imwrite(Ir,[imageDir filesep 'im_' num2str(i,'%.4d') '_crop_' num2str(nc) '.png']);
        imwrite(Mr,[labelDir filesep 'im_' num2str(i,'%.4d') '_crop_' num2str(nc) '.png']);
    end
end
NimTot=nCrops*length(d);

% Classes and sizes of dataset
classes = [
    "Background"
    "Brain"
    ];
numClasses = numel(classes);
labelIDs=[0 255];

% data stores
imds = imageDatastore(imageDir);
pxds = pixelLabelDatastore(labelDir,classes,labelIDs);

% Create net
lgraph = segnetLayers(imageSize,numClasses,'vgg16');
pxLayer = pixelClassificationLayer('Name','labels','ClassNames', classes);
lgraph = removeLayers(lgraph, 'pixelLabels');
lgraph = addLayers(lgraph, pxLayer);
lgraph = connectLayers(lgraph, 'softmax' ,'labels');

% surgery to increase learning rates of the 2 rightmost convlutional layers
% For some reason matlab doesn't let you change the LR directly, so you
% have to extact the layer, change the learning rate, and plug it back in

% figure, plot(lgraph), title('Before surgery')

decoder1_conv1=lgraph.Layers(87);
decoder1_conv1.BiasLearnRateFactor=20;
decoder1_conv1.WeightLearnRateFactor=20;
lgraph = removeLayers(lgraph, 'decoder1_conv1');
lgraph = addLayers(lgraph, decoder1_conv1);
lgraph = connectLayers(lgraph, 'decoder1_relu_2' ,'decoder1_conv1');
lgraph = connectLayers(lgraph, 'decoder1_conv1' ,'decoder1_bn_1');

decoder1_conv2=lgraph.Layers(84);
decoder1_conv2.BiasLearnRateFactor=20;
decoder1_conv2.WeightLearnRateFactor=20;
lgraph = removeLayers(lgraph, 'decoder1_conv2');
lgraph = addLayers(lgraph, decoder1_conv2);
lgraph = connectLayers(lgraph, 'decoder1_unpool' ,'decoder1_conv2');
lgraph = connectLayers(lgraph, 'decoder1_conv2' ,'decoder1_bn_2');


decoder2_conv1=lgraph.Layers(80);
decoder2_conv1.BiasLearnRateFactor=20;
decoder2_conv1.WeightLearnRateFactor=20;
lgraph = removeLayers(lgraph, 'decoder2_conv1');
lgraph = addLayers(lgraph, decoder2_conv1);
lgraph = connectLayers(lgraph, 'decoder2_relu_2' ,'decoder2_conv1');
lgraph = connectLayers(lgraph, 'decoder2_conv1' ,'decoder2_bn_1');

decoder2_conv2=lgraph.Layers(77);
decoder2_conv2.BiasLearnRateFactor=20;
decoder2_conv2.WeightLearnRateFactor=20;
lgraph = removeLayers(lgraph, 'decoder2_conv2');
lgraph = addLayers(lgraph, decoder2_conv2);
lgraph = connectLayers(lgraph, 'decoder2_unpool' ,'decoder2_conv2');
lgraph = connectLayers(lgraph, 'decoder2_conv2' ,'decoder2_bn_2');

% figure, plot(lgraph), title('After surgery')

% Training options
options = trainingOptions('sgdm', ...
    'Momentum', 0.9, ...
    'InitialLearnRate', 5e-2, ... %1e-4   actually, even 0.1 works well ...
    'L2Regularization', 0.0001, ...
    'MaxEpochs', round(20000/NimTot/minibatchsize), ...
    'MiniBatchSize', minibatchsize, ...
    'Shuffle', 'every-epoch', ...
    'VerboseFrequency', 2, ...
    'Plots','training-progress');

% Augmentation
augmenter = imageDataAugmenter('RandXReflection',true,...
    'RandYReflection',true, ...
    'RandXTranslation', [-8 8], ...
    'RandYTranslation',[-8 8], ...
    'RandXScale',[0.9 1.1], ...
    'RandYScale',[0.9 1.1], ...
    'RandRotation',[-10 10], ...
    'RandXShear',[0 10], ...
    'RandYShear',[0 10], ...
    'RandContrast',[0.95 1.05], ...
    'RandBrightness',[-15 15]);

datasource = pixelLabelImageSource(imds,pxds,'DataAugmentation',augmenter);

% Training
[trainedNet, info] = trainNetwork(datasource,lgraph,options);

disp('Training done! Saving to disk');
save(outputFile,'rescalingFactor','trainedNet');
disp('Saved!');


% Show outputs, for the heck of it
disp('Showing outputs; feel free to stop with ctrl-C any time you want');
figure
imds = imageDatastore(imageDir);
while true
    I = read(imds);
    POST = activations(trainedNet,I,'softmax');
    [~,idx]=max(POST,[],3);
    SEG=labelIDs(idx);
    
    subplot(1,3,1), imshow(I)
    subplot(1,3,2), imshow(uint8(255*POST(:,:,2)))
    CONTOUR=SEG & ~imerode(SEG,[0 1 0; 1 1 1; 0 1 0]);
    Icontour=zeros(size(I),'uint8');
    red=I(:,:,1); red(CONTOUR)=255; Icontour(:,:,1)=red;
    green=I(:,:,2); green(CONTOUR)=0; Icontour(:,:,2)=green;
    blue=I(:,:,3); blue(CONTOUR)=0; Icontour(:,:,3)=blue;
    subplot(1,3,3), imshow(Icontour);
    pause
end



