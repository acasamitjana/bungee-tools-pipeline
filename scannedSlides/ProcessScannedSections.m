% This is a script to rename the scaned sections of a given block
% - an input directory with the files for a block
% - the name of the block
% - the name of the stain (LFB/H&E)
% - the equivalency with the blockface images, by specifying (a) the index
%      of the section corresponding to the first slide; and (b) the spacing
%      (in images) between glass slides.
% The script will create a subdirectory inside the input directory (with the name
% of the block and stain), containing the renamed files; corresponding automated
% segmentations, and a mat file ([block_name]_[stain]_mapping.mat) that stores the
% mapping between slides and blockface photos.
% clear, close all, clc


% You probably don't need to touch any of these...
FACTOR=25;
DLmatFilePrefix='FCN.norescaling';


% You definitely don't need to touch anything from here on!

% Input / output directories / files
if exist([pwd() filesep '..' filesep 'configFile.yaml'])
    YamlStruct = ReadYaml([pwd() filesep '..' filesep 'configFile.yaml']);
    inputDirectory = YamlStruct.sectionDir; 
    outputDirectory = YamlStruct.HistoDir;
    labelDirectory = YamlStruct.labelsDir;
    first_section_file = [YamlStruct.sectionDir filesep 'first_slides.csv']; 
    sp_file = [YamlStruct.sectionDir filesep 'separation_slides.csv'];
else
    inputDirectory = uigetdir('','Please select input directory (typically .../Sectioning):');
    outputDirectory = uigetdir('','Please select the output directory (typically .../ScannedSlides):');
    first_section_file = inputdlg('Please provide the file with section numbers corresponding to the first glass slide');
    sp_file = inputdlg('Please provide the file with the number of number of sections in between stained sections (e.g., 10 for interesting blocks, 20 for uninteresting');
end



% Experiment parameters
if exist('run_options.mat')
    load('run_options.mat')
else
    blockName = inputdlg('Please provide block name, including case name (e.g., P57-16_A1.2)');
    blockName = blockName{1};
    stain = questdlg('Is this LFB or H&E?','Stain','LFB','HE','LFB');
end

first_section_table = readtable(first_section_file,'ReadVariableNames', true);
first_section_struct = table2struct(first_section_table);
sp_table = readtable(sp_file,'ReadVariableNames', true);
sp_struct = table2struct(sp_table);
it_sp = find(strcmp({sp_struct.BlockName}, blockName)==1);
sp=sp_struct(it_sp).SeparationSlices;

it_first = find(strcmp({first_section_struct.BlockName}, blockName)==1);
if strcmp(stain, 'LFB')
    first=first_section_struct(it_first).FirstSliceLFB;
else
    first=first_section_struct(it_first).FirstSliceHE;
end


inputDirectory_block = [inputDirectory filesep blockName filesep stain filesep 'renamed'];
if sp==20
    labelDirectory_block = [labelDirectory filesep 'NonInterestingBlocks' filesep blockName filesep 'segmentations'];
else
    labelDirectory_block = [labelDirectory filesep 'InterestingBlocks' filesep blockName filesep 'segmentation_whole_structures'];
end
outputDirectory_block = [outputDirectory filesep blockName filesep stain];
if exist(outputDirectory_block,'dir')==0
    mkdir(outputDirectory_block);
end



% Equivalency with blockface photos
d=dir([inputDirectory_block filesep '*.jpg']);
disp('Writing mat file with section mapping');

matfile = [outputDirectory_block filesep blockName '_' stain '_mapping.mat'];
mapping=first:sp:first+sp*(length(d)-1);
save(matfile,'mapping');

if exist([DLmatFilePrefix '.' stain '.mat'],'file')
    disp('Loading neural net weights'); 
    load([DLmatFilePrefix '.' stain '.mat'],'rescalingFactor','trainedNet');
    SEGMENT=1;
else
    disp(['Neural net for this stain not found (' DLmatFilePrefix '.' stain '.mat)']); 
    disp('Sections will not be segmented');
    SEGMENT=0;
end

if strcmp(blockName, 'P57-16_P6.2') || strcmp(blockName, 'P41-16_P3.2') || strcmp(blockName, 'P41-16_P3.3')
    rotation_angle = -90;
else
    rotation_angle = 180;
end

for i=1:length(d)
    disp(['Working on image ' num2str(i) ' of ' num2str(length(d))]);
    
    inputname=[inputDirectory_block filesep d(i).name];
    outputname=[outputDirectory_block filesep blockName '_'  stain '_' num2str(i,'%.2d') '.jpg'];
    outputnamemask=[outputDirectory_block filesep blockName '_'  stain '_' num2str(i,'%.2d') '.mask.png']; 
    I=imread(inputname);
    imwrite(I,outputname);
    
    if exist([labelDirectory_block filesep d(i).name(1:end-4) '_seg.nii.gz' ])
        Smri = myMRIread([labelDirectory_block filesep d(i).name(1:end-4) '_seg.nii.gz' ]);
        S = squeeze(255*double(Smri.vol > 0));
        S(Smri.vol==4) = 0;
        S = imrotate(S,rotation_angle);
        S = imresize(S,[size(I,1) size(I,2)]);
        imwrite(S,outputnamemask);
     
    elseif SEGMENT>0
        disp(['No labels found for ' d(i).name])
        Ir=imresize(I,1/FACTOR/rescalingFactor);
        [~,~,S]=semanticseg(Ir,trainedNet,'ExecutionEnvironment','cpu');
        S=uint8(255*double(S(:,:,2)));
        S=imresize(S,[size(I,1) size(I,2)]);
        imwrite(S,outputnamemask);
    end
end

disp('All done!')
